package com.gitlab.plugin.workspace

import com.intellij.util.messages.Topic
import com.intellij.util.messages.Topic.AppLevel

interface DuoConfigurationChangedListener {
  companion object {
    @AppLevel
    val DID_CHANGE_CONFIGURATION_TOPIC = Topic(
      DuoConfigurationChangedListener::class.java.name,
      DuoConfigurationChangedListener::class.java
    )
  }

  fun onConfigurationChange(settings: WorkspaceSettings)
}
