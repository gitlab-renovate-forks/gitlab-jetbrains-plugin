package com.gitlab.plugin.codesuggestions.listeners

import com.intellij.util.messages.Topic
import com.intellij.util.messages.Topic.ProjectLevel

interface StreamingInlineCompletionEventListener {
  companion object {
    @ProjectLevel
    val STREAMING_INLINE_COMPLETION_TOPIC = Topic(
      StreamingInlineCompletionEventListener::class.java.name,
      StreamingInlineCompletionEventListener::class.java
    )
  }

  fun onStreamingInlineCompletionCompletion()
}
