package com.gitlab.plugin.codesuggestions.listeners

import com.gitlab.plugin.codesuggestions.services.SuggestionsTracker
import com.intellij.util.messages.Topic
import com.intellij.util.messages.Topic.ProjectLevel

interface SwitchInlineCompletionEventListener {
  companion object {
    @ProjectLevel
    val SWITCH_INLINE_COMPLETION_TOPIC = Topic(
      SwitchInlineCompletionEventListener::class.java.name,
      SwitchInlineCompletionEventListener::class.java
    )
  }

  fun onRequestMultipleSuggestions() {}
  fun onReceivedMultipleSuggestions(suggestionsTracker: SuggestionsTracker) {}
  fun onSwitchInlineCompletion(suggestionsTracker: SuggestionsTracker) {}
}
