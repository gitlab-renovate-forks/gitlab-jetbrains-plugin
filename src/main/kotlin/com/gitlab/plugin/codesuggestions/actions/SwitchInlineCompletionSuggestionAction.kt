@file:Suppress("UnstableApiUsage")

package com.gitlab.plugin.codesuggestions.actions

import com.gitlab.plugin.codesuggestions.SwitchInlineCompletionSuggestionMode
import com.gitlab.plugin.codesuggestions.services.SUGGESTION_TRACKER_KEY
import com.gitlab.plugin.codesuggestions.services.SwitchInlineCompletionSuggestionService
import com.intellij.codeInsight.inline.completion.session.InlineCompletionSession
import com.intellij.openapi.actionSystem.*
import com.intellij.openapi.components.service

class PreviousInlineCompletionSuggestionAction : SwitchInlineCompletionSuggestionAction(
  SwitchInlineCompletionSuggestionMode.PREVIOUS
) {
  init {
    /*
     * JetBrains IDEs versions 2024.x have an PrevInlineCompletionSuggestionAction action with the same shortcut.
     * This is to ensure that only our action is visible when the plugin is enabled and that the shortcut works.
     */
    ActionManager.getInstance().replaceAction("PrevInlineCompletionSuggestionAction", this)
  }
}

class NextInlineCompletionSuggestionAction : SwitchInlineCompletionSuggestionAction(
  SwitchInlineCompletionSuggestionMode.NEXT
) {
  init {
    /*
     * JetBrains IDEs versions 2024.x have an NextInlineCompletionSuggestionAction action with the same shortcut.
     * This is to ensure that only our action is visible when the plugin is enabled and that the shortcut works.
     */
    ActionManager.getInstance().replaceAction("NextInlineCompletionSuggestionAction", this)
  }
}

sealed class SwitchInlineCompletionSuggestionAction(
  private val mode: SwitchInlineCompletionSuggestionMode
) : AnAction() {
  override fun actionPerformed(e: AnActionEvent) {
    val project = e.project
      ?: return

    val editor = e.getData(CommonDataKeys.EDITOR)
      ?: return

    project.service<SwitchInlineCompletionSuggestionService>().switch(editor, mode)
  }

  override fun update(e: AnActionEvent) {
    val editor = e.getData(CommonDataKeys.EDITOR)
      ?: return

    val suggestionsTracker = editor.getUserData(SUGGESTION_TRACKER_KEY)

    val suggestionPartiallyAccepted = suggestionsTracker?.element?.isPartiallyAccepted ?: false
    val isNonStreamingSuggestion = suggestionsTracker?.isStreaming == false
    val isInlineCompletionSessionActive = InlineCompletionSession.getOrNull(editor) != null

    e.presentation.isEnabledAndVisible = isInlineCompletionSessionActive &&
      isNonStreamingSuggestion &&
      !suggestionPartiallyAccepted
  }

  override fun getActionUpdateThread() = ActionUpdateThread.BGT
}
