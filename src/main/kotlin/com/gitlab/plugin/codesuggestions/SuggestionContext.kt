package com.gitlab.plugin.codesuggestions

import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.util.code.CodeFormattingContext
import com.gitlab.plugin.util.removePrefix
import com.gitlab.plugin.util.uri
import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.openapi.project.Project

@Suppress("UnstableApiUsage")
data class SuggestionContext(val request: InlineCompletionRequest) {
  val document by lazy { request.document }

  val prefix: String by lazy {
    document
      .charsSequence
      .take(request.endOffset)
      .toString()
  }

  val suffix: String by lazy {
    document
      .charsSequence
      .drop(request.endOffset)
      .toString()
  }

  val project: Project by lazy { request.file.project }

  var isInvoked: Boolean = false

  fun toRequestPayload() = Completion.Payload(
    currentFile = Completion.Payload.CodeSuggestionRequestFile(
      fileName = removePrefix(request.file.virtualFile.path, request.editor.project?.basePath) ?: request.file.name,
      fileUri = request.file.virtualFile.uri,
      cursorLine = request.editor.caretModel.logicalPosition.line,
      cursorColumn = request.editor.caretModel.logicalPosition.column,
      contentAboveCursor = prefix,
      contentBelowCursor = suffix
    ),
    telemetry = emptyList()
  )

  fun toFormattingContext() = CodeFormattingContext(
    project,
    request.editor,
    prefix,
    suffix,
    request.file.language
  )
}
