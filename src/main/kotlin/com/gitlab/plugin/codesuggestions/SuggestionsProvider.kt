package com.gitlab.plugin.codesuggestions

import com.gitlab.plugin.codesuggestions.render.UpdatableInlineCompletionGrayElement
import com.gitlab.plugin.codesuggestions.services.SUGGESTION_TRACKER_KEY
import com.gitlab.plugin.codesuggestions.services.SuggestionsTracker
import com.gitlab.plugin.lsp.codesuggestions.LanguageServerCompletionStrategy
import com.gitlab.plugin.lsp.services.CodeCompletionRegistryService
import com.gitlab.plugin.services.CodeSuggestionsStateService
import com.intellij.codeInsight.inline.completion.*
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.util.TextRange
import com.intellij.openapi.util.removeUserData
import kotlinx.coroutines.CancellationException
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds

private val BLOCKED_CHARS = setOf("[]", "{}", "()")
val DEBOUNCE_DELAY = 50.milliseconds

@Suppress("UnstableApiUsage")
internal class SuggestionsProvider : DebouncedInlineCompletionProvider() {
  override val id = InlineCompletionProviderID(this::class.java.name)

  private val logger = logger<SuggestionsProvider>()

  override val providerPresentation: InlineCompletionProviderPresentation
    get() = InlineCompletionTooltipPresentation()

  override suspend fun getSuggestionDebounced(request: InlineCompletionRequest): InlineCompletionSuggestion {
    val context = SuggestionContext(request)
    val suggestionRetrievalTime = Clock.System.now()

    val response = context.project.service<LanguageServerCompletionStrategy>().generateCompletions(context)
      ?: return InlineCompletionSuggestion.empty()

    val suggestionRetrievalDuration = Clock.System.now() - suggestionRetrievalTime

    if (response.choices.isEmpty()) {
      logger.debug("No suggestion was returned by the Code Suggestions API")
      return InlineCompletionSuggestion.empty()
    }

    logger.debug(
      "Retrieved suggestion for the request sent at (${suggestionRetrievalTime.toLocalDateTime(TimeZone.UTC)}) " +
        "with the duration of ($suggestionRetrievalDuration ms)"
    )

    return InlineCompletionSuggestion.withFlow {
      val streamingSuggestion = response.getStreamingSuggestion()

      val text = streamingSuggestion?.text ?: response.firstSuggestion
      val element = UpdatableInlineCompletionGrayElement(text, request.editor)
      val suggestionsTracker = SuggestionsTracker(
        context,
        element,
        streamId = streamingSuggestion?.streamId
      )

      request.editor.putUserData(SUGGESTION_TRACKER_KEY, suggestionsTracker)

      emit(element)

      if (streamingSuggestion?.streamId != null) {
        context.project.service<CodeCompletionRegistryService>().register(
          streamingSuggestion.streamId,
          suggestionsTracker
        )
      }
    }
  }

  override suspend fun getDebounceDelay(request: InlineCompletionRequest): Duration = DEBOUNCE_DELAY

  override fun shouldBeForced(request: InlineCompletionRequest): Boolean = false

  override suspend fun getSuggestion(request: InlineCompletionRequest): InlineCompletionSuggestion {
    return try {
      val editor = request.editor

      editor.removeUserData(SUGGESTION_TRACKER_KEY)?.also { suggestionsTracker ->
        editor.project?.service<CodeCompletionRegistryService>()?.cancel(suggestionsTracker)
      }

      super.getSuggestion(request)
    } catch (e: CancellationException) {
      // Workaround to catch JobCancellationException thrown due to job cancel due to debounce.
      InlineCompletionSuggestion.empty()
    }
  }

  override fun isEnabled(event: InlineCompletionEvent): Boolean = !withinBoundingChar(event) &&
    isCodeSuggestionsEnabled(event)

  private fun isCodeSuggestionsEnabled(event: InlineCompletionEvent): Boolean {
    val project = event.toRequest()?.editor?.project ?: return true

    return project.service<CodeSuggestionsStateService>().codeSuggestionsEnabled
  }

  private fun withinBoundingChar(event: InlineCompletionEvent): Boolean {
    val request = event.toRequest() ?: return false

    val currentPosition = request.endOffset
    return currentPosition > 1 &&
      BLOCKED_CHARS.contains(request.document.getText(TextRange(currentPosition - 2, currentPosition)))
  }
}
