package com.gitlab.plugin.actions.status

import com.intellij.openapi.actionSystem.DefaultActionGroup
import org.jetbrains.annotations.NonNls

class StatusActions : DefaultActionGroup() {
  companion object {
    val STATUS_ACTIONS_GROUP_ID: @NonNls String = StatusActions::class.java.name
  }
}
