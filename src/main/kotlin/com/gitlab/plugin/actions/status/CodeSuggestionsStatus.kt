package com.gitlab.plugin.actions.status

import com.gitlab.plugin.services.CodeSuggestionsStateService
import com.gitlab.plugin.ui.GitLabIcons
import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.application.runReadAction
import com.intellij.openapi.components.service

class CodeSuggestionsStatus : AnAction() {
  /**
   * Do nothing when clicked.
   */
  override fun actionPerformed(e: AnActionEvent) = Unit

  override fun getActionUpdateThread() = ActionUpdateThread.BGT

  /**
   * Disable the action for now.
   */
  override fun update(e: AnActionEvent) {
    val codeSuggestionsStateService = e.project?.service<CodeSuggestionsStateService>()
      ?: return

    runReadAction {
      e.presentation.isEnabled = false

      val engagedCheck = codeSuggestionsStateService.getEngagedCheck()
      if (engagedCheck == null) {
        e.presentation.icon = GitLabIcons.Actions.CodeSuggestionsEnabled
        e.presentation.text = "Code Suggestions: Enabled"
      } else {
        e.presentation.icon = GitLabIcons.Actions.CodeSuggestionsDisabled
        e.presentation.text = "Code Suggestions: Disabled (${engagedCheck.checkId})"
      }
    }
  }
}
