package com.gitlab.plugin.actions.status

import com.intellij.ide.BrowserUtil
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent

class ShowDocumentation : AnAction("Show Documentation") {
  override fun actionPerformed(e: AnActionEvent) {
    BrowserUtil.browse("https://docs.gitlab.com/ee/user/gitlab_duo/", e.project)
  }
}
