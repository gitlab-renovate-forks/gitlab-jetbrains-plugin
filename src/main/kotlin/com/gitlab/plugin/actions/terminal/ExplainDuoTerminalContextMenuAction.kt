package com.gitlab.plugin.actions.terminal

import com.gitlab.plugin.services.GitLabApplicationService
import com.gitlab.plugin.telemetry.SnowplowTracker
import com.gitlab.plugin.telemetry.StandardContext
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.jediterm.terminal.ui.TerminalAction
import com.jediterm.terminal.ui.TerminalActionPresentation
import com.snowplowanalytics.snowplow.tracker.events.Structured
import java.awt.event.KeyEvent

class ExplainDuoTerminalContextMenuAction(
  presentation: TerminalActionPresentation,
  project: Project,
) : TerminalAction(presentation) {
  private val terminalActionsService = project.service<TerminalActionsService>()

  override fun isEnabled(e: KeyEvent?): Boolean {
    if (terminalActionsService.areTerminalActionsEnabled()) {
      return terminalActionsService.isTextSelectedInTerminal()
    }
    return false
  }

  override fun isHidden(): Boolean {
    return !terminalActionsService.areTerminalActionsEnabled()
  }

  override fun actionPerformed(e: KeyEvent?): Boolean {
    val event = Structured.builder()
      .category(SnowplowTracker.Category.ASK_GITLAB_CHAT)
      .action("terminal_context_menu_action_clicked")
      .label("/explain")
      .customContext(
        listOf(
          StandardContext.build(),
          SnowplowTracker.IDE_EXTENSION_VERSION_CONTEXT
        )
      )
      .build()

    GitLabApplicationService.getInstance().snowplowTracker.track(event)

    return terminalActionsService.triggerDuoExplain()
  }
}
