package com.gitlab.plugin.actions.terminal

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.services.GitLabApplicationService
import com.gitlab.plugin.telemetry.SnowplowTracker
import com.gitlab.plugin.telemetry.StandardContext
import com.intellij.openapi.actionSystem.ActionUpdateThread
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.components.service
import com.intellij.openapi.project.DumbAwareAction
import com.snowplowanalytics.snowplow.tracker.events.Structured

class ExplainTerminalToolWindowAction :
  DumbAwareAction(
    GitLabBundle.messagePointer("action.terminal.explain-code"),
    GitLabBundle.messagePointer("action.terminal.explain-code.description"),
    com.gitlab.plugin.ui.GitLabIcons.Actions.DuoChatEnabled
  ) {
  override fun getActionUpdateThread(): ActionUpdateThread = ActionUpdateThread.EDT

  override fun update(e: AnActionEvent) {
    val terminalActionsService = e.project?.service<TerminalActionsService>()
    val areTerminalActionsEnabled = terminalActionsService?.areTerminalActionsEnabled() == true

    e.presentation.isVisible = areTerminalActionsEnabled
    e.presentation.isEnabled = areTerminalActionsEnabled && terminalActionsService?.isTextSelectedInTerminal() == true
  }

  override fun actionPerformed(e: AnActionEvent) {
    val event = Structured.builder()
      .category(SnowplowTracker.Category.ASK_GITLAB_CHAT)
      .action("terminal_tool_window_action_clicked")
      .label("/explain")
      .customContext(
        listOf(
          StandardContext.build(),
          SnowplowTracker.IDE_EXTENSION_VERSION_CONTEXT
        )
      )
      .build()

    GitLabApplicationService.getInstance().snowplowTracker.track(event)

    e.project?.service<TerminalActionsService>()?.triggerDuoExplain()
  }
}
