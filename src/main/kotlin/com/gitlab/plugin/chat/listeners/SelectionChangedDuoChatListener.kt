package com.gitlab.plugin.chat.listeners

import com.gitlab.plugin.chat.quickchat.QUICK_CHAT_SESSION_KEY
import com.intellij.openapi.editor.event.SelectionEvent
import com.intellij.openapi.editor.event.SelectionListener

class SelectionChangedDuoChatListener : SelectionListener {
  override fun selectionChanged(e: SelectionEvent) {
    val editor = e.editor

    e.editor.getUserData(QUICK_CHAT_SESSION_KEY)?.selectionChanged(editor)
  }
}
