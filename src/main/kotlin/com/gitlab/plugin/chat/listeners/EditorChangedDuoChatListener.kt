package com.gitlab.plugin.chat.listeners

import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.event.EditorFactoryListener
import com.intellij.openapi.fileEditor.FileEditorManagerEvent
import com.intellij.openapi.fileEditor.FileEditorManagerListener
import com.intellij.openapi.project.Project

class EditorChangedDuoChatListener(
  private val project: Project
) : FileEditorManagerListener, EditorFactoryListener {
  override fun selectionChanged(event: FileEditorManagerEvent) {
    project.service<ChatService>().updateProjectState()
  }
}
