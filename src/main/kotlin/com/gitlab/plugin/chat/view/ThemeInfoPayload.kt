package com.gitlab.plugin.chat.view

import com.intellij.openapi.editor.colors.EditorColorsManager
import com.intellij.ui.JBColor
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import java.awt.Color

@Serializable
data class ThemeInfoPayload(
  val jetBrainsBackground: List<Int> = themePropertyRGB("EditorPane.background"),
  val jetBrainsForeground: List<Int> = themePropertyRGB("EditorPane.foreground"),
  val jetBrainsErrorForeground: List<Int> = themePropertyRGB("Label.errorForeground"),
  val jetBrainsInactiveForeground: List<Int> = themePropertyRGB("EditorPane.inactiveForeground"),

  val jetBrainsInactiveBorder: List<Int> = themePropertyRGB("Borders.contrastBorderColor"),
  val jetBrainsBorder: List<Int> = themePropertyRGB("Borders.color"),

  val jetBrainsBadgeBackground: List<Int> = themePropertyRGB("List.selectionBackground"),
  val jetBrainsBadgeForeground: List<Int> = themePropertyRGB("List.foreground"),

  val jetBrainsActiveText: List<Int> = themePropertyRGB("TextField.foreground"),

  val jetBrainsActiveHyperlink: List<Int> = themePropertyRGB("Link.activeForeground"),
  val jetBrainsHoverHyperlink: List<Int> = themePropertyRGB("Link.hoverForeground"),
  val jetBrainsPressedHyperlink: List<Int> = themePropertyRGB("Link.pressedForeground"),

  val jetBrainsListForeground: List<Int> = themePropertyRGB("List.foreground"),
  val jetBrainsListSelectionForeground: List<Int> = themePropertyRGB("List.selectionForeground"),
  val jetBrainsListSelectionBackground: List<Int> = themePropertyRGB("List.selectionBackground"),
  val jetBrainsListSelectionInactiveBackground: List<Int> = themePropertyRGB(
    "List.selectionInactiveBackground"
  ),
  val jetBrainsListBackground: List<Int> = themePropertyRGB("List.background"),
  val jetBrainsListInactiveBackground: List<Int> = themePropertyRGB("List.inactiveBackground"),

  val jetBrainsButtonDefaultButton: List<Int> = themePropertyRGB("Button.default.startBackground"),
  val jetBrainsButtonForeground: List<Int> = themePropertyRGB("Button.foreground"),
  val jetBrainsButtonShadow: List<Int> = themePropertyRGB("Button.shadow"),
  val jetBrainsButtonDisabledText: List<Int> = themePropertyRGB("Button.disabledText"),

  val jetBrainsTextFieldInactiveForeground: List<Int> = themePropertyRGB("TextField.inactiveForeground"),
  val jetBrainsTextFieldForeground: List<Int> = themePropertyRGB("TextField.foreground"),
  val jetBrainsTextFieldBackground: List<Int> = themePropertyRGB("TextField.background"),
  val jetBrainsTextFieldSelectionBackground: List<Int> = themePropertyRGB("TextField.selectionBackground"),
  val jetBrainsTextFieldSelectionForeground: List<Int> = themePropertyRGB("TextField.selectionForeground"),
  val jetBrainsCheckboxBackground: List<Int> = themePropertyRGB("Checkbox.background"),
  val jetBrainsCheckboxMenuItemSelectionBackground: List<Int> = themePropertyRGB(
    "CheckboxMenuItem.selectionBackground"
  ),
  val jetBrainsCheckboxBorder: List<Int> = themePropertyRGB("Checkbox.border"),
  val jetBrainsCodeBlockEditorPaneBackground: List<Int> = themePropertyRGB(
    EditorColorsManager.getInstance().globalScheme.defaultBackground
  ),
  val jetBrainsFormattedTextFieldInactiveForeground: List<Int> = themePropertyRGB(
    "FormattedTextField.inactiveForeground"
  ),
  val editorPlaceholderForeground: List<Int> = themePropertyRGB("UIDesigner.Placeholder.foreground"),
  val textFieldBorder: List<Int> = themePropertyRGB("TextField.border"),
  val pluginHeaderForeground: List<Int> = themePropertyRGB("Plugins.SectionHeader.foreground"),
  val notificationForeground: List<Int> = themePropertyRGB("Notifications.foreground"),
  val notificationBackground: List<Int> = themePropertyRGB("Notifications.background"),
  val notificationBorder: List<Int> = themePropertyRGB("Editor.Notification.borderInsets.compact"),
  val iconForeground: List<Int> = themePropertyRGB("Actions.Foreground")
) {
  companion object {
    private val json = Json { encodeDefaults = true }
    fun buildJson() = json.encodeToString(serializer(), ThemeInfoPayload())
  }
}

private fun themePropertyRGB(propertyName: String) = JBColor.namedColor(propertyName).toRGBList()
private fun themePropertyRGB(color: Color) = color.toRGBList()

private fun JBColor.toRGBList() = listOf(red, green, blue)
private fun Color.toRGBList() = listOf(red, green, blue)
