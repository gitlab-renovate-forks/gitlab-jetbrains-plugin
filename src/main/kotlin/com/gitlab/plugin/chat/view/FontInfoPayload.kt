package com.gitlab.plugin.chat.view

import com.intellij.openapi.editor.colors.EditorColorsManager
import com.intellij.util.FontUtil
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

@Serializable
data class FontInfoPayload(
  val jetBrainsEditorFont: Map<String, String> = getJetBrainsEditorFont(),
  val jetBrainsConsoleFont: Map<String, String> = getJetBrainsConsoleFont(),
) {
  companion object {
    private val json = Json { encodeDefaults = true }
    fun buildJson() = json.encodeToString(serializer(), FontInfoPayload())
  }
}

private fun getJetBrainsEditorFont() = mapOf(
  "fontSize" to EditorColorsManager.getInstance().globalScheme.editorFontSize.toString(),
  "fontName" to EditorColorsManager.getInstance().globalScheme.editorFontName + ", sans-serif"
)

private fun getJetBrainsConsoleFont() = mapOf(
  "fontSize" to FontUtil.getMenuFont().size.toString(),
  "fontName" to FontUtil.getMenuFont().name + ", sans-serif"
)
