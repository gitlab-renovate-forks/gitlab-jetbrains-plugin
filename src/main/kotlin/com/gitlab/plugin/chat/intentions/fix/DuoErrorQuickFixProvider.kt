package com.gitlab.plugin.chat.intentions.fix

import com.intellij.codeInsight.daemon.impl.HighlightInfo
import com.intellij.codeInsight.daemon.impl.analysis.ErrorQuickFixProvider
import com.intellij.psi.PsiErrorElement

class DuoErrorQuickFixProvider : ErrorQuickFixProvider {
  override fun registerErrorQuickFix(errorElement: PsiErrorElement, builder: HighlightInfo.Builder) {
    val fix = DuoFixQuickFix(errorElement.originalElement)

    builder.registerFix(fix, null, null, null, null)
  }
}
