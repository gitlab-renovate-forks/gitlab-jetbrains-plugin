package com.gitlab.plugin.chat.context.managers

import com.gitlab.plugin.chat.context.AiContextCategory
import com.gitlab.plugin.chat.context.AiContextItem
import com.gitlab.plugin.chat.context.AiContextItemManager
import com.gitlab.plugin.chat.context.AiContextSearchQuery
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project
import kotlinx.coroutines.future.asDeferred

class LanguageServerContextManager(
  private val project: Project
) : AiContextItemManager {
  private val logger = logger<LanguageServerContextManager>()
  private val languageServerService by lazy { project.service<GitLabLanguageServerService>() }

  override suspend fun query(query: AiContextSearchQuery): List<AiContextItem> {
    val lspServer = languageServerService.lspServer
      ?: return emptyList()

    return runCatching {
      lspServer.aiContextQuery(query).asDeferred().await()
    }.getOrElse { error ->
      logger.warn("Failed to get AI context query results from language server", error)
      emptyList()
    }
  }

  override suspend fun add(item: AiContextItem) {
    val lspServer = languageServerService.lspServer
      ?: return

    runCatching {
      lspServer.aiContextAdd(item).asDeferred().await()
    }.getOrElse { error ->
      logger.warn("Failed to add AI context item ${item.id} to language server", error)
    }
  }

  override suspend fun remove(item: AiContextItem) {
    val lspServer = languageServerService.lspServer
      ?: return

    runCatching {
      lspServer.aiContextRemove(item).asDeferred().await()
    }.getOrElse { error ->
      logger.warn("Failed to remove AI context item ${item.id} from language server", error)
    }
  }

  override suspend fun retrieveSelectedContextItemsWithContent(): List<AiContextItem> {
    val lspServer = languageServerService.lspServer
      ?: return emptyList()

    runCatching {
      return lspServer.aiContextRetrieve().asDeferred().await()
    }.getOrElse { error ->
      logger.warn("Failed to retrieve selected context items from language server", error)
      return emptyList()
    }
  }

  override suspend fun getCurrentItems(): List<AiContextItem> {
    val lspServer = languageServerService.lspServer
      ?: return emptyList()

    return runCatching {
      lspServer.aiContextCurrentItems().asDeferred().await()
    }.getOrElse { error ->
      logger.warn("Failed to get current items from language server", error)
      emptyList()
    }
  }

  override suspend fun getAvailableCategories(): List<AiContextCategory> {
    val lspServer = languageServerService.lspServer
      ?: return emptyList()

    return runCatching {
      @Suppress("UselessCallOnCollection") // filterNotNull is actually needed
      lspServer.aiContextGetProviderCategories().asDeferred().await().filterNotNull()
    }.getOrElse { error ->
      logger.warn("Failed to get provider categories from language server", error)
      emptyList()
    }
  }

  override suspend fun clearSelectedContextItems() {
    val lspServer = languageServerService.lspServer
      ?: return

    runCatching {
      lspServer.aiContextClear().asDeferred().await()
    }.getOrElse { error ->
      logger.warn("Failed to clear selected AI context items from language server", error)
    }
  }

  @Suppress("TooGenericExceptionCaught")
  override suspend fun shouldIncludeAdditionalContext(): Boolean {
    return try {
      getAvailableCategories().isNotEmpty()
    } catch (e: Exception) {
      logger.error("Error checking if additional context is enabled", e)
      false
    }
  }
}
