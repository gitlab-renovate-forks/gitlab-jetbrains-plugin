package com.gitlab.plugin.chat.context

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

// To track context items between web views, the extension and the language server.
@Serializable
data class AiContextItem(
  val id: String,
  val category: AiContextCategory,
  val content: String? = null,
  val metadata: AIContextItemMetadata?
)

@Serializable
data class AIContextItemMetadata(
  val title: String,
  val enabled: Boolean,
  val disabledReasons: List<String>? = null,
  val subType: AIContextProviderType,
  val icon: String? = null,
  val secondaryText: String? = null,
  val subTypeLabel: String? = null,
  val relativePath: String? = null,
  val iid: String? = null,
  val workspaceFolder: AiContextItemWorkspaceFolder? = null,
  val project: String? = null,
  val webUrl: String? = null,
  val repositoryUri: String? = null,
  val repositoryName: String? = null,
  val selectedBranch: String? = null,
  val gitType: String? = null,
  val libs: List<AiContextItemDependencyLibrary>? = null
)

// To search for context items of a specified category. Sent from web views to the language server.
@Serializable
data class AiContextSearchQuery(
  val category: AiContextCategory,
  val query: String,
  val workspaceFolders: List<AiContextItemWorkspaceFolder>
)

@Serializable
data class AiContextItemWorkspaceFolder(
  val uri: String,
  val name: String
)

@Serializable
data class AiContextItemDependencyLibrary(
  val name: String,
  val version: String
)

@Serializable
enum class AIContextProviderType {
  @SerialName("open_tab")
  OPEN_TAB,

  @SerialName("local_file_search")
  LOCAL_FILE_SEARCH,

  @SerialName("issue")
  ISSUE,

  @SerialName("merge_request")
  MERGE_REQUEST,

  @SerialName("snippet")
  SNIPPET,

  @SerialName("dependency")
  DEPENDENCY,

  @SerialName("local_git")
  LOCAL_GIT
}

@Serializable
enum class AiContextCategory {
  @SerialName("file")
  FILE,

  @SerialName("snippet")
  SNIPPET,

  @SerialName("issue")
  ISSUE,

  @SerialName("merge_request")
  MERGE_REQUEST,

  @SerialName("dependency")
  DEPENDENCY,

  @SerialName("local_git")
  LOCAL_GIT
}
