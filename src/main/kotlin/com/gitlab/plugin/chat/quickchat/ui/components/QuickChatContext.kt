package com.gitlab.plugin.chat.quickchat.ui.components

import com.gitlab.plugin.chat.context.AiContextCategory
import com.gitlab.plugin.chat.quickchat.ui.QuickChatUIMessageHandler
import com.gitlab.plugin.chat.quickchat.ui.quickChatBackground
import com.gitlab.plugin.chat.view.model.ChatViewMessage
import com.gitlab.plugin.chat.view.model.ContextCategoriesResultMessage
import com.gitlab.plugin.chat.view.model.ContextCurrentItemsResultMessage
import com.intellij.ui.JBColor
import java.awt.BorderLayout
import javax.swing.BorderFactory
import javax.swing.JLabel
import javax.swing.JPanel

@Suppress("MagicNumber")
class QuickChatContext(messageHandler: QuickChatUIMessageHandler) : JPanel(BorderLayout()) {
  private var availableCategories: List<AiContextCategory> = emptyList()

  private val context: JLabel by lazy {
    JLabel().apply {
      border = BorderFactory.createEmptyBorder(3, 0, 0, 0)
      background = quickChatBackground

      font = font.deriveFont(11f)
      foreground = JBColor.DARK_GRAY
    }
  }

  init {
    border = BorderFactory.createEmptyBorder()
    background = quickChatBackground

    add(context, BorderLayout.WEST)

    isVisible = false

    messageHandler.register(::onMessageReceived)
  }

  private fun onMessageReceived(message: ChatViewMessage) {
    when (message) {
      is ContextCategoriesResultMessage -> categoriesUpdated(message)
      is ContextCurrentItemsResultMessage -> currentItemsUpdated(message)
      else -> return
    }
  }

  private fun categoriesUpdated(message: ContextCategoriesResultMessage) {
    availableCategories = message.categories
  }

  private fun currentItemsUpdated(message: ContextCurrentItemsResultMessage) {
    val snippet = message.items.firstOrNull { availableCategories.contains(it.category) }

    isVisible = snippet != null
    context.text = "Include lines: ${snippet?.content.orEmpty()}"
  }
}
