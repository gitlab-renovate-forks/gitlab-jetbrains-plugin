package com.gitlab.plugin.chat.quickchat.ui.components

import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.quickchat.ui.QuickChatUIMessageHandler
import com.gitlab.plugin.chat.quickchat.ui.growsHorizontally
import com.gitlab.plugin.chat.quickchat.ui.quickChatBackground
import com.gitlab.plugin.chat.quickchat.ui.quickChatPrimaryColor
import com.intellij.util.ui.JBUI
import java.awt.*
import javax.swing.*

@Suppress("MagicNumber")
class QuickChatMessage(
  private val messageHandler: QuickChatUIMessageHandler,
  userPrompt: String
) : JPanel() {
  private val gridConstraints: GridBagConstraints
  private var duoResponse: DuoResponseContainer? = null

  init {
    layout = GridBagLayout()

    background = quickChatBackground

    gridConstraints = GridBagConstraints().growsHorizontally().apply {
      insets = JBUI.insetsBottom(5)
    }

    add(QuickChatUserPrompt(userPrompt), gridConstraints)
  }

  fun createPendingDuoResponse() {
    DuoResponseContainer().let { response ->
      duoResponse = response
      add(response, gridConstraints)
    }
  }

  fun updateDuoResponse(record: ChatRecord) {
    if (duoResponse == null) {
      createPendingDuoResponse()
    }

    duoResponse?.response?.chunkReceived(record)
  }

  private inner class DuoResponseContainer : JPanel(BorderLayout(5, 0)) {
    val response: QuickChatDuoResponse

    init {
      border = BorderFactory.createEmptyBorder()
      background = quickChatBackground

      add(
        JLabel("Duo:").apply {
          foreground = quickChatPrimaryColor
          verticalAlignment = SwingConstants.TOP
        },
        BorderLayout.WEST
      )

      response = QuickChatDuoResponse(messageHandler)
      add(response, BorderLayout.CENTER)
    }
  }
}
