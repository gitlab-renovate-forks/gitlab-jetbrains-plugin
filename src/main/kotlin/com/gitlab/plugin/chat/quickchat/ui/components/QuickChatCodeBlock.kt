package com.gitlab.plugin.chat.quickchat.ui.components

import com.gitlab.plugin.chat.quickchat.markdown.MarkdownLanguageConverter
import com.gitlab.plugin.chat.quickchat.markdown.SyntaxElement
import com.gitlab.plugin.chat.quickchat.messages.QuickChatUIMessage
import com.gitlab.plugin.chat.quickchat.ui.QuickChatUIMessageHandler
import com.gitlab.plugin.chat.quickchat.ui.listeners.QuickChatExitKeyListener
import com.gitlab.plugin.chat.quickchat.ui.quickChatBackground
import com.intellij.openapi.actionSystem.PlatformDataKeys
import com.intellij.openapi.editor.actions.IncrementalFindAction
import com.intellij.openapi.editor.ex.EditorEx
import com.intellij.openapi.ide.CopyPasteManager
import com.intellij.ui.EditorTextField
import com.intellij.ui.JBColor
import com.intellij.ui.LanguageTextField
import com.intellij.util.concurrency.annotations.RequiresEdt
import java.awt.*
import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.datatransfer.StringSelection
import java.awt.event.FocusAdapter
import java.awt.event.FocusEvent
import javax.swing.*
import javax.swing.BorderFactory
import javax.swing.JPanel

@Suppress("MagicNumber")
class QuickChatCodeBlock @RequiresEdt constructor(
  private val codeBlock: SyntaxElement.FencedCodeBlock,
  private val messageHandler: QuickChatUIMessageHandler
) : JPanel() {
  init {
    layout = BoxLayout(this, BoxLayout.PAGE_AXIS)

    border = BorderFactory.createEmptyBorder(0, 0, 0, 20)
    background = quickChatBackground

    if (codeBlock.language.isNotBlank()) {
      add(CodeBlockActionsContainer())
      add(Box.createVerticalStrut(5))
    }

    add(CodeSnippetContainer())

    addKeyListener(QuickChatExitKeyListener(messageHandler))
  }

  private inner class CodeBlockActionsContainer : JPanel() {
    init {
      alignmentX = Component.LEFT_ALIGNMENT

      border = BorderFactory.createEmptyBorder()
      background = quickChatBackground

      layout = BoxLayout(this, BoxLayout.X_AXIS)

      add(
        IconButton(
          icon = QuickChatIcons.InsertCodeSnippet,
          text = "Insert code snippet",
        ) {
          messageHandler.onMessageReceived(QuickChatUIMessage.InsertCodeSnippet(codeBlock.snippet))
        }.apply {
          addKeyListener(QuickChatExitKeyListener(messageHandler))
        }
      )
      add(Box.createHorizontalStrut(5))
      add(
        IconButton(
          icon = QuickChatIcons.CopyToClipboard,
          text = "Copy to clipboard",
        ) {
          CopyPasteManager.getInstance().setContents(StringSelection(codeBlock.snippet))
        }.apply {
          addKeyListener(QuickChatExitKeyListener(messageHandler))
        }
      )
    }
  }

  private inner class CodeSnippetContainer : JPanel(BorderLayout()) {
    private val languageTextField: EditorTextField

    init {
      alignmentX = Component.LEFT_ALIGNMENT

      border = BorderFactory.createEmptyBorder(8, 12, 8, 12)
      background = JBColor.background()

      languageTextField = object : LanguageTextField(
        MarkdownLanguageConverter.convert(codeBlock.language),
        null,
        codeBlock.snippet,
        false
      ) {
        init {
          border = BorderFactory.createEmptyBorder()
          background = JBColor.background()

          isViewer = true
          setFontInheritedFromLAF(false)

          addFocusListener(
            object : FocusAdapter() {
              override fun focusGained(e: FocusEvent) {
                editor?.component?.requestFocus()
              }
            }
          )
        }

        // NOTE: Replace this with isRenderMode = true when 2023.3 support is dropped.
        override fun getData(dataId: String): Any? {
          val editor = getEditor(false)
          if (editor != null) {
            if (PlatformDataKeys.COPY_PROVIDER.`is`(dataId)) {
              return editor.copyProvider
            }
          }

          return null
        }

        override fun createEditor(): EditorEx {
          return super.createEditor().apply {
            setHorizontalScrollbarVisible(true)
            putUserData(IncrementalFindAction.SEARCH_DISABLED, true)

            component.addKeyListener(QuickChatExitKeyListener(messageHandler))
          }
        }
      }

      add(languageTextField)
    }

    override fun getMinimumSize(): Dimension {
      val editor = languageTextField.editor
        ?: return super.getMinimumSize()

      val lineHeight = editor.lineHeight
      val lineCount = codeBlock.snippet.lines().size
      val borderSize = 16 // 8 top + 8 bottom

      return Dimension(super.getMinimumSize().width, lineHeight * lineCount + borderSize)
    }
  }
}
