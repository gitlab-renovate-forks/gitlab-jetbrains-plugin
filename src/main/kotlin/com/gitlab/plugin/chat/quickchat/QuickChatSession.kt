package com.gitlab.plugin.chat.quickchat

import com.gitlab.plugin.chat.ChatController
import com.gitlab.plugin.chat.quickchat.ui.QuickChatUI
import com.intellij.openapi.editor.ComponentInlayRenderer
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.Inlay
import com.intellij.openapi.editor.markup.RangeHighlighter
import com.intellij.openapi.util.Disposer
import com.intellij.openapi.util.Key

var QUICK_CHAT_SESSION_KEY = Key.create<QuickChatSession>("QUICK_CHAT_SESSION_KEY")

class QuickChatSession(
  private val inlay: Inlay<ComponentInlayRenderer<QuickChatUI>>,
  private val gutterIcon: RangeHighlighter,
  @Suppress("UnusedPrivateProperty") private val controller: ChatController
) {
  fun close() {
    gutterIcon.dispose()
    Disposer.dispose(inlay)
  }

  fun selectionChanged(editor: Editor) {
    if (!editor.selectionModel.hasSelection()) {
      return controller.selectionChanged(null, null)
    }

    val startLine = editor.selectionModel.selectionStartPosition?.let { editor.visualToLogicalPosition(it) }?.line
    val endLine = editor.selectionModel.selectionEndPosition?.let { editor.visualToLogicalPosition(it) }?.line

    when {
      startLine == null || endLine == null -> controller.selectionChanged(null, null)
      else -> controller.selectionChanged(startLine + 1, endLine + 1)
    }
  }

  fun line() = inlay.visualPosition.line
}
