package com.gitlab.plugin.chat.quickchat.ui.components

import com.gitlab.plugin.chat.quickchat.messages.QuickChatUIMessage
import com.gitlab.plugin.chat.quickchat.ui.*
import com.gitlab.plugin.chat.quickchat.ui.listeners.QuickChatExitKeyListener
import java.awt.BorderLayout
import java.awt.Dimension
import java.awt.FlowLayout
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import java.awt.event.KeyListener
import javax.swing.BorderFactory
import javax.swing.JPanel
import javax.swing.JTextField
import javax.swing.event.DocumentListener

@Suppress("MagicNumber")
class QuickChatBox(private val messageHandler: QuickChatUIMessageHandler) : JPanel(BorderLayout()) {
  private val textBox by lazy {
    JTextField().apply {
      border = BorderFactory.createEmptyBorder()
      background = quickChatBackground

      preferredSize = Dimension(QUICK_CHAT_WIDTH, 35)

      addKeyListener(QuickChatExitKeyListener(messageHandler))
      addKeyListener(SendChatKeyListener(messageHandler))
    }
  }

  init {
    border = BorderFactory.createLineBorder(quickChatPrimaryColor, 1, true)
    background = quickChatBackground

    add(textBox, BorderLayout.CENTER)
    add(shortcuts(), BorderLayout.EAST)
  }

  private fun shortcuts() = JPanel(FlowLayout(FlowLayout.CENTER, 5, 0)).apply {
    border = BorderFactory.createEmptyBorder(10, 5, 10, 5)
    background = quickChatBackground
    isOpaque = true

    add(ShortcutLabel("ENTER", "send"))
    add(ShortcutLabel("SLASH", "actions"))
  }

  fun onDocumentChange(listener: DocumentListener) {
    textBox.document.addDocumentListener(listener)
  }

  fun onKeyPress(listener: KeyListener) {
    textBox.addKeyListener(listener)
  }

  fun setText(text: String) {
    textBox.text = text
    requestFocus()
  }

  override fun requestFocus() {
    textBox.requestFocus()
  }

  private inner class SendChatKeyListener(private val messageHandler: QuickChatUIMessageHandler) : KeyAdapter() {
    private val prohibitedPrefixes = listOf("/clear", "/clean")

    override fun keyPressed(e: KeyEvent) {
      val canSubmit = textBox.document.getProperty(CAN_SUBMIT) as? Boolean ?: true

      if (!canSubmit) {
        return
      }

      val prompt = textBox.text
      if (e.keyCode == KeyEvent.VK_ENTER && prompt.isNotBlank()) {
        textBox.text = ""

        if (!prompt.hasProhibitedPrefix()) {
          messageHandler.receive(QuickChatUIMessage.NewUserPrompt(prompt))
        }
      }
    }

    private fun String.hasProhibitedPrefix() = prohibitedPrefixes.any { startsWith(it, ignoreCase = true) }
  }
}
