package com.gitlab.plugin.chat.model

import com.gitlab.plugin.chat.context.AiContextItem
import com.gitlab.plugin.services.GitLabServerService
import com.gitlab.plugin.services.VersionBasedFeature
import com.intellij.openapi.components.service
import com.intellij.util.application
import kotlinx.datetime.Clock
import kotlinx.datetime.LocalDateTime
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.descriptors.elementNames
import java.util.*

@Serializable
data class ChatRecordFileContext(
  val fileName: String,
  val selectedText: String,
  val contentAboveCursor: String? = null,
  val contentBelowCursor: String? = null
)

@Serializable
data class ChatRecordContext(
  val currentFile: ChatRecordFileContext
)

@Serializable
data class ChatRecord(
  val id: String = UUID.randomUUID().toString(),
  val role: Role,
  val type: Type = Type.GENERAL,
  val requestId: String,
  var state: State,
  var content: String? = null,
  var contentHtml: String? = null,
  var timestamp: LocalDateTime = Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()),
  var extras: Metadata? = null,
  val context: ChatRecordContext? = null,
  var chunkId: Int? = null
) {
  val errors: MutableList<String> = mutableListOf()

  companion object {
    fun pendingAssistantResponse(userChatRecord: ChatRecord): ChatRecord {
      require(userChatRecord.role == Role.USER) { "Assistant chat records can only be created from user chat records." }

      return ChatRecord(
        requestId = userChatRecord.requestId,
        role = Role.ASSISTANT,
        type = userChatRecord.type,
        state = State.PENDING
      )
    }
  }

  @Serializable
  data class Metadata(
    val sources: List<Source> = emptyList(),
    val contextItems: List<AiContextItem>?
  )

  @Serializable
  data class Source(val name: String, val url: String)

  @Serializable
  enum class State {
    @SerialName("pending")
    PENDING,

    @SerialName("ready")
    READY
  }

  @Serializable
  enum class Role {
    @SerialName("user")
    USER,

    @SerialName("assistant")
    ASSISTANT,

    @SerialName("system")
    SYSTEM;

    companion object {
      fun fromValue(value: String): Role = when (value) {
        "user" -> USER
        "assistant" -> ASSISTANT
        "system" -> SYSTEM
        else -> throw IllegalArgumentException("unknown value: $value")
      }
    }
  }

  @Serializable
  enum class Type {
    @SerialName("general")
    GENERAL,

    @SerialName("explainCode")
    EXPLAIN_CODE,

    @SerialName("help")
    HELP,

    @SerialName("generateTests")
    GENERATE_TESTS,

    @SerialName("fixCode")
    FIX_CODE,

    @SerialName("refactorCode")
    REFACTOR_CODE,

    @SerialName("newConversation")
    NEW_CONVERSATION,

    @SerialName("clearChat")
    CLEAR_CHAT;

    companion object {
      fun fromContent(content: String): Type {
        val gitlabServerService = application.service<GitLabServerService>()
        val trimmedContent = content.trim()

        return when (trimmedContent) {
          "/reset" -> NEW_CONVERSATION
          "/explain" -> EXPLAIN_CODE
          "/help" -> HELP
          "/refactor" -> REFACTOR_CODE
          "/tests" -> GENERATE_TESTS
          "/fix" -> FIX_CODE
          "/clear" -> CLEAR_CHAT
          "/clean" -> {
            if (gitlabServerService.isEnabled(VersionBasedFeature.REMOVE_CLEAN_COMMAND)) {
              GENERAL
            } else {
              CLEAR_CHAT
            }
          }
          else -> GENERAL
        }
      }

      fun getSerialName(recordType: ChatRecord.Type): String {
        val descriptor: SerialDescriptor = serializer().descriptor
        val index = recordType.ordinal
        return descriptor.elementNames.elementAt(index)
      }
    }
  }
}
