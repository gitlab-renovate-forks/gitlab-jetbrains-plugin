package com.gitlab.plugin.services

import com.gitlab.plugin.lsp.params.FeatureStateCheckParams
import com.gitlab.plugin.lsp.params.FeatureStateParams
import com.intellij.openapi.components.Service

@Service(Service.Level.PROJECT)
class CodeSuggestionsStateService {
  private var engagedChecks: List<FeatureStateCheckParams>? = null
  val codeSuggestionsEnabled: Boolean
    get() = engagedChecks?.isEmpty() ?: false

  fun update(state: FeatureStateParams) {
    engagedChecks = state.engagedChecks
  }

  fun getEngagedCheck() = engagedChecks?.firstOrNull()
}
