package com.gitlab.plugin.services.health

import com.gitlab.plugin.lsp.params.*
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.util.project.workspaceFolder
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project
import kotlinx.coroutines.future.asDeferred

@Service(Service.Level.PROJECT)
class ConfigurationValidationService(private val project: Project) {
  private val logger = logger<ConfigurationValidationService>()

  @Suppress("TooGenericExceptionCaught")
  suspend fun validateConfiguration(request: ConfigurationValidationRequest): Map<FeatureId, FeatureStateParams>? {
    val languageServer = project.service<GitLabLanguageServerService>().lspServer
    if (languageServer == null) {
      logger.info("Could not validate the configuration. Language Server is not started.")
      return null
    }

    logger.info("Validating configuration against the Language Server.")
    val featuresValidation = try {
      languageServer.validateConfiguration(
        Settings(
          baseUrl = request.baseUrl,
          token = request.token,
          codeCompletion = request.codeSuggestionsEnabled?.let { enabled ->
            CodeCompletion(enabled = enabled)
          },
          duoChat = request.duoChatEnabled?.let { enabled ->
            DuoChatFeature(enabled = enabled)
          },
          workspaceFolders = listOfNotNull(project.workspaceFolder)
        )
      ).asDeferred().await()
    } catch (e: Exception) {
      logger.info("Could not validate configuration because of an exception: ${e.message}")
      return null
    }

    val result = featuresValidation.associateBy { it.featureId }
    logger.info("Received configuration validation information for features: ${result.keys}.")
    return result
  }
}

data class ConfigurationValidationRequest(
  val baseUrl: String,
  val token: String,
  val codeSuggestionsEnabled: Boolean? = null,
  val duoChatEnabled: Boolean? = null
)
