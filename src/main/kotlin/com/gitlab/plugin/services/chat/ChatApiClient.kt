package com.gitlab.plugin.services.chat

import com.gitlab.plugin.api.GitLabChatRetryTimeoutException
import com.gitlab.plugin.api.duo.ExceptionHandler
import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.chat.api.model.AiActionResponse
import com.gitlab.plugin.chat.api.model.AiMessage
import com.gitlab.plugin.chat.api.model.toAiMessage
import com.gitlab.plugin.chat.context.AiContextItem
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.graphql.scalars.UserID
import com.gitlab.plugin.graphql.type.AiMessageRole
import com.gitlab.plugin.services.GitLabUserService
import com.gitlab.plugin.ui.CreateNotificationExceptionHandler
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.util.application
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlin.math.max

@Service(Service.Level.PROJECT)
internal class ChatApiClient(
  private val coroutineScope: CoroutineScope,
) {
  companion object {
    const val AI_RESPONSE_INITIAL_DELAY_MILLISECONDS = 500L
    const val AI_RESPONSE_MAX_TOTAL_DELAY_MILLISECONDS = 30_000L
  }

  private val userService: GitLabUserService = application.service<GitLabUserService>()
  private val exceptionHandler: ExceptionHandler = CreateNotificationExceptionHandler()
  private val logger = logger<ChatApiClient>()
  val canceledPromptRequestIds = mutableListOf<String>()

  suspend fun processNewUserPrompt(
    subscriptionId: String,
    question: String,
    context: ChatRecordContext? = null,
    aiContextItems: List<AiContextItem>? = null
  ): AiActionResponse? {
    val action = application
      .service<GraphQLApi>()
      .chatMutation(question, subscriptionId, context, aiContextItems)

    return action?.let {
      AiActionResponse(it)
    }
  }

  suspend fun subscribeToUpdates(
    subscriptionId: String,
    useAdditionalContext: Boolean,
    onMessageReceived: suspend (message: AiMessage) -> Unit
  ): Job? {
    val userId = userService.retrieveCurrentUser()?.id
      ?: throw CurrentUserNotFoundException("Could not find current user id in context")

    val subscription = application.service<GraphQLApi>().chatSubscription(
      subscriptionId,
      useAdditionalContext,
      UserID(userId)
    ) ?: return null

    return coroutineScope.launch {
      var shouldContinue = true
      subscription.takeWhile { shouldContinue }.collect {
        val message = it.data?.toAiMessage()

        // Ignore system notes for now as we don't have a way to display them and
        // this will result in an error as the caller of this code is not expecting
        // any responses for "system" role.
        if (message != null && !message.role.equals(AiMessageRole.SYSTEM.name, ignoreCase = true)) {
          if (canceledPromptRequestIds.contains(message.requestId)) {
            shouldContinue = false
            coroutineContext.cancel()
            return@collect
          }

          if (message.chunkId == null) {
            shouldContinue = false
            coroutineContext.cancel()
          }

          onMessageReceived(message)
        }
      }
    }
  }

  suspend fun getMessages(requestId: String, useAdditionalContext: Boolean): List<AiMessage> {
    return try {
      retryWithBackoff(timeoutException = GitLabChatRetryTimeoutException(requestId)) {
        val messages = application.service<GraphQLApi>().chatQuery(requestId, useAdditionalContext)

        val isComplete = messages.any { message -> message.role == AiMessageRole.ASSISTANT.name.lowercase() }
        Pair(messages, isComplete)
      }
    } catch (e: GitLabChatRetryTimeoutException) {
      exceptionHandler.handleException(e)
      emptyList()
    }
  }

  fun cancelPrompt(canceledPromptRequestId: String) {
    canceledPromptRequestIds.add(canceledPromptRequestId)
  }

  suspend fun clearChat(subscriptionId: String): AiActionResponse? {
    val action = application.service<GraphQLApi>().chatMutation("/clear", subscriptionId)

    if (action?.errors?.isNotEmpty() == true) {
      logger.warn("GraphQL API returned an error: ${action.errors.joinToString(", ")}")
    }

    return action?.let { AiActionResponse(it) }
  }

  // Retry given block with exponential backoff.
  // Will continue retrying until block returns a pair with true as the second value or
  // the total time spent retrying equals maxTotalDelayMilliseconds.
  private suspend fun <T> retryWithBackoff(
    timeoutException: Throwable,
    block: suspend () -> Pair<T, Boolean>
  ): T {
    val maxTotalDelayMilliseconds = AI_RESPONSE_MAX_TOTAL_DELAY_MILLISECONDS
    var currentDelayMilliseconds = AI_RESPONSE_INITIAL_DELAY_MILLISECONDS
    var totalDelayMilliseconds = currentDelayMilliseconds

    while (totalDelayMilliseconds < maxTotalDelayMilliseconds) {
      val (value, isComplete) = block()

      if (isComplete) {
        return value
      }

      delay(currentDelayMilliseconds)
      totalDelayMilliseconds += currentDelayMilliseconds
      currentDelayMilliseconds = (max(currentDelayMilliseconds, 1) * 2.0).toLong().coerceAtMost(maxTotalDelayMilliseconds - totalDelayMilliseconds)
    }

    logger.info("Retry failed after $maxTotalDelayMilliseconds milliseconds")
    throw timeoutException
  }
}

class CurrentUserNotFoundException(message: String) : Exception(message)
