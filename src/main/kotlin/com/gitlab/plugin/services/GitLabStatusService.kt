package com.gitlab.plugin.services

import com.gitlab.plugin.GitLabSettingsChangeEvent
import com.gitlab.plugin.GitLabSettingsListener
import com.intellij.openapi.components.Service
import com.intellij.util.application

@Service(Service.Level.APP)
class GitLabStatusService {

  fun gitlabStatusLoading() {
    gitlabStatusChangedNotify(Status.LOADING)
  }

  fun gitlabStatusEnabled() {
    gitlabStatusChangedNotify(Status.ENABLED)
  }

  fun gitlabStatusDisabled() {
    gitlabStatusChangedNotify(Status.DISABLED)
  }

  private fun gitlabStatusChangedNotify(status: Status) {
    application.messageBus.syncPublisher(GitLabSettingsListener.SETTINGS_CHANGED)
      .codeStyleSettingsChanged(GitLabSettingsChangeEvent(status))
  }
}

enum class Status {
  ENABLED,
  DISABLED,
  ERROR,
  LOADING,
  NOT_LICENSED,
  GITLAB_VERSION_UNSUPPORTED
}
