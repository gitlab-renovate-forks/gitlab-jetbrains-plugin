package com.gitlab.plugin.services

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.lsp.params.FeatureStateParams
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.Notification
import com.gitlab.plugin.ui.NotificationGroupType
import com.intellij.notification.NotificationType
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.Service.Level
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.util.application

@Service(Level.PROJECT)
class AuthenticationStateService(private val project: Project) {
  var authenticated: Boolean = false

  fun update(state: FeatureStateParams) {
    val currentlyAuthenticated = authenticated

    val engagedCheck = state.engagedChecks.firstOrNull()
    authenticated = engagedCheck == null

    if (authenticated != currentlyAuthenticated) {
      engagedCheck?.let { check ->
        GitLabNotificationManager().sendNotification(
          Notification(
            title = GitLabBundle.message("notification.title.gitlab-duo"),
            message = check.details ?: check.checkId
          ),
          notificationGroupType = NotificationGroupType.IMPORTANT,
          notificationType = NotificationType.ERROR,
          project = project
        )
      }
    }

    if (!authenticated) {
      application.service<GitLabStatusService>().gitlabStatusDisabled()
    } else {
      application.service<GitLabStatusService>().gitlabStatusEnabled()
    }
  }
}
