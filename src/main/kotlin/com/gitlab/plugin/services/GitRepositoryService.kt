package com.gitlab.plugin.services

import com.gitlab.plugin.exceptions.ExceptionTracker
import com.intellij.dvcs.repo.VcsRepositoryManager
import com.intellij.dvcs.repo.VcsRepositoryMappingListener
import com.intellij.openapi.Disposable
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.guessProjectDir
import com.intellij.openapi.util.Disposer
import com.intellij.openapi.vfs.VfsUtil
import com.intellij.openapi.vfs.VirtualFile
import com.intellij.util.application
import git4idea.repo.GitRepository
import git4idea.repo.GitRepositoryManager
import kotlinx.coroutines.*
import java.net.URL
import kotlin.coroutines.resume
import kotlin.time.Duration.Companion.seconds

private val REPOSITORY_MAPPING_TIMEOUT = 3.seconds

@Service(Service.Level.PROJECT)
class GitRepositoryService(
  private val project: Project,
  private val scope: CoroutineScope
) : Disposable {
  private val logger = logger<GitRepositoryService>()
  private val disposable = Disposer.newDisposable()

  /**
   * Retrieve the repository for current project and execute the block with it
   *
   * If the repository is already loaded the block is executed immediately,
   * otherwise it waits until it is fully loaded
   *
   * Any calls to repository manager should not be on EDT thread
   */
  suspend fun fetchRepository(): GitRepository? = scope.async {
    val projectDirectory = project.guessProjectDir() ?: return@async null

    val repository = getRepositoryForPath(projectDirectory)

    if (repository != null) {
      return@async repository
    }

    try {
      waitForVcsRepositoryMappingUpdated()
    } catch (e: TimeoutCancellationException) {
      logger.warn("Repository mappings update didn't happen", e)
    }

    return@async getRepositoryForPath(projectDirectory)
  }.await()

  /**
   * Find repository by URI
   */
  fun findRepository(repositoryUri: String): GitRepository? {
    val repositoryFile = VfsUtil.findFileByURL(URL(repositoryUri))
      ?: return null

    return GitRepositoryManager.getInstance(project).getRepositoryForFile(repositoryFile)
  }

  /**
   * Retrieves the repository for the given directory location
   */
  private fun getRepositoryForPath(projectDirectory: VirtualFile): GitRepository? {
    val repositoryManager = GitRepositoryManager.getInstance(project)
    return repositoryManager.getRepositoryForRoot(projectDirectory)
  }

  /**
   * Subscribes to VSC repository updates and waits for updates
   * or until the timeout is reached
   */
  private suspend fun waitForVcsRepositoryMappingUpdated(): Unit = withTimeout(REPOSITORY_MAPPING_TIMEOUT) {
    suspendCancellableCoroutine { continuation ->
      val busConnection = project.messageBus.connect()

      busConnection.subscribe(
        VcsRepositoryManager.VCS_REPOSITORY_MAPPING_UPDATED,
        VcsRepositoryMappingListener {
          busConnection.disconnect()

          try {
            continuation.resume(Unit)
          } catch (e: IllegalStateException) {
            if (e.message?.contains("Already resumed") == true) {
              logger.info("Wait for git repository mapping update already resumed.", e)
            } else {
              application.service<ExceptionTracker>().handle(e)
            }
          }
        }
      )

      continuation.invokeOnCancellation {
        busConnection.disconnect()
      }
    }
  }

  /**
   * Allow object to be disposed
   *
   * For more information see [Disposer and Disposable](https://plugins.jetbrains.com/docs/intellij/disposers.html)
   */
  override fun dispose() {
    disposable.dispose()
  }
}
