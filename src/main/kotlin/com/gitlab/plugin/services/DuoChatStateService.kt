package com.gitlab.plugin.services

import com.gitlab.plugin.actions.chat.DUO_CHAT_TOOL_WINDOW_ID
import com.gitlab.plugin.lsp.params.FeatureStateCheckParams
import com.gitlab.plugin.lsp.params.FeatureStateParams
import com.intellij.openapi.application.runInEdt
import com.intellij.openapi.components.Service
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindowManager

@Service(Service.Level.PROJECT)
class DuoChatStateService(private val project: Project) {
  private var engagedChecks: List<FeatureStateCheckParams>? = null
  val duoChatEnabled: Boolean
    get() = engagedChecks?.isEmpty() ?: false

  fun update(state: FeatureStateParams) {
    engagedChecks = state.engagedChecks

    val toolWindowManager = ToolWindowManager.getInstance(project)
    val toolWindow = toolWindowManager.getToolWindow(DUO_CHAT_TOOL_WINDOW_ID)

    if (toolWindow?.isDisposed == false) {
      runInEdt { toolWindow.isAvailable = duoChatEnabled }
    }
  }

  fun getEngagedCheck() = engagedChecks?.firstOrNull()
}
