package com.gitlab.plugin.util.code

import com.intellij.openapi.application.readAction
import com.intellij.openapi.command.WriteCommandAction.runWriteCommandAction
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiDocumentManager
import com.intellij.psi.PsiFile
import com.intellij.psi.PsiFileFactory
import com.intellij.psi.codeStyle.CodeStyleManager

class CodeFormatter {
  private val logger = logger<CodeFormatter>()

  suspend fun format(
    code: String,
    context: CodeFormattingContext
  ): String {
    val trimmedCode = when {
      context.isCurrentLineEmpty -> code.trimStart('\n', '\t', ' ').trimEnd('\n', '\t', ' ')
      else -> code.trimEnd('\n', '\t', ' ')
    }

    if (trimmedCode.isInlineCode()) {
      return trimmedCode
    }

    val project = context.project
    val inlinePart = trimmedCode.lines().first() + '\n' // re-add newline to inline part
    val blockPart = trimmedCode.drop(inlinePart.length)

    val content = StringBuilder().apply {
      append(context.prefix)
      append(inlinePart)
      append(blockPart)
      append(context.suffix)
    }

    val psiFile = readAction {
      PsiFileFactory
        .getInstance(project)
        .createFileFromText(context.language, content.toString())
    }

    val codeStartOffset = context.prefix.length + inlinePart.length
    val codeEndOffset = context.prefix.length + trimmedCode.length + 1 // format until after the suggestion
    runWriteCommandAction(project) {
      CodeStyleManager
        .getInstance(project)
        .adjustLineIndent(psiFile, TextRange(codeStartOffset, codeEndOffset))
    }

    val psiFileDocument = PsiDocumentManager.getInstance(project).getDocument(psiFile)
    if (psiFileDocument == null) {
      logger.error("Generated PsiFile has no document.")
      return trimmedCode
    }

    val indentedCodeEndOffset = psiFileDocument
      .getLineNumber(codeStartOffset)
      .let { startLine -> startLine - 1 + blockPart.lines().size }
      .let { endLine -> psiFileDocument.getLineEndOffset(endLine) }

    return inlinePart + psiFile.extractSuggestion(codeStartOffset, indentedCodeEndOffset)
  }

  private fun PsiFile.extractSuggestion(suggestionStartOffset: Int, suggestionEndOffset: Int): String {
    return text.filterIndexed { index, _ -> index in suggestionStartOffset until suggestionEndOffset }
  }

  private fun String.isInlineCode() = lines().size == 1
}
