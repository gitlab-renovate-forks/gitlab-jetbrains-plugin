package com.gitlab.plugin.util

import com.gitlab.plugin.GitLabBundle
import com.intellij.openapi.application.ApplicationInfo
import com.intellij.openapi.application.ApplicationNamesInfo

data class IdeMetadata(
  val name: String,
  val version: String,
  val vendor: String,
  val extensionName: String?,
  val extensionVersion: String?
) {
  companion object {
    fun get() = IdeMetadata(
      name = ApplicationNamesInfo.getInstance().fullProductName,
      version = ApplicationInfo.getInstance().build.asString(),
      vendor = ApplicationInfo.getInstance().shortCompanyName,
      extensionName = GitLabBundle.plugin().name,
      extensionVersion = GitLabBundle.plugin().version
    )
  }
}
