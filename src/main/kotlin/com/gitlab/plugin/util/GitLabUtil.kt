package com.gitlab.plugin.util

import com.gitlab.plugin.GitLabBundle
import com.intellij.collaboration.api.httpclient.HttpClientUtil

object GitLabUtil {
  const val SERVICE_NAME = "com.gitlab.plugin"
  const val GITLAB_DEFAULT_URL = "https://gitlab.com"

  val userAgent by lazy {
    HttpClientUtil.getUserAgentValue("gitlab-jetbrains-plugin/${GitLabBundle.plugin().version ?: "DEV"}")
  }
}
