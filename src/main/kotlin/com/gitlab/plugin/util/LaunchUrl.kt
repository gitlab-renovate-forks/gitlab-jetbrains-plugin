package com.gitlab.plugin.util

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.intellij.ide.BrowserUtil

fun launchUrl(url: String) = BrowserUtil.browse(url)

object Urls {
  const val CODE_SUGGESTIONS_SETUP_DOCS =
    "https://docs.gitlab.com/ee/user/gitlab_duo/turn_on_off.html"

  const val JETBRAINS_1PASSWORD_CLI_INTEGRATION =
    "https://docs.gitlab.com/ee/editor_extensions/jetbrains_ide/index.html#integrate-with-1password-cli"

  val GENERATE_TOKEN =
    "${DuoPersistentSettings.getInstance().url}/-/user_settings/personal_access_tokens?name=GitLab%20Duo%20For%20JetBrains&scopes=api"

  const val PLUGIN_SETUP_INSTRUCTIONS =
    "https://gitlab.com/gitlab-renovate-forks/gitlab-jetbrains-plugin/-/blob/main/README.md#setup"

  const val SUPPORTED_LANGUAGES_DOC =
    "https://docs.gitlab.com/ee/user/project/repository/code_suggestions/supported_extensions.html#supported-languages"
}
