package com.gitlab.plugin.util

import com.gitlab.plugin.authentication.PatProvider
import com.gitlab.plugin.authentication.TokenProviderType
import com.gitlab.plugin.services.GitLabUserService
import com.gitlab.plugin.workspace.DuoConfigurationChangedListener
import com.gitlab.plugin.workspace.WorkspaceSettings
import com.intellij.openapi.components.service
import com.intellij.openapi.progress.runBackgroundableTask
import com.intellij.util.application
import kotlinx.coroutines.runBlocking

class TokenUpdatedDuoConfigurationChangedListener : DuoConfigurationChangedListener {

  override fun onConfigurationChange(settings: WorkspaceSettings) {
    when (settings.tokenProviderType) {
      TokenProviderType.PAT -> TokenUtil.setToken(settings.token)
      TokenProviderType.ONE_PASSWORD -> TokenUtil.setToken("")
      else -> return updateDuoFeaturesStatus()
    }

    if (settings.token.isNotBlank()) {
      runBackgroundableTask(title = "Refreshing GitLab personal access token status", cancellable = false) {
        application.service<PatProvider>().cacheToken(settings.token)
        updateDuoFeaturesStatus()
      }
    }
  }

  private fun updateDuoFeaturesStatus() {
    runBlocking {
      application.service<GitLabUserService>().invalidateAndFetchCurrentUser()
    }
  }
}
