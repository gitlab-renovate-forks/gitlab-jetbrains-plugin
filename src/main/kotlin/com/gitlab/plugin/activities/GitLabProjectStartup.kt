package com.gitlab.plugin.activities

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.authentication.OAuthTokenProvider
import com.gitlab.plugin.editor.EditorRegistryService
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.services.GitLabStatusService
import com.gitlab.plugin.services.GitLabUserService
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.Notification
import com.gitlab.plugin.ui.NotificationGroupType
import com.gitlab.plugin.util.parseVersion
import com.intellij.ide.util.PropertiesComponent
import com.intellij.notification.NotificationType
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.ProjectManager
import com.intellij.openapi.startup.ProjectActivity
import com.intellij.util.application

internal class GitLabProjectStartup : ProjectActivity {
  companion object {
    const val DUO_PLUGIN_LAST_INSTALLED_VERSION = "com.gitlab.plugin.last.installed.version"
  }

  private val applicationLevelProperties = PropertiesComponent.getInstance()

  override suspend fun execute(project: Project) {
    application.service<GitLabStatusService>().gitlabStatusLoading()

    application.executeOnPooledThread {
      // Stop the Language Server possibly process started for the default project.
      // This scenario can happen if a user opens the settings panel when no projects are open.
      ProjectManager.getInstance().defaultProject.service<GitLabLanguageServerService>().stopServer()

      project.service<GitLabLanguageServerService>().startServer()
    }

    application.service<GitLabUserService>().invalidateAndFetchCurrentUser()

    // The token refresh task has to be restarted after an IDE restart since all the threads are terminated at restart.
    if (DuoPersistentSettings.getInstance().oauthEnabled) {
      application.service<OAuthTokenProvider>().startTokenRefreshTimer()
    }

    getDuoReadyToUseNotification()?.let {
      GitLabNotificationManager().sendNotification(
        it,
        NotificationGroupType.GENERAL,
        NotificationType.INFORMATION
      )
    }

    project.service<EditorRegistryService>().registerAllEditors()
  }

  private fun getDuoReadyToUseNotification(): Notification? {
    val lastVersion = applicationLevelProperties.getValue(DUO_PLUGIN_LAST_INSTALLED_VERSION, "")
    val currentVersion = GitLabBundle.plugin().version?.let { parseVersion(it) }.orEmpty()

    if (currentVersion == lastVersion) {
      return null
    }

    applicationLevelProperties.setValue(DUO_PLUGIN_LAST_INSTALLED_VERSION, currentVersion)
    return Notification(
      GitLabBundle.message("notification.startup.get-started.title"),
      GitLabBundle.message("notification.startup.get-started.message", currentVersion)
    )
  }
}
