package com.gitlab.plugin.authentication

import com.gitlab.plugin.util.TokenUtil
import com.intellij.collaboration.auth.services.OAuthCredentialsAcquirer
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.util.application
import java.time.Duration
import java.time.Instant
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

@Service(Service.Level.APP)
class OAuthTokenProvider : TokenProvider {
  private val logger = logger<OAuthTokenProvider>()
  private var currentToken: GitLabAuthorizationToken? = null
  var scheduler: ScheduledExecutorService = Executors.newScheduledThreadPool(1)

  override fun token(): String {
    if (currentToken == null) {
      loadCachedToken()
    } else {
      refreshTokenIfExpired()
    }

    return currentToken?.accessToken.orEmpty()
  }

  fun updateToken(newToken: GitLabAuthorizationToken?) {
    this.currentToken = newToken
    TokenUtil.setOAuthToken(newToken)
    application.service<TokenUpdateLanguageServerService>().updateTokenLSConfiguration(token())
    if (newToken == null) DuoPersistentSettings.getInstance().oauthEnabled = false
  }

  private fun loadCachedToken() {
    if (currentToken != null || !DuoPersistentSettings.getInstance().oauthEnabled) return

    TokenUtil.getOAuthToken()?.let { cachedToken ->
      logger.info(
        "Loading cached token from PasswordSafe. Expiration timestamp is ${cachedToken.tokenExpirationTimestamp}"
      )
      currentToken = cachedToken
      refreshTokenIfExpired()
    } ?: run {
      logger.info(
        "No cached token found in PasswordSafe. Updating settings to reflect that OAuth is no longer enabled."
      )
      DuoPersistentSettings.getInstance().oauthEnabled = false
    }
  }

  private fun refreshTokenIfExpired() {
    val tokenExpirationTimestamp = Instant.ofEpochSecond(currentToken?.tokenExpirationTimestamp ?: 0L)
    // Always check if the token is expired first
    if (tokenExpirationTimestamp > Instant.now()) return

    logger.info("Refreshing expired token with timestamp $tokenExpirationTimestamp.")

    val refreshResult = application.service<GitLabOAuthService>().refreshToken(currentToken)
    val tokenForUpdate = when (refreshResult) {
      is OAuthCredentialsAcquirer.AcquireCredentialsResult.Success -> {
        val newToken = refreshResult.credentials
        val newTokenExpirationTimestamp = Instant.ofEpochSecond(newToken.tokenExpirationTimestamp)

        logger.info("The OAuth token has been refreshed and it expires at $newTokenExpirationTimestamp.")
        newToken
      }

      is OAuthCredentialsAcquirer.AcquireCredentialsResult.Error -> {
        logger.info(refreshResult.description)

        // Currently all JetBrains IDEs share a single stored token in PasswordSafe, so if the refresh fails,
        // we need to check if another IDE has already updated it
        TokenUtil.getOAuthToken()?.let { storedToken ->
          val storedTokenExpirationTimestamp = Instant.ofEpochSecond(storedToken.tokenExpirationTimestamp)
          if (storedTokenExpirationTimestamp > Instant.now()) {
            logger.info(
              "The PasswordSafe OAuth token has been refreshed and it expires at $storedTokenExpirationTimestamp"
            )
            storedToken
          } else {
            null
          }
        }
      }
    }

    // Update even if null so we don't get stuck redoing refreshes with a bad refresh token
    updateToken(tokenForUpdate)
  }

  fun startTokenRefreshTimer(
    refreshIntervalInSeconds: Int = currentToken?.expiresIn ?: DEFAULT_REFRESH_INTERVAL_SECONDS
  ) {
    if (!DuoPersistentSettings.getInstance().oauthEnabled) {
      logger.info("Canceling the timer for token refresh.")
      scheduler.shutdownNow()
      return
    }

    val refreshIntervalInMillis = Duration.ofSeconds(refreshIntervalInSeconds.toLong()).toMillis()

    scheduler.scheduleAtFixedRate({
      if (currentToken != null) {
        refreshTokenIfExpired()
        logger.info("Token refreshed by scheduled task.")
      }
    }, 0, refreshIntervalInMillis, TimeUnit.MILLISECONDS)
  }

  companion object {
    const val DEFAULT_REFRESH_INTERVAL_SECONDS: Int = 7200
  }
}
