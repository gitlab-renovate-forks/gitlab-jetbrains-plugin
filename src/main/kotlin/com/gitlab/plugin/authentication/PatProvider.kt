package com.gitlab.plugin.authentication

import com.gitlab.plugin.util.TokenUtil
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.util.application

@Service(Service.Level.APP)
class PatProvider : TokenProvider {

  private var token = ""

  override fun token(): String = token(slowOperationsPermitted = true)

  /**
   * The slowOperationsPermitted parameter allows slower operations like keychain or 1Password lookups to be executed.
   * This parameter should be true when used in background thread calls where we want to retrieve the token if the cache is empty.
   * It should be false when used in UI thread calls where we just want to check the cached value.
   */
  fun token(slowOperationsPermitted: Boolean): String {
    if (token.isNotBlank()) {
      return token
    }

    return if (slowOperationsPermitted) {
      token = if (application.service<DuoPersistentSettings>().integrate1PasswordCLI) {
        application.service<OnePasswordTokenProvider>().token()
      } else {
        TokenUtil.getToken().orEmpty()
      }

      token
    } else {
      ""
    }
  }

  fun cacheToken(newToken: String) {
    token = newToken
  }
}
