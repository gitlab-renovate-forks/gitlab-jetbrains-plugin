package com.gitlab.plugin.authentication

import com.intellij.collaboration.auth.credentials.Credentials
import com.intellij.collaboration.auth.services.*
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.util.Url
import com.intellij.util.Urls.newFromEncoded
import com.intellij.util.application
import com.intellij.util.io.DigestUtil
import io.ktor.http.*
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import org.jetbrains.ide.BuiltInServerManager
import org.jetbrains.ide.RestService
import java.util.*
import java.util.concurrent.CompletableFuture

@Service(Service.Level.APP)
class GitLabOAuthService : OAuthServiceBase<Credentials>() {
  private val jsonSerializer = Json { ignoreUnknownKeys = true }
  private val logger = logger<GitLabOAuthService>()
  private val expiredRequests = mutableListOf<OAuthRequestWithResult<Credentials>?>()

  override val name: String
    get() = "GitLabOAuthService"

  override fun revokeToken(token: String) {
    DuoPersistentSettings.getInstance().oauthEnabled = false
    revokeTokenGitLab(token)

    application.service<OAuthTokenProvider>().updateToken(null)

    logger.info("Revoke token on server side is not implemented.")
  }

  fun authorize(): CompletableFuture<Credentials> {
    // Allow user to open the auth page in the browser even if the old request was never completed
    if (currentRequest.get() != null) {
      expiredRequests.add(currentRequest.get())
      currentRequest.get()?.result?.completeExceptionally(
        RuntimeException("Request expired due to more recent authorization attempt")
      )
    }

    val request = GitLabOAuthRequest()
    val result = super.authorize(request)
    return result
  }

  fun refreshToken(currentToken: GitLabAuthorizationToken?): OAuthCredentialsAcquirer.AcquireCredentialsResult<GitLabAuthorizationToken> {
    val tokenUrl = GITLAB_TOKEN_URL.addParameters(
      mapOf(
        "client_id" to CLIENT_ID,
        "refresh_token" to currentToken?.refreshToken,
        "grant_type" to "refresh_token",
        "redirect_uri" to REDIRECT_URL.toExternalForm(),
        "Content-Type" to "application/x-www-form-urlencoded",
      )
    )

    return try {
      val responseToken = OAuthCredentialsAcquirerHttp.requestToken(tokenUrl)

      when (responseToken.statusCode()) {
        HttpStatusCode.OK.value -> {
          val credentials = jsonSerializer.decodeFromString<GitLabAuthorizationToken>(responseToken.body())
          OAuthCredentialsAcquirer.AcquireCredentialsResult.Success(credentials)
        }
        else -> OAuthCredentialsAcquirer.AcquireCredentialsResult.Error(
          "Refresh token response was unsuccessful: ${responseToken.statusCode()}; ${responseToken.body()}"
        )
      }
    } catch (e: SerializationException) {
      OAuthCredentialsAcquirer.AcquireCredentialsResult.Error(e.message ?: "No error message available")
    }
  }

  private fun revokeTokenGitLab(token: String) {
    val tokenUrl = GITLAB_REVOKE_TOKEN_URL.addParameters(
      mapOf(
        "client_id" to CLIENT_ID,
        "token" to token,
        "Content-Type" to "application/x-www-form-urlencoded",
      )
    )

    val response = OAuthCredentialsAcquirerHttp.requestToken(tokenUrl)
    if (response.statusCode() == HttpStatusCode.OK.value) {
      logger.info("Revoke token on server side was successful.")
    } else {
      logger.info("Revoke token on server side was unsuccessful.")
    }
  }

  // Checking that the response from the server is sending the same CSRF token back in the state parameter
  override fun handleOAuthServerCallback(
    path: String,
    parameters: Map<String, List<String>>
  ): OAuthService.OAuthResult<Credentials>? {
    val request = currentRequest.get() ?: return null
    val gitLabOAuthRequest = request.request as? GitLabOAuthRequest ?: return null

    val callbackState = parameters["state"]?.firstOrNull()
    if (gitLabOAuthRequest.state != callbackState) {
      // Terminate the current request unless an expired request matches the received CSRF, which means the user attempted
      // to authorize in an older browser tab than most recent one. This attempt is invalid, but we should not terminate
      // the current request so the user can still complete authorization in the newer tab.
      expiredRequests.find { expiredRequest ->
        (expiredRequest?.request as? GitLabOAuthRequest)?.state == callbackState
      }.also { expiredRequest ->
        if (expiredRequest != null) {
          expiredRequests.remove(expiredRequest)
        } else {
          request.result.completeExceptionally(RuntimeException("Invalid CSRF token"))
        }
      }

      return OAuthService.OAuthResult(request.request, false)
    }

    return super.handleOAuthServerCallback(path, parameters)
  }

  private class GitLabOAuthRequest : OAuthRequest<Credentials> {
    val state = PkceUtils.generateCodeVerifier()
    private val codeVerifier = PkceUtils.generateCodeVerifier()
    private val codeChallenge = computeCodeChallenge(codeVerifier)

    override val authorizationCodeUrl: Url
      get() = REDIRECT_URL

    override val credentialsAcquirer: OAuthCredentialsAcquirer<Credentials> =
      GitLabOAuthCredentialsAcquirer(codeVerifier)

    override val authUrlWithParameters: Url = GITLAB_OAUTH_URL.addParameters(
      mapOf(
        "client_id" to CLIENT_ID,
        "code_verifier" to codeVerifier,
        "code_challenge" to codeChallenge,
        "state" to state,
        "redirect_uri" to authorizationCodeUrl.toExternalForm(),
        "code_challenge_method" to "S256",
        "scope" to "api",
        "response_type" to "code"
      )
    )

    private fun computeCodeChallenge(codeVerifier: String): String {
      val shaCodeVerifier: ByteArray = DigestUtil.sha256().digest(codeVerifier.toByteArray())
      return Base64.getUrlEncoder().withoutPadding().encode(shaCodeVerifier).toString(Charsets.UTF_8)
    }
  }

  private class GitLabOAuthCredentialsAcquirer(private val codeVerifier: String) :
    OAuthCredentialsAcquirer<Credentials> {
    private val jsonSerializer = Json { ignoreUnknownKeys = true }

    override fun acquireCredentials(code: String): OAuthCredentialsAcquirer.AcquireCredentialsResult<Credentials> {
      val tokenUrl = GITLAB_TOKEN_URL.addParameters(
        mapOf(
          "client_id" to CLIENT_ID,
          "code" to code,
          "code_verifier" to codeVerifier,
          "redirect_uri" to REDIRECT_URL.toExternalForm(),
          "grant_type" to "authorization_code",
          "Content-Type" to "application/x-www-form-urlencoded",
        )
      )
      val responseToken = OAuthCredentialsAcquirerHttp.requestToken(tokenUrl)
      val credentials = jsonSerializer.decodeFromString<GitLabAuthorizationToken>(responseToken.body())

      // Update the token from the OAuth provider
      application.service<OAuthTokenProvider>().updateToken(credentials)
      DuoPersistentSettings.getInstance().oauthEnabled = true

      // Start token refresh timer
      application.service<OAuthTokenProvider>()
        .startTokenRefreshTimer(
          credentials.expiresIn
        )

      return OAuthCredentialsAcquirer.AcquireCredentialsResult.Success(credentials)
    }
  }

  companion object {
    const val SERVICE_NAME = "oauth/gitlab"
    private const val CLIENT_ID = "732efe7d78ba62b01d2a95cd3b02f3ca2a00c7cec32c01e060c1b7664a9aa367"
    private val GITLAB_OAUTH_URL = newFromEncoded("https://gitlab.com/oauth/authorize")
    private val GITLAB_TOKEN_URL = newFromEncoded("https://gitlab.com/oauth/token")
    private val GITLAB_REVOKE_TOKEN_URL = newFromEncoded("https://gitlab.com/oauth/revoke")
    private val REDIRECT_URL: Url
      get() = newFromEncoded(
        "http://127.0.0.1:${BuiltInServerManager.getInstance().port}/${RestService.PREFIX}/$SERVICE_NAME/authorization"
      )
  }
}
