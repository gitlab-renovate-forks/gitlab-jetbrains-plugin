package com.gitlab.plugin.authentication

import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.util.application

@Service(Service.Level.APP)
class GitLabTokenProviderManager {
  private val tokenProviders: Map<TokenProviderType, TokenProvider> = mapOf(
    Pair(TokenProviderType.OAUTH, application.service<OAuthTokenProvider>()),
    Pair(TokenProviderType.PAT, application.service<PatProvider>())
  )

  fun getToken(tokenProviderType: TokenProviderType = TokenProviderType.OAUTH): String {
    val tokenByType = tokenProviders[tokenProviderType]?.token()
    if (tokenByType?.isNotEmpty() == true) {
      return tokenByType
    }

    for (provider in tokenProviders) {
      val currentToken = provider.value.token()
      if (currentToken.isNotEmpty()) {
        return currentToken
      }
    }
    return ""
  }
}

enum class TokenProviderType {
  PAT,
  OAUTH,
  ONE_PASSWORD
}
