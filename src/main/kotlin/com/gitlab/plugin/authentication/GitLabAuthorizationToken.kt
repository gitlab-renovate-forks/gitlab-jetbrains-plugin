package com.gitlab.plugin.authentication

import com.intellij.collaboration.auth.credentials.Credentials
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class GitLabAuthorizationToken(
  @SerialName("access_token")
  override val accessToken: String,
  @SerialName("refresh_token")
  val refreshToken: String,
  @SerialName("expires_in")
  val expiresIn: Int,
  @SerialName("created_at")
  val createdAt: Long,

  // Making the token expired earlier to prevent the scenario where a delayed request might send an expired token to the server
  private val tokenExpirationBufferSeconds: Int = 120,
  val tokenExpirationTimestamp: Long = expiresIn.let { createdAt.plus(it).minus(tokenExpirationBufferSeconds) }
) : Credentials
