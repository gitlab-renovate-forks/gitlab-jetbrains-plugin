package com.gitlab.plugin.authentication

import com.gitlab.plugin.lsp.params.Settings
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.project.ProjectManager
import com.intellij.util.application
import org.eclipse.lsp4j.DidChangeConfigurationParams

@Service(Service.Level.APP)
class TokenUpdateLanguageServerService {
  fun updateTokenLSConfiguration(newToken: String) {
    ProjectManager.getInstance().openProjects.forEach { project ->
      project.service<GitLabLanguageServerService>().lspServer?.workspaceService?.didChangeConfiguration(
        DidChangeConfigurationParams(
          Settings(
            token = newToken.ifEmpty { application.service<GitLabTokenProviderManager>().getToken() },
          )
        )
      )
    }
  }
}
