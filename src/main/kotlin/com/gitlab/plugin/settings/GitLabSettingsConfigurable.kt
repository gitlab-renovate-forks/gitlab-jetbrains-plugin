package com.gitlab.plugin.settings

import com.gitlab.plugin.BuildConfig
import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.GitLabBundle.message
import com.gitlab.plugin.authentication.*
import com.gitlab.plugin.authentication.DuoPersistentSettings.Companion.GITLAB_REALM_SAAS
import com.gitlab.plugin.language.Language
import com.gitlab.plugin.lsp.listeners.LanguageServerSettingsChangedListener
import com.gitlab.plugin.lsp.params.FeatureId
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.lsp.settings.CodeCompletion
import com.gitlab.plugin.lsp.settings.FeatureFlags
import com.gitlab.plugin.lsp.settings.HttpAgentOptions
import com.gitlab.plugin.lsp.settings.LanguageServerSettings
import com.gitlab.plugin.services.health.ConfigurationValidationRequest
import com.gitlab.plugin.services.health.ConfigurationValidationService
import com.gitlab.plugin.telemetry.tracked.actions.trackAuthenticationType
import com.gitlab.plugin.ui.GitLabIcons
import com.gitlab.plugin.util.Urls
import com.gitlab.plugin.util.launchUrl
import com.gitlab.plugin.util.removeTrailingSlash
import com.gitlab.plugin.workspace.DuoConfigurationChangedListener
import com.gitlab.plugin.workspace.Telemetry
import com.gitlab.plugin.workspace.WorkspaceSettings
import com.intellij.icons.AllIcons
import com.intellij.openapi.application.invokeLater
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.observable.properties.PropertyGraph
import com.intellij.openapi.options.BoundConfigurable
import com.intellij.openapi.options.Configurable
import com.intellij.openapi.options.ConfigurationException
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.ComboBox
import com.intellij.openapi.ui.DialogPanel
import com.intellij.ui.SimpleTextAttributes.GRAY_ATTRIBUTES
import com.intellij.ui.components.BrowserLink
import com.intellij.ui.components.JBCheckBox
import com.intellij.ui.components.JBPasswordField
import com.intellij.ui.components.JBTextField
import com.intellij.ui.dsl.builder.*
import com.intellij.ui.dsl.builder.Align.Companion.FILL
import com.intellij.util.application
import com.intellij.util.ui.StatusText
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.jetbrains.annotations.VisibleForTesting
import java.awt.Font
import javax.swing.*

@Suppress("TooManyFunctions", "LargeClass")
class GitLabSettingsConfigurable(
  private val project: Project,
  private val coroutineScope: CoroutineScope,
) : BoundConfigurable(message("settings.ui.group.name")) {
  private lateinit var settings: DuoPersistentSettings

  private val patProvider = application.service<PatProvider>()
  private val lspSettings = project.service<LanguageServerSettings>()
  private val languageServerService get() = project.service<GitLabLanguageServerService>()

  private val tokenProviderManager by lazy {
    application.service<GitLabTokenProviderManager>()
  }

  private val urlErrorMessage = message("settings.ui.error.url.invalid")
  private val tokenLabel = message("settings.ui.gitlab.token")
  private val createTokenLabel = message("settings.ui.gitlab.create-token")
  private val hostLabel = message("settings.ui.gitlab.url")
  private val docsLabel = message("settings.ui.gitlab.docs-link-label")
  private val telemetryLabel = message("settings.ui.gitlab.enable-telemetry")
  private val telemetryComment = message("settings.ui.gitlab.telemetry-comment")
  private val additionalLanguagesComment = message("settings.ui.gitlab.additional-languages-comment")
  private val verifySetupLabel = message("settings.ui.gitlab.verify-setup")
  private val ignoreCertificateErrorsLabel = message("settings.ui.gitlab.ignore-certificate-errors-label")
  private val ignoreCertificateErrorsDescription = message(
    "settings.ui.gitlab.ignore-certificate-errors-description"
  )
  private val codeSuggestionsEnabledLabel = message("settings.ui.gitlab.enable-duo-code-suggestions")
  private val duoChatEnabledLabel = message("settings.ui.gitlab.enable-duo-chat")
  private val tabPanels = mutableListOf<DialogPanel>()
  private val logger = logger<GitLabSettingsConfigurable>()
  private val propertyGraph = PropertyGraph()
  private val authenticatedIcon = GitLabIcons.Status.Success

  private var settingsPanel: DialogPanel? = null
  private var oauthEnabledGraphProperty = propertyGraph.property(false)
  private val onePasswordAccounts = propertyGraph.property(listOf<OnePasswordTokenProvider.Account>())

  private lateinit var urlField: Cell<JBTextField>
  private lateinit var codeSuggestionsEnabledCheckbox: Cell<JBCheckBox>
  private lateinit var duoChatEnabledCheckbox: Cell<JBCheckBox>

  private lateinit var errorReportingInstructions: Cell<BrowserLink>
  private lateinit var patSection: Row
  private lateinit var integrate1PasswordSettings: Row
  private lateinit var onePasswordAccount: Row
  private lateinit var onePasswordNoAccountsWarningRow: Row
  private lateinit var secretReferenceRow: Row
  private lateinit var patField: Cell<JBPasswordField>
  private lateinit var patFieldEmptyText: StatusText
  private lateinit var verifyButton: Cell<JButton>
  private lateinit var integrate1PasswordCheckbox: Cell<JBCheckBox>
  private lateinit var integrate1PasswordSecretReferenceField: Cell<JTextField>
  private lateinit var integrate1PasswordDropdown: Cell<ComboBox<*>>
  private lateinit var tokenText: String
  private lateinit var healthCheck: Row
  private lateinit var tokenHealth: Cell<JLabel>
  private lateinit var codeSuggestionHealth: Cell<JLabel>
  private lateinit var duoChatHealth: Cell<JLabel>
  private lateinit var verifyLabel: Cell<JLabel>
  private lateinit var cannotVerifyLabel: Cell<JLabel>
  private lateinit var settingsHealthChecker: SettingsHealthChecker

  @VisibleForTesting
  internal lateinit var authTabbedPane: Cell<JTabbedPane>

  private val didConfigurationChangeListener = application
    .messageBus
    .syncPublisher(DuoConfigurationChangedListener.DID_CHANGE_CONFIGURATION_TOPIC)

  private val lspSettingsChangedPublisher by lazy {
    project
      .messageBus
      .syncPublisher(LanguageServerSettingsChangedListener.LANGUAGE_SERVER_SETTINGS_CHANGED_TOPIC)
  }

  override fun createPanel(): DialogPanel {
    ensureLanguageServerStarted()

    settings = DuoPersistentSettings.getInstance()

    settingsPanel = panel {
      setupInstructionsLinkRow()
      connectionGroup()
      authenticationGroup()
      healthCheck = healthCheckGroup()
      featuresGroup()
      languagesGroup(codeCompletion = lspSettings.state.workspaceSettings.codeCompletion)
      advancedGroup()
    }.apply {
      tabPanels.forEach { tabPanel ->
        registerIntegratedPanel(tabPanel)
      }
    }

    settingsHealthChecker = createSettingsHealthChecker()

    fetchToken()

    return settingsPanel as DialogPanel
  }

  override fun apply() {
    val panel = settingsPanel ?: return
    val validationMessages = panel.validateAll()

    if (validationMessages.isEmpty()) {
      super.apply()

      val workspaceSettings: WorkspaceSettings = createWorkspaceSettings()
      didConfigurationChangeListener.onConfigurationChange(workspaceSettings)
      trackAuthenticationType(workspaceSettings.tokenProviderType)
      logger.info("Saved authentication method ${workspaceSettings.tokenProviderType}.")

      updateAuthenticatedIcon()

      coroutineScope.launch {
        if (languageServerService.serverRunning) {
          lspSettingsChangedPublisher.onLanguageServerSettingsChanged()
        } else {
          languageServerService.startServer()
        }
      }
    } else {
      throw ConfigurationException(validationMessages.joinToString(separator = "\n") { it.message })
    }
  }

  private fun Panel.setupInstructionsLinkRow() {
    row { browserLink(docsLabel, Urls.CODE_SUGGESTIONS_SETUP_DOCS) }
  }

  private fun Panel.connectionGroup() {
    groupRowsRange(message("settings.ui.gitlab.connection-section-title")) {
      gitLabUrlRow()
      ignoreCertificateErrorsRow()
    }
  }

  private fun Panel.authenticationGroup() {
    groupRowsRange(message("settings.ui.gitlab.authentication-section-title")) {
      authenticationTabsRow()
      verifySetupRow()
    }
  }

  private fun Panel.authenticationTabsRow() {
    oauthEnabledGraphProperty = propertyGraph.property(settings.oauthEnabled)

    row {
      authTabbedPane = cell(
        JTabbedPane().apply {
          addTab(
            message("settings.ui.gitlab.pat-tab-heading"),
            authenticationPanel {
              oauthEnabledCommentRow()
              patSection = patSectionRow()
            }
          )
          addTab(
            message("settings.ui.gitlab.1password-tab-heading"),
            authenticationPanel {
              integrate1PasswordSettings = integrate1PasswordSettingsRow()
              onePasswordNoAccountsWarningRow = onePasswordNoAccountsWarningRow()
              onePasswordAccount = onePasswordAccountRow()
              secretReferenceRow = secretReferenceRow()
            }
          )

          addOrRemoveOAuthTab()

          setSelectedIndex(
            when {
              settings.oauthEnabled -> oauthTabIndex()
              settings.integrate1PasswordCLI -> onePasswordTabIndex()
              else -> patTabIndex()
            }
          )
        }
      )

      updateAuthenticatedIcon()
    }

    if (authTabbedPane.component.selectedIndex == authTabbedPane.component.onePasswordTabIndex()) {
      fetch1PasswordAccounts()
    }

    authTabbedPane.component.addChangeListener {
      verifyButton.visible(
        (authTabbedPane.component.selectedIndex == authTabbedPane.component.oauthTabIndex() && settings.oauthEnabled) ||
          (authTabbedPane.component.selectedIndex == authTabbedPane.component.patTabIndex() && !settings.oauthEnabled) ||
          (authTabbedPane.component.selectedIndex == authTabbedPane.component.onePasswordTabIndex() && !settings.oauthEnabled)
      )

      patSection.enabled(!settings.oauthEnabled)
      integrate1PasswordSettings.enabled(!settings.oauthEnabled)
      onePasswordAccount.enabled(!settings.oauthEnabled)
      secretReferenceRow.enabled(!settings.oauthEnabled)

      fetch1PasswordAccounts()
    }
  }

  private fun authenticationPanel(init: Panel.() -> Unit): DialogPanel {
    return panel {
      init()
    }.also { tabPanels.add(it) }
  }

  private fun Panel.gitLabUrlRow() {
    row(hostLabel) {
      urlField = textField().bindText(settings::url)
        .addValidationRule(urlErrorMessage) { it.text.isBlank() }
        .onChanged { authTabbedPane.component.addOrRemoveOAuthTab() }
        .also { urlField ->
          urlField.onApply {
            urlField.text(urlField.component.text.removeTrailingSlash())
          }
        }
    }
  }

  private fun JTabbedPane.oauthTabIndex(): Int {
    return indexOfTab(message("settings.ui.gitlab.oauth-tab-heading"))
  }

  private fun JTabbedPane.patTabIndex(): Int {
    return indexOfTab(message("settings.ui.gitlab.pat-tab-heading"))
  }

  private fun JTabbedPane.onePasswordTabIndex(): Int {
    return indexOfTab(message("settings.ui.gitlab.1password-tab-heading"))
  }

  private fun JTabbedPane.addOrRemoveOAuthTab(initialOAuthTabIndex: Int = 0) {
    val currentOAuthTabIndex = oauthTabIndex() // will be -1 if no tab has the given title
    val oauthTabCurrentlyShown = currentOAuthTabIndex != -1
    val shouldShowOAuthTab = settings.gitlabRealm() == GITLAB_REALM_SAAS

    when {
      shouldShowOAuthTab && !oauthTabCurrentlyShown -> {
        insertTab(
          message("settings.ui.gitlab.oauth-tab-heading"),
          if (oauthEnabledGraphProperty.get()) authenticatedIcon else null,
          authenticationPanel { oauthLinkRow() },
          null,
          initialOAuthTabIndex
        )
      }

      !shouldShowOAuthTab && oauthTabCurrentlyShown -> {
        removeTabAt(currentOAuthTabIndex)
      }
    }
  }

  private fun Panel.ignoreCertificateErrorsRow() {
    row {
      checkBox(ignoreCertificateErrorsLabel).bindSelected(settings::ignoreCertificateErrors).also { checkbox ->
        comment(ignoreCertificateErrorsDescription).also { warning ->
          warning.visible(settings.ignoreCertificateErrors)
          checkbox.onChanged {
            warning.visible(it.isSelected)
          }
        }
      }
    }
  }

  private fun Panel.oauthLinkRow() {
    row {
      val loginLink = link(
        text = message("settings.ui.gitlab.oauth-login-link-text"),
        action = {
          val messageBusConnection = application.messageBus.connect()

          messageBusConnection.subscribe(
            GitLabOAuthCallbackListener.OAUTH_CALLBACK_TOPIC,
            DefaultGitLabOAuthCallbackListener({
              oauthEnabledGraphProperty.set(true)
              trackAuthenticationType(TokenProviderType.OAUTH)
              logger.info("OAuth login success.")
              messageBusConnection.disconnect()
            })
          )

          coroutineScope.launch {
            application.service<GitLabOAuthService>().authorize()
              .whenComplete { credentials, error ->
                when {
                  error != null -> {
                    logger.warn("OAuth login failure: ${error.message}")
                    messageBusConnection.disconnect()
                  }

                  credentials == null -> {
                    logger.warn("OAuth login failed to return credentials")
                    messageBusConnection.disconnect()
                  }
                }
              }
          }
        }
      ).visible(!oauthEnabledGraphProperty.get())

      val revokeLink = link(
        text = message("settings.ui.gitlab.oauth-revoke-link-text"),
        action = {
          application.service<GitLabOAuthService>().revokeToken(application.service<OAuthTokenProvider>().token())
          oauthEnabledGraphProperty.set(false)
        }
      ).visible(oauthEnabledGraphProperty.get())

      label(message("settings.ui.gitlab.oauth-authenticated-comment"))
        .visibleIf(oauthEnabledGraphProperty)
        .apply {
          component.icon = authenticatedIcon
          component.foreground = UIManager.getColor("Component.infoForeground")
          component.font = component.font.deriveFont(Font.ITALIC)
        }

      oauthEnabledGraphProperty.afterChange { oauthEnabled ->
        settings.oauthEnabled = oauthEnabled

        loginLink.visible(!oauthEnabled)
        revokeLink.visible(oauthEnabled)
        verifyButton.visible(oauthEnabled)
        updateAuthenticatedIcon()
      }
    }
  }

  private fun updateAuthenticatedIcon() {
    coroutineScope.launch {
      var authenticatedTabIndex: Int? = null

      if (oauthEnabledGraphProperty.get()) {
        authenticatedTabIndex = authTabbedPane.component.oauthTabIndex()
      } else {
        val tokenProviderType = when {
          settings.integrate1PasswordCLI -> TokenProviderType.ONE_PASSWORD
          else -> TokenProviderType.PAT
        }

        val healthCheckResult = project.service<ConfigurationValidationService>().validateConfiguration(
          ConfigurationValidationRequest(
            baseUrl = settings.url,
            token = tokenProviderManager.getToken(),
          )
        )

        authenticatedTabIndex = healthCheckResult?.get(FeatureId.AUTHENTICATION)
          ?.takeIf { it.engagedChecks.isEmpty() }
          ?.let {
            when (tokenProviderType) {
              TokenProviderType.ONE_PASSWORD -> authTabbedPane.component.onePasswordTabIndex()
              TokenProviderType.PAT -> authTabbedPane.component.patTabIndex()
              else -> null
            }
          }
      }

      authTabbedPane.component.apply {
        for (tabIndex in 0 until tabCount) {
          if (tabIndex == authenticatedTabIndex) {
            setIconAt(tabIndex, authenticatedIcon)
          } else {
            setIconAt(tabIndex, null)
          }
        }
      }
    }
  }

  private fun Panel.patSectionRow(): Row {
    tokenText = patProvider.token(slowOperationsPermitted = false)

    return row {
      val patLabel = label(tokenLabel)
      patField = passwordField()
        .bindText(::tokenText)
        .focused()
        .errorOnApply(
          message("settings.ui.gitlab.auth-method-required")
        ) {
          !integrate1PasswordCheckbox.component.isSelected &&
            String(patField.component.password).isBlank() &&
            !oauthEnabledGraphProperty.get()
        }
        .apply {
          patFieldEmptyText = component.emptyText.apply {
            setText(message("settings.ui.personal-access-token.loading"), GRAY_ATTRIBUTES)
          }
          patLabel.component.labelFor = component
        }
      button(createTokenLabel) { launchUrl(Urls.GENERATE_TOKEN) }
    }
  }

  private fun Panel.oauthEnabledCommentRow() {
    row {
      comment(message("settings.ui.gitlab.oauth-enabled-comment")).visibleIf(oauthEnabledGraphProperty)
    }
  }

  private fun Panel.verifySetupRow() {
    twoColumnsRow(
      {
        verifyButton = button(verifySetupLabel) {
          verifyLabel.visible(true)

          invokeLater {
            validateConfiguration()
          }
        }
      }
    ) {
      verifyLabel = label("Verifying...").apply { visible(false) }
    }
  }

  private fun Panel.healthCheckGroup(): Row {
    return group("Health Check") {
      row { tokenHealth = healthCheckField("Validating token health...") }
      row { codeSuggestionHealth = healthCheckField("Validating Code Suggestions health...") }
      row { duoChatHealth = healthCheckField("Validating GitLab Duo Chat health...") }

      row {
        cannotVerifyLabel = label(message("settings.ui.gitlab.verify-setup.unavailable")).also { label ->
          label.visible(false)
          label.component.icon = AllIcons.General.Warning
        }
      }

      row {
        errorReportingInstructions = browserLink(
          "How to report errors",
          "https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin#reporting-issues-or-providing-feedback"
        ).visible(false)
      }
    }.visible(false)
  }

  private fun Panel.featuresGroup() {
    groupRowsRange("Features") {
      row {
        codeSuggestionsEnabledCheckbox =
          checkBox(codeSuggestionsEnabledLabel).bindSelected(settings::codeSuggestionsEnabled)
      }
      row {
        duoChatEnabledCheckbox = checkBox(duoChatEnabledLabel).bindSelected(
          { settings.duoChatEnabled },
          { isEnabled -> settings.duoChatEnabled = isEnabled }
        )
      }
    }
  }

  private fun Panel.languagesGroup(codeCompletion: CodeCompletion) {
    groupRowsRange(
      title = message("settings.ui.gitlab.languages.title"),
      indent = true
    ) {
      Language
        .languageList
        .filter { it.isJetBrainsSupported }
        .forEach { language ->
          row {
            checkBox(language.displayName).bindSelected(
              { !codeCompletion.disabledSupportedLanguages.any { language.matches(it) } },
              { isEnabled ->
                // This removes displayName usage in persistence. Moving forward, we will only persist language IDs.
                codeCompletion.disabledSupportedLanguages.remove(language.displayName)

                if (isEnabled) {
                  codeCompletion.enableSupportedLanguage(language.id)
                } else {
                  codeCompletion.disableSupportedLanguage(language.id)
                }
              }
            )
          }
        }
    }

    row {
      val label = label("Additional languages")
      textField().bindText(
        { codeCompletion.additionalLanguages.joinToString(", ") },
        { languages ->
          codeCompletion.removeAllAdditionalLanguages()
          languages
            .split(",")
            .map { it.trim() }
            .forEach { codeCompletion.addAdditionalLanguage(it) }
        }
      ).apply {
        label.component.labelFor = component
      }
      comment(additionalLanguagesComment)
    }

    row {
      checkBox(
        message("settings.ui.gitlab.verify-setup.code-suggestions.open-tabs")
      ).bindSelected(codeCompletion::enableOpenTabsContext)
    }
  }

  private fun Panel.advancedGroup() {
    groupRowsRange("Advanced") {
      telemetryRow()
      languageServerGroup()
    }
  }

  private fun Panel.telemetryRow() {
    row {
      checkBox(telemetryLabel).bindSelected(settings::telemetryEnabled)
    }

    row {
      comment(telemetryComment)
    }

    row("Tracking URL") {
      textField().bindText(settings::trackingUrl).onReset {
        settings.trackingUrl = settings.trackingUrl.ifBlank {
          BuildConfig.SNOWPLOW_COLLECTOR_URL
        }
      }.enabled(false)
    }
  }

  private fun Panel.integrate1PasswordSettingsRow(): Row {
    return row {
      integrate1PasswordCheckbox = checkBox(message("settings.ui.gitlab.1password-cli-integrate-label"))
        .bindSelected(settings::integrate1PasswordCLI)
        .onChanged {
          if (it.isSelected) {
            patFieldEmptyText.setText(
              message("settings.ui.personal-access-token.computed"),
              GRAY_ATTRIBUTES
            )
          } else {
            patFieldEmptyText.clear()
          }
        }

      browserLink(
        message("settings.ui.gitlab.1password-cli-gitlab-docs-label"),
        Urls.JETBRAINS_1PASSWORD_CLI_INTEGRATION,
      )
    }
  }

  private fun Panel.onePasswordNoAccountsWarningRow(): Row {
    return row {
      comment(message("settings.ui.gitlab.1password-no-accounts"))

      integrate1PasswordCheckbox.onChanged {
        visible(it.isSelected && onePasswordAccounts.get().isEmpty())
      }
    }.visible(integrate1PasswordCheckbox.component.isSelected && onePasswordAccounts.get().isEmpty())
  }

  private fun Panel.onePasswordAccountRow(): Row {
    return row {
      val accountLabel = label(message("settings.ui.gitlab.1password-accounts-label"))

      integrate1PasswordDropdown =
        comboBox(onePasswordAccounts.get().map { it.url }).bindItem(settings::integrate1PasswordAccount).apply {
          accountLabel.component.labelFor = component
        }

      link(
        text = message("settings.ui.gitlab.1password-retry-get-accounts-link-text"),
        action = { fetch1PasswordAccounts() }
      )

      onePasswordAccounts.afterChange {
        onePasswordNoAccountsWarningRow.visible(integrate1PasswordCheckbox.component.isSelected && it.isEmpty())

        // Update dropdown items and maintain previous selected item after account re-fetch
        val previousSelectedItem =
          integrate1PasswordDropdown.component.selectedItem ?: settings.integrate1PasswordAccount
        integrate1PasswordDropdown.component.model = DefaultComboBoxModel(
          it.map { account -> account.url }.toTypedArray()
        ).apply {
          selectedItem = previousSelectedItem
        }
      }
    }
  }

  private fun Panel.secretReferenceRow(): Row {
    return row {
      enabledIf(integrate1PasswordCheckbox.selected)

      val secretReferenceLabel = label(message("settings.ui.gitlab.1password-cli-secret-reference-label"))

      integrate1PasswordSecretReferenceField = textField()
        .align(FILL)
        .bindText(settings::integrate1PasswordCLISecretReference)
        .errorOnApply(message("settings.ui.gitlab.1password-secret-reference-invalid")) {
          integrate1PasswordCheckbox.component.isSelected && !OP_SECRET_REFERENCE_PATTERN.matches(it.text)
        }.apply {
          component.emptyText.setText(DEFAULT_GITLAB_1PASSWORD_SECRET_REFERENCE)
          secretReferenceLabel.component.labelFor = component
        }
    }
  }

  private fun Panel.languageServerGroup() = collapsibleGroup("GitLab Language Server") {
    lateinit var restartLspIcon: Cell<JLabel>

    row {
      comment(GitLabBundle.getMessage("settings.ui.group.language-server.description"))
    }

    row {
      button(message("settings.ui.language-server.restart")) {
        restartLspIcon.component.icon = AllIcons.Actions.Refresh
        restartLspIcon.visible(true)

        coroutineScope.launch {
          languageServerService.restart()

          restartLspIcon.component.icon = AllIcons.RunConfigurations.TestPassed
        }
      }

      restartLspIcon = icon(AllIcons.Actions.Refresh).visible(false)
    }.visible(languageServerService.serverRunning)

    group("Logging") {
      row("Log Level") {
        textField()
          .bindText(
            { lspSettings.state.workspaceSettings.logLevel.orEmpty() },
            { lspSettings.state.workspaceSettings.logLevel = it }
          )
      }
    }

    createFeatureFlagsGroup(
      lspSettings.state.workspaceSettings.featureFlags
    )

    createHttpAgentOptionsGroup(
      lspSettings.state.workspaceSettings.httpAgentOptions
    )
  }

  private fun Panel.createFeatureFlagsGroup(featureFlags: FeatureFlags) = group(
    "Additional Features"
  ) {
    row {
      checkBox(
        message("settings.ui.gitlab.advanced.stream-code-generations")
      ).bindSelected(featureFlags::streamCodeGenerations)
    }
  }

  private fun Panel.createHttpAgentOptionsGroup(httpAgentOptions: HttpAgentOptions) = collapsibleGroup(
    "HTTP Agent Options"
  ) {
    var caTextField: Cell<JBTextField>? = null
    var caCheckBox: Cell<JBCheckBox>? = null
    row {
      caCheckBox = checkBox(message("settings.ui.gitlab.advanced.pass-ca-cert")).bindSelected(httpAgentOptions::pass)
        .onChanged {
          if (it.isSelected) {
            caTextField?.enabled(false)
          } else {
            caTextField?.enabled(true)
          }
        }
    }

    row(message("settings.ui.gitlab.advanced.ca")) {
      caTextField = textField().bindText(
        { httpAgentOptions.ca.orEmpty() },
        { httpAgentOptions.ca = it }
      ).enabled(!(caCheckBox?.component?.isSelected ?: false))
    }

    row(message("settings.ui.gitlab.advanced.cert")) {
      textField().bindText(
        { httpAgentOptions.cert.orEmpty() },
        { httpAgentOptions.cert = it }
      )
    }

    row(message("settings.ui.gitlab.advanced.cert-key")) {
      textField().bindText(
        { httpAgentOptions.certKey.orEmpty() },
        { httpAgentOptions.certKey = it }
      )
    }
  }

  private fun validateConfiguration() {
    coroutineScope.launch {
      settingsHealthChecker.check(
        request = ConfigurationValidationRequest(
          baseUrl = urlField.component.text.removeTrailingSlash(),
          token = retrieveAuthenticationToken(),
          codeSuggestionsEnabled = codeSuggestionsEnabledCheckbox.component.isSelected,
          duoChatEnabled = duoChatEnabledCheckbox.component.isSelected
        )
      )
    }
  }

  private fun createWorkspaceSettings(
    url: String? = null,
    codeSuggestionsEnabled: Boolean? = null,
    duoChatEnabled: Boolean? = null,
    tokenProviderType: TokenProviderType? = null,
    token: String? = null
  ) = WorkspaceSettings(
    url = url ?: settings.url,
    codeSuggestionsEnabled = codeSuggestionsEnabled ?: settings.codeSuggestionsEnabled,
    duoChatEnabled = duoChatEnabled ?: settings.duoChatEnabled,
    tokenProviderType = tokenProviderType ?: when (authTabbedPane.component.selectedIndex) {
      authTabbedPane.component.oauthTabIndex() -> TokenProviderType.OAUTH
      authTabbedPane.component.patTabIndex() -> TokenProviderType.PAT
      authTabbedPane.component.onePasswordTabIndex() -> TokenProviderType.ONE_PASSWORD
      else -> TokenProviderType.PAT
    },
    token = token ?: retrieveAuthenticationToken(),
    telemetry = Telemetry(settings.telemetryEnabled),
    ignoreCertificateErrors = settings.ignoreCertificateErrors
  )

  private fun createSettingsHealthChecker(): SettingsHealthChecker {
    return SettingsHealthChecker(
      project = project,
      errorReportingInstructions = errorReportingInstructions,
      healthCheckGroup = healthCheck,
      healthCheckLabels = mapOf(
        FeatureId.AUTHENTICATION to tokenHealth,
        FeatureId.CODE_SUGGESTIONS to codeSuggestionHealth,
        FeatureId.CHAT to duoChatHealth
      ),
      verifyLabel = verifyLabel,
      cannotVerifyLabel = cannotVerifyLabel
    )
  }

  private fun fetchToken() {
    coroutineScope.launch {
      val token = tokenProviderManager.getToken()
      if (settings.integrate1PasswordCLI) {
        tokenText = token
        patFieldEmptyText.setText(message("settings.ui.personal-access-token.computed"), GRAY_ATTRIBUTES)
      } else {
        tokenText = token
        patFieldEmptyText.clear()
      }
      verifyButton.enabled(true)
      patField.enabled(true)
    }
  }

  private fun fetch1PasswordAccounts() {
    if (authTabbedPane.component.selectedIndex != authTabbedPane.component.onePasswordTabIndex()) {
      return
    }

    coroutineScope.launch {
      val accounts = application.service<OnePasswordTokenProvider>().getAccounts()

      onePasswordAccounts.set(accounts)
    }
  }

  private fun retrieveAuthenticationToken(): String {
    return when (authTabbedPane.component.selectedIndex) {
      authTabbedPane.component.oauthTabIndex() -> application.service<OAuthTokenProvider>().token()
      authTabbedPane.component.patTabIndex() -> String(patField.component.password)
      authTabbedPane.component.onePasswordTabIndex() -> application.service<OnePasswordTokenProvider>()
        .getTokenBySecretReference(
          integrate1PasswordSecretReferenceField.component.text,
          account = integrate1PasswordDropdown.component.selectedItem as String?
        )

      else -> ""
    }
  }

  /*
   * Ensure that the language server is started when the settings page is opened.
   * If no projects are open, a language server process will be created for the default project.
   */
  private fun ensureLanguageServerStarted() {
    coroutineScope.launch {
      project.service<GitLabLanguageServerService>().startServer()
    }
  }
}

private fun Row.healthCheckField(text: String) = label(text).apply {
  comment("")
  comment?.isVisible = false
}

fun GitLabSettingsConfigurable.asBeta(): Configurable =
  object : Configurable by this, Configurable.Beta {}
