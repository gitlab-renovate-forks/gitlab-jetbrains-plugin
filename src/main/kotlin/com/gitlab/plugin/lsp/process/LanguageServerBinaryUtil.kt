package com.gitlab.plugin.lsp.process

import com.intellij.openapi.diagnostic.logger
import java.nio.file.Path

class LanguageServerBinaryUtil {
  val logger = logger<LanguageServerBinaryUtil>()

  fun makeExecutable(path: Path) {
    try {
      logger.info("Applying executable permissions to file: $path")
      val file = path.toFile()
      if (!file.exists()) {
        error("Could not find Language Server binary: $path")
      }
      if (file.canExecute()) {
        logger.info("Language Server binary is already executable: $path")
        return
      }
      if (file.setExecutable(true)) {
        logger.info("Successfully made file executable: $path")
      } else {
        logger.warn("Failed to make file executable: $path")
      }
    } catch (e: SecurityException) {
      logger.error("Permission denied while trying to make Language Server binary executable: $path", e)
    }
  }

  fun resolveBinaryName(): String {
    val os = System.getProperty("os.name").lowercase()
    val arch = System.getProperty("os.arch").lowercase()

    logger.info("Getting language server binary. os=$os arch=$arch")
    return when {
      os.contains("win") -> "gitlab-lsp-win-x64.exe"
      os.contains("nix|nux|aix".toRegex()) -> "gitlab-lsp-linux-x64"
      os.contains("mac") && arch.contains("aarch64") -> "gitlab-lsp-macos-arm64"
      os.contains("mac") -> "gitlab-lsp-macos-x64"
      else -> error("Unsupported OS and architecture. os=$os, arch=$arch")
    }
  }
}
