package com.gitlab.plugin.lsp.services

import com.gitlab.plugin.codesuggestions.listeners.StreamingInlineCompletionEventListener.Companion.STREAMING_INLINE_COMPLETION_TOPIC
import com.gitlab.plugin.codesuggestions.services.SuggestionsTracker
import com.gitlab.plugin.lsp.params.StreamingCompletionResponse
import com.intellij.openapi.components.Service
import com.intellij.openapi.project.Project

@Service(Service.Level.PROJECT)
class CodeCompletionRegistryService(private val project: Project) {
  private val inProgressStreams: MutableMap<String, SuggestionsTracker> = mutableMapOf()

  fun register(id: String, suggestion: SuggestionsTracker) {
    inProgressStreams[id] = suggestion
  }

  fun complete(response: StreamingCompletionResponse) {
    val suggestion = inProgressStreams.remove(response.id)

    if (suggestion != null) {
      project.messageBus.syncPublisher(STREAMING_INLINE_COMPLETION_TOPIC).onStreamingInlineCompletionCompletion()
    }
  }

  fun cancel(id: String) {
    inProgressStreams.remove(id)?.cancel()
  }

  fun cancel(suggestion: SuggestionsTracker) {
    inProgressStreams.remove(suggestion.streamId)
    suggestion.cancel()
  }

  operator fun get(id: String): SuggestionsTracker? {
    return inProgressStreams[id]
  }
}
