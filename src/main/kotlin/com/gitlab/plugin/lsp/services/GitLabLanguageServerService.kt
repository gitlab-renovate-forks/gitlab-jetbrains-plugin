package com.gitlab.plugin.lsp.services

import com.gitlab.plugin.BuildConfig
import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.asProxyInfo
import com.gitlab.plugin.lsp.GitLabLanguageServer
import com.gitlab.plugin.lsp.GitLabLanguageServerClient
import com.gitlab.plugin.lsp.capabilities.LanguageServerCapabilityManager
import com.gitlab.plugin.lsp.listeners.LanguageServerStartedListener
import com.gitlab.plugin.lsp.process.LanguageServerBinaryUtil
import com.gitlab.plugin.lsp.util.KotlinxSerializableEnumAdapterFactory
import com.gitlab.plugin.util.project.workspaceFolder
import com.intellij.ide.plugins.PluginManagerCore
import com.intellij.openapi.Disposable
import com.intellij.openapi.application.ApplicationInfo
import com.intellij.openapi.application.ApplicationNamesInfo
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.Service.Level.PROJECT
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.diagnostic.logger
import com.intellij.openapi.extensions.PluginId
import com.intellij.openapi.project.Project
import com.intellij.util.net.HttpConfigurable
import kotlinx.coroutines.CoroutineScope
import org.eclipse.lsp4j.*
import org.eclipse.lsp4j.jsonrpc.Launcher
import java.io.File
import java.io.IOException
import java.nio.file.Path
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

@Service(PROJECT)
class GitLabLanguageServerService(
  private val project: Project,
  private val coroutineScope: CoroutineScope
) : Disposable {
  companion object {
    const val LANGUAGE_SERVER_STARTED_TIMEOUT_SECONDS = 30L
  }

  private val languageStartedPublisher by lazy {
    project
      .messageBus
      .syncPublisher(LanguageServerStartedListener.LANGUAGE_SERVER_STARTED_TOPIC)
  }

  private val languageServerBinaryUtil = LanguageServerBinaryUtil()

  private val logger: Logger = logger<GitLabLanguageServerService>()
  private val logExecutor: ExecutorService = Executors.newSingleThreadExecutor()
  private var currentProcess: Process? = null

  @Suppress("ForbiddenVoid")
  private var listenerFuture: Future<Void>? = null
  var lspServer: GitLabLanguageServer? = null

  val serverRunning
    get() = lspServer != null

  override fun dispose() {
    logExecutor.shutdownNow()
    stopServer()
  }

  fun startServer() {
    if (currentProcess != null && lspServer != null) {
      logger.info("Language server already started for project ${project.name}")
      return
    }

    val client = createProcess()
      ?: return

    logger.info("Starting Language Server for project ${project.name}")
    listenerFuture = client.startListening()

    lspServer = client.remoteProxy.also {
      val initializeRequest = it.initialize(
        initializeParams(
          extension = GitLabBundle.plugin().let { plugin ->
            GitLabLanguageServerExtension(
              name = plugin.name,
              version = plugin.version
            )
          },
          ide = ApplicationInfo.getInstance().let { ide ->
            GitLabLanguageServerIde(
              name = ApplicationNamesInfo.getInstance().fullProductName,
              version = ide.build.asString(),
              vendor = ide.shortCompanyName,
            )
          },
          folders = listOfNotNull(project.workspaceFolder)
        )
      )

      initializeRequest.handleAsync { result, err ->
        if (err != null) {
          logger.error("Failed to initialize Language Server", err)
        } else {
          logger.info("Initialized Language Server: $result")
          lspServer?.initialized(null)
          languageStartedPublisher.onLanguageServerStarted()
        }
      }.completeOnTimeout(Unit, LANGUAGE_SERVER_STARTED_TIMEOUT_SECONDS, TimeUnit.SECONDS)
    }
  }

  fun stopServer() {
    if (lspServer == null) {
      return
    }

    project.service<LanguageServerCapabilityManager>().unregisterAll()

    lspServer?.exit()
    lspServer = null

    // This is required to avoid thread leaking [issue](https://github.com/eclipse-lsp4j/lsp4j/issues/770)
    listenerFuture?.cancel(true)
  }

  fun restart() {
    stopServer()
    startServer()
  }

  private fun createProcess(): Launcher<GitLabLanguageServer>? {
    val builder = try {
      if (BuildConfig.USE_LOCAL_LSP) {
        val nodeCommand = System.getenv("ASDF_DIR")?.let { "$it/shims/node" } ?: "node"
        val relativeLspPath = "lib/gitlab-lsp/tmp/language-server/node/main-bundle.js"

        ProcessBuilder(
          nodeCommand,
          "--inspect=6010",
          resolvePluginRelativePath(relativeLspPath).toString(),
          "--stdio"
        ).apply {
          directory(project.basePath?.let { File(it) })
        }
      } else {
        val executablePath = resolvePluginRelativePath("lib/gitlab-lsp/bin/${languageServerBinaryUtil.resolveBinaryName()}")
          ?: error("Could not find Language Server binary.")

        languageServerBinaryUtil.makeExecutable(executablePath)

        ProcessBuilder(executablePath.toString(), "--stdio")
      }
    } catch (e: IOException) {
      logger.warn("Failed to the Language Server create process.", e)
      return null
    }

    builder.injectHttpProxyEnvironmentVariables(logger)

    if (BuildConfig.REDIRECT_LS_LOGS_CONSOLE) {
      builder.redirectError(ProcessBuilder.Redirect.INHERIT)
    }

    val process = builder.start()

    if (!BuildConfig.REDIRECT_LS_LOGS_CONSOLE) {
      startLogPull(process)
    }

    process.onExit().thenApply {
      logger.info("Language Server shutdown for project ${project.name}. processInfo: ${process.info()}")
      currentProcess = null
    }

    return Launcher.Builder<GitLabLanguageServer>()
      .setLocalService(GitLabLanguageServerClient(project, coroutineScope))
      .setRemoteInterface(GitLabLanguageServer::class.java)
      .setInput(process.inputStream)
      .setOutput(process.outputStream)
      .configureGson { gsonBuilder -> gsonBuilder.registerTypeAdapterFactory(KotlinxSerializableEnumAdapterFactory()) }
      .create()
      .also { currentProcess = process }
  }

  fun resolvePluginRelativePath(relativePathToLsp: String): Path? {
    return PluginId.getId("com.gitlab.plugin")
      .let(PluginManagerCore::getPlugin)
      ?.pluginPath
      ?.resolve(relativePathToLsp)
  }

  private fun startLogPull(process: Process) {
    logExecutor.execute {
      while (process.isAlive) {
        val stderrLine = process.errorReader().readLine()
        if (stderrLine != null) {
          logger.info("[LS STDERR] $stderrLine")
        }
      }
    }
  }

  private fun initializeParams(
    extension: GitLabLanguageServerExtension,
    ide: GitLabLanguageServerIde,
    folders: List<WorkspaceFolder>,
  ): InitializeParams = InitializeParams().apply {
    processId = currentProcess?.pid()?.toInt()
    capabilities = ClientCapabilities(
      WorkspaceClientCapabilities().also { capabilities ->
        capabilities.configuration = true
        capabilities.workspaceFolders = true
      },
      TextDocumentClientCapabilities().apply {
        completion = CompletionCapabilities().apply {
          completionItem = CompletionItemCapabilities()
          completionItemKind = CompletionItemKindCapabilities(listOf(CompletionItemKind.Text))
          contextSupport = true
          insertTextMode = InsertTextMode.AdjustIndentation
        }
      },
      WindowClientCapabilities().apply {
        showMessage = WindowShowMessageRequestCapabilities().apply {
          messageActionItem = WindowShowMessageRequestActionItemCapabilities()
        }
      },
    )
    clientInfo = ClientInfo(BuildConfig.PLUGIN_NAME, extension.version)
    initializationOptions = GitLabLanguageServerInitializationOptions(extension, ide)
    workspaceFolders = folders
  }
}

class GitLabLanguageServerExtension(val name: String, val version: String)

class GitLabLanguageServerIde(val name: String, val version: String, val vendor: String)

class GitLabLanguageServerInitializationOptions(
  val extension: GitLabLanguageServerExtension?,
  val ide: GitLabLanguageServerIde,
)

fun ProcessBuilder.injectHttpProxyEnvironmentVariables(logger: Logger): ProcessBuilder {
  val environmentVariables = mutableMapOf<String, String>()

  HttpConfigurable.getInstance().asProxyInfo()?.apply {
    logger.info("Preserving IDE proxy settings for language server process.")
    environmentVariables["http_proxy"] = getHttpProxyUrl()
    environmentVariables["HTTPS_PROXY"] = getHttpsProxyUrl()
    proxyExceptions?.let { environmentVariables["NO_PROXY"] = it }
  }

  listOf("http_proxy", "HTTPS_PROXY", "NO_PROXY").forEach { proxyVariable ->
    environmentVariables.computeIfAbsent(proxyVariable) {
      logger.info("Passing through $proxyVariable from environment variable.")
      System.getenv(proxyVariable)
    }
  }

  if (environmentVariables.isNotEmpty()) {
    environment().putAll(environmentVariables)
  } else {
    logger.info("No proxy settings detected in IDE settings or IDE process environment variables.")
  }

  return this
}
