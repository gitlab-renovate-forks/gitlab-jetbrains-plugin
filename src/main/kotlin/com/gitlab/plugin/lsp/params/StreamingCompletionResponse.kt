package com.gitlab.plugin.lsp.params

data class StreamingCompletionResponse(
  val id: String,
  val completion: String = "",
  val done: Boolean = false,
)
