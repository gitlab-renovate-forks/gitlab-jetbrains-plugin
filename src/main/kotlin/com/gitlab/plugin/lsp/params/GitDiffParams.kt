package com.gitlab.plugin.lsp.params

data class GitDiffParams(
  val repositoryUri: String,
  val branch: String? = null
)
