package com.gitlab.plugin.lsp.listeners

import com.gitlab.plugin.lsp.params.StreamingCompletionResponse
import com.intellij.util.messages.Topic

interface CompletionListener {
  companion object {
    @Topic.ProjectLevel
    val CODE_COMPLETION_TOPIC = Topic(
      CompletionListener::class.java.name,
      CompletionListener::class.java
    )
  }

  fun cancel(id: String)
  fun update(params: StreamingCompletionResponse)
}
