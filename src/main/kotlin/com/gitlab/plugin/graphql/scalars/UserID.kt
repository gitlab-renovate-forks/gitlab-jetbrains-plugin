package com.gitlab.plugin.graphql.scalars

class UserID(gid: String) : GitLabScalar(gid)

val UserIDAdapter =
  GitLabScalarAdapter { UserID(it) }
