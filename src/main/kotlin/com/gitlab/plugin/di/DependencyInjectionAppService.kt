package com.gitlab.plugin.di

import com.gitlab.plugin.api.duo.DuoClient
import com.intellij.openapi.components.Service

@Service(Service.Level.APP)
class DependencyInjectionAppService {

  private val duoClientInstance by lazy { DuoClient() }
  fun getDuoClient() = duoClientInstance
}
