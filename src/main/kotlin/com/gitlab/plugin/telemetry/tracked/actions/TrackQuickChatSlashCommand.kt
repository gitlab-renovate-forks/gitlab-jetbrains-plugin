package com.gitlab.plugin.telemetry.tracked.actions

import com.gitlab.plugin.services.GitLabApplicationService
import com.gitlab.plugin.telemetry.SnowplowTracker
import com.gitlab.plugin.telemetry.StandardContext
import com.snowplowanalytics.snowplow.tracker.events.Structured

fun trackQuickChatSlashCommand(message: String) {
  val tracker = GitLabApplicationService.getInstance().snowplowTracker

  val command = when (val prefix = message.takeWhile { it != ' ' }) {
    in listOf("/explain", "/refactor", "/fix", "/tests") -> prefix
    in listOf("/reset", "/clear", "/clean") -> null
    else -> "general_message"
  } ?: return

  val event = Structured.builder()
    .category(SnowplowTracker.Category.GITLAB_QUICK_CHAT)
    .action("message_sent")
    .label(command)
    .customContext(
      listOf(
        StandardContext.build(),
        SnowplowTracker.IDE_EXTENSION_VERSION_CONTEXT
      )
    )
    .build()

  tracker.track(event)
}
