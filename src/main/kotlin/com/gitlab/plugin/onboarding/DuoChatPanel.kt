package com.gitlab.plugin.onboarding

import com.gitlab.plugin.ktx.splitTextIntoJLabels
import com.gitlab.plugin.ui.components.HyperlinkTextPane
import com.gitlab.plugin.ui.components.Shortcut
import com.gitlab.plugin.ui.components.WrappableLabel
import com.intellij.openapi.keymap.KeymapUtil
import com.intellij.util.ui.JBFont
import com.intellij.util.ui.WrapLayout
import java.awt.Dimension
import java.awt.FlowLayout
import java.awt.event.KeyEvent
import javax.swing.*

@Suppress("MagicNumber")
class DuoChatPanel(
  private val onOpenDuoChatClicked: () -> Unit,
  private val onBackButtonClicked: () -> Unit,
  private val onDoneButtonClicked: () -> Unit,
) : JPanel() {

  private val title = WrappableLabel("Ask Duo questions about your code") {
    font = JBFont.h0().asBold()
    alignmentX = RIGHT_ALIGNMENT
  }

  private val openDuoChat = HyperlinkTextPane().apply {
    alignmentX = RIGHT_ALIGNMENT

    addHyperlink("Open Duo Chat.", onOpenDuoChatClicked)
    addText(" To get contextual answers: highlight some code, then ask Duo Chat a question about it.")
  }

  private val instructions = JPanel(WrapLayout(FlowLayout.LEFT, 0, 0)).apply {
    alignmentX = RIGHT_ALIGNMENT

    splitTextIntoJLabels(
      "To write tests, refactor, or explain highlighted code, access slash commands by right-clicking in the " +
        "editor, or typing"
    )

    add(Shortcut("/"))
    add(JLabel(" in Duo Chat."))
  }

  private val quickChat = JPanel(WrapLayout(FlowLayout.LEFT, 0, 0)).apply {
    alignmentX = RIGHT_ALIGNMENT

    add(JLabel("Press "))
    add(Shortcut("${KeymapUtil.getKeyText(KeyEvent.VK_ALT)} c"))
    splitTextIntoJLabels(" to access Duo Quick Chat in your editor window.")
  }

  private val buttons = JPanel(FlowLayout(FlowLayout.TRAILING)).apply {
    alignmentX = RIGHT_ALIGNMENT

    add(JButton("Back").apply { addActionListener { onBackButtonClicked() } })
    add(JButton("Done").apply { addActionListener { onDoneButtonClicked() } })
  }

  init {
    layout = BoxLayout(this, BoxLayout.PAGE_AXIS)

    add(title)
    add(Box.createRigidArea(Dimension(0, 8)))
    add(openDuoChat)
    add(Box.createRigidArea(Dimension(0, 8)))
    add(instructions)
    add(Box.createRigidArea(Dimension(0, 8)))
    add(quickChat)
    add(Box.createRigidArea(Dimension(0, 16)))
    add(buttons)
  }
}
