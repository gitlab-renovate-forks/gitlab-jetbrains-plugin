package com.gitlab.plugin.api.duo.requests

import com.google.gson.JsonPrimitive
import org.eclipse.lsp4j.Command

const val SUGGESTION_ACCEPTED_COMMAND = "gitlab.ls.codeSuggestionAccepted"
const val START_STREAMING_COMMAND = "gitlab.ls.startStreaming"

class Completion {
  data class Payload(
    val promptVersion: Int = 1,
    val projectPath: String = "",
    val projectId: Int = -1,
    val currentFile: CodeSuggestionRequestFile = CodeSuggestionRequestFile(),
    val telemetry: List<Telemetry> = emptyList()
  ) {
    data class CodeSuggestionRequestFile(
      val fileName: String = "",
      val fileUri: String = "",
      val cursorColumn: Int = 0,
      val cursorLine: Int = 0,
      val contentAboveCursor: String = "",
      val contentBelowCursor: String = ""
    )

    data class Telemetry(
      val modelEngine: String,
      val modelName: String,
      val lang: String,
      val errors: Int
    )
  }

  data class Response(
    val choices: List<Choice>,
    val model: Model? = null
  ) {
    data class Choice(
      val text: String,
      val command: Command? = null
    ) {
      val uniqueTrackingId: String? = when (command?.command) {
        SUGGESTION_ACCEPTED_COMMAND -> (command.arguments?.getOrNull(0) as? JsonPrimitive)?.asString
        START_STREAMING_COMMAND -> (command.arguments?.getOrNull(1) as? JsonPrimitive)?.asString
        else -> null
      }

      val streamId: String? = when (command?.command) {
        START_STREAMING_COMMAND -> (command.arguments?.getOrNull(0) as? JsonPrimitive)?.asString
        else -> null
      }

      val isStreaming = streamId != null

      val optionId: Int? = when (isStreaming) {
        true -> null
        false -> (command?.arguments?.getOrNull(1) as? JsonPrimitive)?.asInt
      }
    }

    data class Model(
      val engine: String = "",
      val name: String = "",
      val lang: String = ""
    )

    val firstSuggestion: String
      get() = choices.first().text

    fun hasSuggestions() = choices.isNotEmpty() && choices.all { it.text.isNotBlank() || it.isStreaming }

    fun getStreamingSuggestion(): Choice? {
      return choices.find { it.isStreaming }
    }
  }
}
