package com.gitlab.plugin.api.duo

import com.apollographql.apollo3.ApolloClient
import com.apollographql.apollo3.api.Optional
import com.apollographql.apollo3.exception.ApolloException
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.chat.api.model.AiAction
import com.gitlab.plugin.chat.api.model.AiMessage
import com.gitlab.plugin.chat.api.model.toAiContextItemMetadata
import com.gitlab.plugin.chat.context.AiContextCategory
import com.gitlab.plugin.chat.context.AiContextItem
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.ChatRecordFileContext
import com.gitlab.plugin.exceptions.ExceptionTracker
import com.gitlab.plugin.graphql.*
import com.gitlab.plugin.graphql.scalars.UserID
import com.gitlab.plugin.graphql.type.*
import com.gitlab.plugin.ui.CreateNotificationExceptionHandler
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.intellij.openapi.components.Service
import com.intellij.openapi.components.service
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.diagnostic.logger
import com.intellij.util.application
import com.intellij.util.asSafely
import io.ktor.util.*
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.delay
import kotlinx.coroutines.withTimeout
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

@Suppress("TooManyFunctions", "FunctionNaming")
@Service(Service.Level.APP)
class GraphQLApi {
  companion object {
    fun create(apolloClient: ApolloClient, exceptionHandler: ExceptionHandler) = GraphQLApi().apply {
      this.apolloClient = apolloClient
      this.exceptionHandler = exceptionHandler
    }

    private const val WEBSOCKET_OPEN_TIMEOUT_MS = 5_000L
    private const val WEBSOCKET_OPEN_POLLING_INTERVAL_MS = 100L
  }

  private val apolloClientFactory = application.service<ApolloClientFactory>()
  private var apolloClient: ApolloClient = apolloClientFactory.create()
  private var exceptionHandler: ExceptionHandler = CreateNotificationExceptionHandler(GitLabNotificationManager())
  private val exceptionTracker: ExceptionTracker = application.service<ExceptionTracker>()
  private val logger: Logger = logger<GraphQLApi>()

  private var skipWebSocketOpenCheck = false

  suspend fun getCurrentUser() = try {
    apolloClient.query(CurrentUserQuery())
      .execute()
      .dataAssertNoErrors
      .currentUser
  } catch (cause: ApolloException) {
    exceptionHandler.handleException(cause)
    logger.info("GraphQL CurrentUserQuery returned an error: ${cause.message}")
    null
  }

  @Suppress("DEPRECATION")
  suspend fun chatMutation(
    content: String,
    clientSubscriptionId: String,
    context: ChatRecordContext? = null,
    aiContextItems: List<AiContextItem>? = null
  ): AiAction? = try {
    waitForOpenWebSocketConnection()

    val currentFile = Optional.presentIfNotNull(
      context?.let {
        extractCurrentFile(it.currentFile)
      }
    )

    when {
      aiContextItems == null -> executeChatMutation_17_4_and_earlierMutation(
        question = content,
        currentFileContext = currentFile,
        clientSubscriptionId = clientSubscriptionId
      )
      else -> executeChatMutation_17_5_and_laterMutation(
        question = content,
        currentFileContext = currentFile,
        clientSubscriptionId = clientSubscriptionId,
        additionalContext = aiContextItems.toGraphQLInput(),
      )
    }
  } catch (cause: ApolloException) {
    exceptionHandler.handleException(cause)
    logger.info("GraphQL ChatMutation request returned an error: ${cause.message}")
    null
  }

  private suspend fun executeChatMutation_17_4_and_earlierMutation(
    question: String,
    clientSubscriptionId: String,
    currentFileContext: Optional<AiCurrentFileInput?>
  ): AiAction? {
    val request = ChatMutation_17_4_and_earlierMutation(
      question = question,
      currentFileContext = currentFileContext,
      clientSubscriptionId = clientSubscriptionId
    )

    val response = apolloClient.mutation(request)
      .execute()
      .dataAssertNoErrors
      .aiAction

    return response?.requestId?.let { AiAction(requestId = it, errors = response.errors) }
  }

  private suspend fun executeChatMutation_17_5_and_laterMutation(
    question: String,
    clientSubscriptionId: String,
    currentFileContext: Optional<AiCurrentFileInput?>,
    additionalContext: Optional<List<AiAdditionalContextInput>?>
  ): AiAction? {
    val request = ChatMutation_17_5_and_laterMutation(
      question = question,
      currentFileContext = currentFileContext,
      clientSubscriptionId = clientSubscriptionId,
      additionalContext = additionalContext,
      platformOrigin = "jetbrains_extension" // NOTE: Need to add support for this value in the monolith
    )

    val response = apolloClient.mutation(request)
      .execute()
      .dataAssertNoErrors
      .aiAction

    return response?.requestId?.let { AiAction(requestId = it, errors = response.errors) }
  }

  @Suppress("DEPRECATION")
  suspend fun chatQuery(requestId: String, useAdditionalContext: Boolean) = try {
    when {
      useAdditionalContext -> executeChatQuery_17_5_and_laterQuery(requestId)
      else -> executeChatQuery_17_4_and_earlierQuery(requestId)
    }
  } catch (cause: ApolloException) {
    exceptionHandler.handleException(cause)
    logger.info("GraphQL ChatQuery request returned an error: ${cause.message}")
    null
  }.orEmpty()

  private suspend fun executeChatQuery_17_4_and_earlierQuery(requestId: String): List<AiMessage>? {
    val request = ChatQuery(Optional.present(listOf(requestId)))

    return apolloClient.query(request)
      .execute()
      .dataAssertNoErrors
      .messages
      .nodes
      ?.filterNotNull()
      ?.map { node ->
        AiMessage(
          requestId = requestId,
          role = node.role.toString().toLowerCasePreservingASCIIRules(),
          content = node.content.orEmpty(),
          contentHtml = node.contentHtml.orEmpty(),
          timestamp = Clock.System.now().toLocalDateTime(TimeZone.UTC).toString(),
        )
      }
  }

  @Suppress("UseOrEmpty")
  private suspend fun executeChatQuery_17_5_and_laterQuery(requestId: String): List<AiMessage>? {
    val request = ChatQuery_17_5_and_laterQuery(Optional.present(listOf(requestId)))

    return apolloClient.query(request)
      .execute()
      .dataAssertNoErrors
      .messages
      .nodes
      ?.filterNotNull()
      ?.map { node ->
        AiMessage(
          requestId = requestId,
          role = node.role.toString().toLowerCasePreservingASCIIRules(),
          content = node.content.orEmpty(),
          contentHtml = node.contentHtml.orEmpty(),
          timestamp = Clock.System.now().toLocalDateTime(TimeZone.UTC).toString(),
          extras = node.extras?.let { extra ->
            ChatRecord.Metadata(
              contextItems = extra.additionalContext?.map { item ->
                AiContextItem(
                  id = item.id,
                  category = AiContextCategory.valueOf(item.category.name),
                  metadata = item.metadata.asSafely<LinkedHashMap<*, *>>()?.toAiContextItemMetadata()
                )
              }
            )
          }
        )
      }
  }

  private fun extractCurrentFile(currentFile: ChatRecordFileContext) = AiCurrentFileInput(
    fileName = currentFile.fileName,
    selectedText = currentFile.selectedText,
    contentAboveCursor = Optional.present(currentFile.contentAboveCursor),
    contentBelowCursor = Optional.present(currentFile.contentBelowCursor)
  )

  private fun List<AiContextItem>.toGraphQLInput(): Optional<List<AiAdditionalContextInput>?> {
    return Optional.present(
      value = map { aiContextItem ->
        AiAdditionalContextInput(
          id = aiContextItem.id,
          category = AiAdditionalContextCategory.safeValueOf(aiContextItem.category.name),
          content = aiContextItem.content.orEmpty(),
          metadata = Optional.present(
            mapOf(
              "title" to aiContextItem.metadata?.title,
              "enabled" to aiContextItem.metadata?.enabled,
              "disabledReasons" to aiContextItem.metadata?.disabledReasons,
              "subType" to aiContextItem.metadata?.subType?.name,
              "relativePath" to aiContextItem.metadata?.relativePath,
              "icon" to aiContextItem.metadata?.icon,
              "secondaryText" to aiContextItem.metadata?.secondaryText,
              "subTypeLabel" to aiContextItem.metadata?.subTypeLabel,
              "iid" to aiContextItem.metadata?.iid,
              "project" to aiContextItem.metadata?.project,
              "webUrl" to aiContextItem.metadata?.webUrl,
              "workspaceFolder" to aiContextItem.metadata?.workspaceFolder?.let { workspaceFolder ->
                mapOf(
                  "uri" to workspaceFolder.uri,
                  "name" to workspaceFolder.name
                )
              },
              "repositoryUri" to aiContextItem.metadata?.repositoryUri,
              "repositoryName" to aiContextItem.metadata?.repositoryName,
              "selectedBranch" to aiContextItem.metadata?.selectedBranch,
              "gitType" to aiContextItem.metadata?.gitType,
              "libs" to aiContextItem.metadata?.libs?.let { libs ->
                libs.map { lib ->
                  mapOf(
                    "name" to lib.name,
                    "version" to lib.version
                  )
                }
              }
            )
          )
        )
      }
    )
  }

  suspend fun getProject(projectPath: String) = try {
    apolloClient.query(ProjectQuery(projectPath))
      .execute()
      .dataAssertNoErrors
      .project
  } catch (cause: ApolloException) {
    exceptionHandler.handleException(cause)
    logger.info("GraphQL ProjectQuery returned an error: ${cause.message}")
    null
  }

  fun chatSubscription(clientSubscriptionId: String, useAdditionalContext: Boolean, userId: UserID) = try {
    val subscription = when {
      useAdditionalContext -> ChatWithAdditionalContextSubscription(userId, clientSubscriptionId)
      else -> ChatSubscription(userId, clientSubscriptionId)
    }

    apolloClient.subscription(subscription).toFlow()
  } catch (cause: ApolloException) {
    exceptionHandler.handleException(cause)
    logger.info("GraphQL ChatSubscription returned an error: ${cause.message}")
    null
  }

  fun reload() {
    apolloClient = apolloClientFactory.create()
  }

  /**
   * Wait for a websocket connection to be established.
   * If it is not established by then, a TimeoutCancellationException will be tracked to Sentry.
   * If a TimeoutCancellationException is thrown, subsequent requests won't check for WebSocket availability again.
   *
   * This is important because in some cases the connection is still not available
   * by the time the backend is done responding to the prompt.
   */
  private suspend fun waitForOpenWebSocketConnection() {
    try {
      if (skipWebSocketOpenCheck) {
        logger.debug("Already failed to establish WebSocket connection. Skipping WebSocket open check.")
        return
      }

      withTimeout(WEBSOCKET_OPEN_TIMEOUT_MS) {
        while (apolloClientFactory.getWebSocketEngine()?.isOpen != true) {
          delay(WEBSOCKET_OPEN_POLLING_INTERVAL_MS)
        }
      }

      logger.debug("WebSocket connection established in time to send ChatMutation request.")
    } catch (cause: TimeoutCancellationException) {
      // The GraphQL request should not fail.
      // The error could be caused by a proxy configuration that blocks WebSocket usage.
      logger.warn("WebSocket connection did not establish in time to send ChatMutation request.")
      exceptionTracker.handle(DidNotEstablishWebSocketConnectionInTimeException(cause))

      skipWebSocketOpenCheck = true
    }
  }
}

class DidNotEstablishWebSocketConnectionInTimeException(cause: Throwable) : Exception(cause)
