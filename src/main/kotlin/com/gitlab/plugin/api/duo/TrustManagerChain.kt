package com.gitlab.plugin.api.duo

import com.intellij.openapi.components.service
import com.intellij.util.application
import com.intellij.util.net.ssl.CertificateManager
import java.security.KeyStore
import java.security.KeyStoreException
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

interface TrustManagerChain {
  fun getChain(): List<X509TrustManager>
}

object DefaultTrustManagerChain : TrustManagerChain {
  override fun getChain(): List<X509TrustManager> {
    val osKeyStore = buildOsKeyStore() ?: return listOf(application.service<CertificateManager>().trustManager)

    val osTrustManager = buildTrustManager(osKeyStore)

    return listOf(osTrustManager, application.service<CertificateManager>().trustManager)
  }

  private fun buildOsKeyStore(): KeyStore? {
    val os = System.getProperty("os.name").lowercase()
    val keyStoreName = when {
      os.contains("mac") -> "KeychainStore"
      os.contains("windows") -> "Windows-ROOT"
      else -> return null
    }

    return try {
      KeyStore.getInstance(keyStoreName).also {
        it.load(null, null)
      }
    } catch (ignored: KeyStoreException) {
      // it is possible that OS was not detected correctly or there is no such store in the system
      null
    }
  }

  private fun buildTrustManager(keyStore: KeyStore?): X509TrustManager {
    val trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm()).also {
      it.init(keyStore)
    }

    return trustManagerFactory.trustManagers.single() as X509TrustManager
  }
}
