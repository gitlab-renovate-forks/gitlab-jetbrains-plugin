package com.gitlab.plugin.api.duo

import com.gitlab.plugin.api.HttpExceptionHandler
import com.gitlab.plugin.api.proxiedEngine
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.authentication.GitLabTokenProviderManager
import com.gitlab.plugin.exceptions.ExceptionTracker
import com.gitlab.plugin.util.GitLabUtil
import com.google.gson.FieldNamingPolicy
import com.intellij.collaboration.util.resolveRelative
import com.intellij.openapi.components.service
import com.intellij.util.application
import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.gson.*
import okhttp3.OkHttpClient
import java.net.URI
import javax.net.ssl.SSLContext

class DuoClient(
  private val userAgent: String = GitLabUtil.userAgent,
  private val shouldRetry: Boolean = true,
  httpClientEngine: HttpClientEngine = proxiedEngine(),
  private val host: String = DuoPersistentSettings.getInstance().url,
) {
  companion object {
    private const val HTTP_TIMEOUT = 60_000L
  }

  private val restClient = HttpClient(httpClientEngine) {
    expectSuccess = true

    defaultRequest {
      contentType(ContentType.Application.Json)
    }

    install(UserAgent) {
      agent = userAgent
    }

    if (shouldRetry) {
      install(HttpRequestRetry) {
        retryOnServerErrors(maxRetries = 3)
        exponentialDelay()
      }
    }

    install(ContentNegotiation) {
      gson {
        setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
      }
    }

    install(HttpTimeout) {
      requestTimeoutMillis = HTTP_TIMEOUT
      socketTimeoutMillis = HTTP_TIMEOUT
    }

    HttpResponseValidator {
      handleResponseExceptionWithRequest { cause: Throwable, request: HttpRequest ->
        HttpExceptionHandler.handle(cause, request)
      }
    }
  }

  suspend fun get(
    endpoint: String,
    block: HttpRequestBuilder.() -> Unit = {}
  ): HttpResponse? {
    try {
      val newBlock = fun HttpRequestBuilder.() {
        method = HttpMethod.Get
        usingToken()
        block.invoke(this)
      }

      val fullUrl = URI(host).resolveRelative(endpoint).toString()

      return restClient.request(fullUrl, newBlock)
    } catch (ex: Exception) {
      service<ExceptionTracker>().handle(ex)
      return null
    }
  }
}

fun OkHttpClient.Builder.configureSslSocketFactory() {
  val trustManager = UserConfigurableTrustManager(
    application.service<DuoPersistentSettings>(),
    DefaultTrustManagerChain
  )
  val sslContext = SSLContext.getInstance("TLS")
  sslContext.init(null, arrayOf(trustManager), null)
  sslSocketFactory(sslContext.socketFactory, trustManager)
}

fun HttpRequestBuilder.usingToken() {
  val token = application.service<GitLabTokenProviderManager>().getToken()

  headers {
    remove(HttpHeaders.Authorization)
    header(HttpHeaders.Authorization, "Bearer $token")
  }
}
