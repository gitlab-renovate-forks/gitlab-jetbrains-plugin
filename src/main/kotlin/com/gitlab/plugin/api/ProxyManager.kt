package com.gitlab.plugin.api

import com.gitlab.plugin.api.duo.configureSslSocketFactory
import com.intellij.openapi.diagnostic.logger
import com.intellij.util.net.HttpConfigurable
import io.ktor.client.engine.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.engine.okhttp.OkHttp
import okhttp3.*
import okhttp3.Authenticator
import java.io.IOException
import java.net.*

fun HttpConfigurable.asProxyInfo(): ProxyManager.ProxyInfo? {
  if (!USE_HTTP_PROXY && !PROXY_TYPE_IS_SOCKS) {
    ProxyManager.logger.info("No proxy configuration detected in IDE settings.")
    return null
  }

  val proxyInfo = ProxyManager.ProxyInfo(
    if (USE_HTTP_PROXY) Proxy.Type.HTTP else Proxy.Type.SOCKS,
    PROXY_HOST,
    PROXY_PORT,
    proxyLogin,
    plainProxyPassword,
    PROXY_AUTHENTICATION,
    PROXY_EXCEPTIONS
  )

  val prefix = "Routing connections through ${proxyInfo.proxyType.name} proxy ${proxyInfo.host}:${proxyInfo.port} from IDE settings"
  ProxyManager.logger.info(
    when {
      proxyInfo.login != null && proxyInfo.password != null -> "$prefix using login ${proxyInfo.login} and password."
      proxyInfo.login != null -> "$prefix using login ${proxyInfo.login}."
      proxyInfo.password != null -> "$prefix using password."
      else -> "$prefix without authentication."
    }
  )

  return proxyInfo
}

class ProxyManager(private val proxyInfo: ProxyInfo, val hasProxyForUri: (String?) -> Boolean) :
  ProxySelector(), Authenticator {

  companion object {
    val logger = logger<ProxyManager>()
  }

  data class ProxyInfo(
    val proxyType: Proxy.Type,
    val host: String,
    val port: Int,
    val login: String?,
    val password: String?,
    val usesAuth: Boolean,
    val proxyExceptions: String?
  ) {
    @Suppress("HttpUrlsUsage")
    fun getHttpProxyUrl() = if (usesAuth) {
      val encodedLogin = login?.let { URLEncoder.encode(login, Charsets.UTF_8) }.orEmpty()
      val encodedPassword = password?.let { URLEncoder.encode(password, Charsets.UTF_8) }.orEmpty()
      "http://$encodedLogin:$encodedPassword@$host:$port"
    } else {
      "http://$host:$port"
    }

    /**
     * NOTE: Using an HTTP proxy for HTTPS traffic is why this uses the http protocol by default.
     * JetBrains IDEs don't let you specify the protocol as HTTPS in their HTTP Proxy settings.
     * @see https://serverfault.com/a/817684
     */
    fun getHttpsProxyUrl() = getHttpProxyUrl()
  }

  private val proxy = Proxy(proxyInfo.proxyType, InetSocketAddress.createUnresolved(proxyInfo.host, proxyInfo.port))
  override fun select(uri: URI?): MutableList<Proxy> =
    if (hasProxyForUri(uri.toString())) mutableListOf(proxy) else mutableListOf()

  override fun connectFailed(uri: URI?, socketAddress: SocketAddress?, exception: IOException?) = throw exception!!

  override fun authenticate(route: Route?, response: Response): Request? {
    if (!proxyInfo.usesAuth) return null
    route?.proxy ?: return null

    val credential = Credentials.basic(proxyInfo.login ?: "", proxyInfo.password ?: "")

    return response.request.newBuilder().addHeader("Proxy-Authorization", credential).build()
  }
}

fun OkHttpConfig.setupProxy(
  proxy: ProxyManager.ProxyInfo?,
  hasProxyForUri: (String?) -> Boolean = { true }
) {
  config {
    configureSslSocketFactory()
    proxy ?: return@config
    followRedirects(true)

    val proxyManager = ProxyManager(proxy, hasProxyForUri)

    proxySelector(proxyManager)
    proxyAuthenticator(proxyManager)
  }
}

fun proxiedEngine(): HttpClientEngine = OkHttp.create {
  setupProxy(HttpConfigurable.getInstance().asProxyInfo()) {
    HttpConfigurable.getInstance().isHttpProxyEnabledForUrl(it)
  }
}
