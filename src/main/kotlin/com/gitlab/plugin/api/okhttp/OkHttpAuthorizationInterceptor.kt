package com.gitlab.plugin.api.okhttp

import com.gitlab.plugin.authentication.GitLabTokenProviderManager
import com.intellij.openapi.components.service
import com.intellij.util.application
import okhttp3.Interceptor
import okhttp3.Response

class OkHttpAuthorizationInterceptor : Interceptor {
  override fun intercept(chain: Interceptor.Chain): Response {
    var request = chain.request()

    if (request.header("Authorization") == null) {
      val tokenManager = application.service<GitLabTokenProviderManager>()

      request = request.newBuilder()
        .addHeader("Authorization", "Bearer ${tokenManager.getToken()}")
        .build()
    }

    return chain.proceed(request)
  }
}
