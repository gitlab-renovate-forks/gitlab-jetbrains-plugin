package com.gitlab.plugin.lsp.util

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.beInstanceOf
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

class KotlinxSerializableEnumAdapterFactoryTest : DescribeSpec({
  val gson = GsonBuilder()
    .registerTypeAdapterFactory(KotlinxSerializableEnumAdapterFactory())
    .create()

  it("should return null for non-enum types") {
    val factory = KotlinxSerializableEnumAdapterFactory()
    val adapter = factory.create(Gson(), TypeToken.get(String::class.java))
    adapter shouldBe null
  }

  it("should return null for base Enum class") {
    val factory = KotlinxSerializableEnumAdapterFactory()
    val adapter = factory.create(Gson(), TypeToken.get(Enum::class.java))
    adapter shouldBe null
  }

  it("should return null for unannotated enum") {
    val factory = KotlinxSerializableEnumAdapterFactory()
    val adapter = factory.create(Gson(), TypeToken.get(UnannotatedEnum::class.java))
    adapter shouldBe null
  }

  it("should return adapter for annotated enum") {
    val factory = KotlinxSerializableEnumAdapterFactory()
    val adapter = factory.create(Gson(), TypeToken.get(AnnotatedEnum::class.java))
    adapter should beInstanceOf<KotlinxSerializableEnumAdapterFactory.KotlinxSerializableEnumAdapter<AnnotatedEnum>>()
  }

  describe("KotlinxSerializableEnumAdapter") {
    context("serialization") {
      it("should serialize annotated enum using SerialName value") {
        val json = gson.toJson(AnnotatedEnum.ONE)
        json shouldBe "\"first\""
      }

      it("should fall back to using name if serializing an unannotated enum") {
        val json = gson.toJson(UnannotatedEnum.ONE)
        json shouldBe "\"ONE\""
      }

      it("should serialize null as null") {
        val json = gson.toJson(null)
        json shouldBe "null"
      }

      it("should serialize enum in an object") {
        data class Wrapper(val enum: AnnotatedEnum)
        val json = gson.toJson(Wrapper(AnnotatedEnum.TWO))
        json shouldBe """{"enum":"second"}"""
      }
    }

    context("deserialization") {
      it("should deserialize unannotated enum") {
        val enum = gson.fromJson("\"ONE\"", UnannotatedEnum::class.java)
        enum shouldBe UnannotatedEnum.ONE
      }

      it("should deserialize annotated enum using SerialName value") {
        val enum = gson.fromJson("\"first\"", AnnotatedEnum::class.java)
        enum shouldBe AnnotatedEnum.ONE
      }

      it("should deserialize null as null") {
        val enum = gson.fromJson("null", AnnotatedEnum::class.java)
        enum shouldBe null
      }

      it("should deserialize enum in an object") {
        data class Wrapper(val enum: AnnotatedEnum)
        val obj = gson.fromJson("""{"enum":"third"}""", Wrapper::class.java)
        obj.enum shouldBe AnnotatedEnum.THREE
      }

      it("should return null for invalid enum value") {
        val result = gson.fromJson("\"invalid\"", UnannotatedEnum::class.java)
        result shouldBe null
      }
    }
  }
}) {
  enum class UnannotatedEnum {
    ONE,
    TWO,
    THREE
  }

  @Serializable
  enum class AnnotatedEnum {
    @SerialName("first")
    ONE,

    @SerialName("second")
    TWO,

    @SerialName("third")
    THREE
  }
}
