package com.gitlab.plugin.lsp.listeners

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.authentication.GitLabTokenProviderManager
import com.gitlab.plugin.chat.view.CefChatBrowser
import com.gitlab.plugin.lsp.params.Settings
import com.gitlab.plugin.lsp.params.toDto
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.lsp.settings.LanguageServerSettings
import com.gitlab.plugin.lsp.settings.LanguageServerSettingsState
import com.gitlab.plugin.services.GitLabProjectService
import com.gitlab.plugin.workspace.Telemetry
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.guessProjectDir
import com.intellij.openapi.vfs.VirtualFile
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*
import org.eclipse.lsp4j.DidChangeConfigurationParams

class ConfigureLanguageServerStartedListenerTest : DescribeSpec({
  val url = "https://gitlab.com"
  val token = "gl-pat123456"
  val projectPath = "gitlab-org/gitlab"
  val telemetry = Telemetry(true, "http://telemetry.gitlab.com")
  val ignoreCertificateSettings = true

  val project = mockk<Project>()

  val gitLabTokenProviderManager = mockk<GitLabTokenProviderManager> {
    every { this@mockk.getToken() } returns token
  }

  val duoSettings = mockk<DuoPersistentSettings> {
    every { this@mockk.url } returns url
    every { this@mockk.telemetryEnabled } returns telemetry.enabled
    every { this@mockk.trackingUrl } returns telemetry.trackingUrl
    every { this@mockk.ignoreCertificateErrors } returns ignoreCertificateSettings
    every { this@mockk.duoChatEnabled } returns true
    every { this@mockk.codeSuggestionsEnabled } returns true
  }

  val gitLabProjectService = mockk<GitLabProjectService> {
    every { getCurrentProjectPath() } returns projectPath
  }

  val languageServerSettingsState = mockk<LanguageServerSettingsState>(relaxed = true)
  val languageServerSettings = mockk<LanguageServerSettings>()
  val languageServerService = mockk<GitLabLanguageServerService>(relaxed = true)
  val chatBrowser = mockk<CefChatBrowser>(relaxed = true)

  val listener = ConfigureLanguageServerStartedListener(project)

  beforeSpec {
    mockkStatic(Project::guessProjectDir)
  }

  beforeEach {
    val application = mockApplication()

    every { application.service<GitLabTokenProviderManager>() } returns gitLabTokenProviderManager
    every { application.service<DuoPersistentSettings>() } returns duoSettings

    every { project.getService(GitLabLanguageServerService::class.java) } returns languageServerService
    every { project.getService(LanguageServerSettings::class.java) } returns languageServerSettings
    every { project.getService(GitLabProjectService::class.java) } returns gitLabProjectService
    every { project.getService(CefChatBrowser::class.java) } returns chatBrowser
    every { chatBrowser.loadLSWebView() } just Runs
    every { languageServerSettings.state } returns languageServerSettingsState

    val projectDir = mockk<VirtualFile>().also {
      every { it.url } returns "file:///home/user/gitlab/gitlab/"
      every { it.name } returns "gitlab"
    }
    every { project.guessProjectDir() } returns projectDir

    every {
      languageServerSettingsState.workspaceSettings.codeCompletion.additionalLanguages
    } returns mutableSetOf("R")

    every {
      languageServerSettingsState.workspaceSettings.codeCompletion.getDisabledLanguages()
    } returns mutableSetOf("cpp", "javascript")
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  it("should send language server workspace configuration to the language server") {
    listener.onLanguageServerStarted()

    val params = slot<DidChangeConfigurationParams>()
    verify {
      languageServerService
        .lspServer
        ?.workspaceService
        ?.didChangeConfiguration(capture(params))
    }
    val configurationSettings = params.captured.settings as Settings
    configurationSettings.baseUrl shouldBe url
    configurationSettings.token shouldBe token
    configurationSettings.projectPath shouldBe projectPath
    configurationSettings.ignoreCertificateErrors shouldBe ignoreCertificateSettings
    configurationSettings.logLevel shouldBe languageServerSettingsState.workspaceSettings.logLevel
    configurationSettings.openTabsContext shouldBe languageServerSettingsState.workspaceSettings.codeCompletion.enableOpenTabsContext
    configurationSettings.codeCompletion?.additionalLanguages shouldBe setOf("R")
    configurationSettings.codeCompletion?.disabledSupportedLanguages shouldBe setOf("cpp", "javascript")
    configurationSettings.codeCompletion?.enabled shouldBe true
    configurationSettings.httpAgentOptions shouldBe languageServerSettingsState.workspaceSettings.httpAgentOptions.toDto()
    configurationSettings.featureFlags shouldBe languageServerSettingsState.workspaceSettings.featureFlags.toDto()
    configurationSettings.telemetry shouldBe telemetry.toDto()
    configurationSettings.duoChat?.enabled shouldBe duoSettings.duoChatEnabled
    configurationSettings.workspaceFolders!![0].uri shouldBe "file:///home/user/gitlab/gitlab/"
    configurationSettings.workspaceFolders!![0].name shouldBe "gitlab"
  }
})
