package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.DuoClient
import com.gitlab.plugin.api.duo.requests.Metadata
import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.di.DependencyInjectionAppService
import com.intellij.openapi.components.service
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.ktor.client.call.*
import io.ktor.client.statement.*
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import java.lang.module.ModuleDescriptor.Version.parse
import kotlin.test.assertEquals

class GitLabServerServiceTest : DescribeSpec({

  val httpResponse = mockk<HttpResponse>()

  beforeEach {
    val application = mockApplication()

    every { application.service<DependencyInjectionAppService>() } returns mockk<DependencyInjectionAppService>()
      .also { di ->
        every { di.getDuoClient() } returns mockk<DuoClient>().also { duoClient ->
          coEvery { duoClient.get(any()) } returns httpResponse
        }
      }
  }

  val serverService = GitLabServerService()

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    clearAllMocks()
  }

  describe("get current server data") {
    beforeEach {
      coEvery { httpResponse.body<Metadata.Response?>() } returns Metadata.Response(version = "1.0")
    }

    it("fetches new data, when server data is stale") {
      val metadata = serverService.get()
      assertEquals(parse("1.0"), metadata.version)
    }

    it("returns existing data when server data is fresh") {
      val metadata = serverService.get()
      metadata.lastUpdated = System.currentTimeMillis()
      assertEquals(parse("1.0"), metadata.version)
    }
  }

  describe("check ifEnabled returns correctly") {
    beforeEach {
      val server = serverService.get()
      server.lastUpdated = 0
    }

    it("returns true when feature is enabled") {
      coEvery { httpResponse.body<Metadata.Response?>() } returns Metadata.Response(version = "16.8.0")
      val feature = VersionBasedFeature.JETBRAINS_EXTENSION
      serverService.get()

      val result = serverService.isEnabled(feature)

      result shouldBe true
    }

    it("returns false when feature is not enabled") {
      coEvery { httpResponse.body<Metadata.Response?>() } returns Metadata.Response(version = "16.7.0")
      val feature = VersionBasedFeature.JETBRAINS_EXTENSION
      serverService.get()

      val result = serverService.isEnabled(feature)

      result shouldBe false
    }

    it("returns false when version is empty") {
      coEvery { httpResponse.body<Metadata.Response?>() } returns null
      val feature = VersionBasedFeature.JETBRAINS_EXTENSION
      serverService.get()

      val result = serverService.isEnabled(feature)

      result shouldBe false
    }
  }

  describe("isVersionUpgradeRequired returns correctly when called") {
    beforeEach {
      coEvery { httpResponse.body<Metadata.Response?>() } returns Metadata.Response(version = "16.8.0")
      val server = serverService.get()
      server.lastUpdated = 0
    }
  }
})
