package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.graphql.ApolloClientFactory
import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.graphql.ProjectQuery
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.mockk.*

class DuoEnabledForGitLabProjectServiceTest : DescribeSpec({
  beforeContainer {
    mockkConstructor(GraphQLApi::class)
    mockkConstructor(ApolloClientFactory::class)
  }

  afterContainer {
    unmockkAll()
  }

  describe("DuoEnabledProjectService") {
    val gitLabProjectService = mockk<GitLabProjectService>(relaxed = true)

    val projectPath = "test/test-project"
    val project = mockk<Project>()
    val graphQLApi = mockk<GraphQLApi>()

    lateinit var service: DuoEnabledForGitLabProjectService

    beforeEach {
      val application = mockApplication()
      every { application.service<GraphQLApi>() } returns graphQLApi
      every { project.service<GitLabProjectService>() } returns gitLabProjectService
      every { gitLabProjectService.getCurrentProjectPath() } returns projectPath

      every { project.name } returns "test-name"

      service = DuoEnabledForGitLabProjectService(project)
    }

    afterEach {
      clearAllMocks()
    }

    describe("isDuoEnabledForProject") {
      it("returns false for a project with duo disabled") {
        coEvery {
          graphQLApi.getProject(projectPath)
        } returns ProjectQuery.Project(false)

        service.isDuoEnabledAtGitLabProjectLevel().shouldBeFalse()
      }

      it("returns true for a project with duo enabled") {
        coEvery {
          graphQLApi.getProject(projectPath)
        } returns ProjectQuery.Project(true)

        service.isDuoEnabledAtGitLabProjectLevel().shouldBeTrue()
      }

      it("returns true when given an empty project path") {
        coEvery {
          graphQLApi.getProject("")
        } returns ProjectQuery.Project(true)
        every { gitLabProjectService.getCurrentProjectPath() } returns ""

        service.isDuoEnabledAtGitLabProjectLevel().shouldBeTrue()
      }

      context("Using GitLab 16.8 without feature flags") {
        it("returns and caches true") {
          coEvery {
            graphQLApi.getProject(projectPath)
          } returns null

          service.isDuoEnabledAtGitLabProjectLevel().shouldBeTrue()
        }
      }

      it("returns cached duo enabled value for given project") {
        coEvery {
          graphQLApi.getProject(projectPath)
        } returns ProjectQuery.Project(true)

        service.isDuoEnabledAtGitLabProjectLevel().shouldBeTrue()
        service.isDuoEnabledAtGitLabProjectLevel().shouldBeTrue()

        coVerify(atLeast = 1) { graphQLApi.getProject(projectPath) }
      }

      it("invalidates and fetch the duo enabled value for given project") {
        coEvery {
          graphQLApi.getProject(projectPath)
        } returns ProjectQuery.Project(true)

        service.isDuoEnabledAtGitLabProjectLevel().shouldBeTrue()

        coEvery {
          graphQLApi.getProject(projectPath)
        } returns ProjectQuery.Project(false)

        service.isDuoEnabledAtGitLabProjectLevel(true).shouldBeFalse()
      }
    }
  }
})
