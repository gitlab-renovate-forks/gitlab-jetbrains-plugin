package com.gitlab.plugin.services.chat

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.chat.context.AiContextCategory
import com.gitlab.plugin.chat.mockDuoChatToolWindowManager
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.ChatRecordFileContext
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.chat.view.CefChatBrowser
import com.gitlab.plugin.lsp.GitLabLanguageServer
import com.gitlab.plugin.lsp.services.GitLabLanguageServerService
import com.gitlab.plugin.services.DuoChatStateService
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.registry.RegistryManager
import com.intellij.openapi.wm.ToolWindow
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import java.util.concurrent.CompletableFuture

@OptIn(ExperimentalCoroutinesApi::class)
class ChatServiceTest : DescribeSpec({
  val project: Project = mockk(relaxed = true)

  describe("ChatService") {
    val application = mockApplication()
    val registryManager = mockk<RegistryManager>(relaxed = true)

    lateinit var chatService: ChatService
    lateinit var mockBrowser: CefChatBrowser
    lateinit var chatApiClient: ChatApiClient
    lateinit var duoChatToolWindow: ToolWindow

    beforeEach {
      val mockLanguageServerService = mockk<GitLabLanguageServerService>(relaxed = true)
      val mockLanguageServer = mockk<GitLabLanguageServer>(relaxed = true)
      every { project.service<GitLabLanguageServerService>() } returns mockLanguageServerService
      every { mockLanguageServerService.lspServer } returns mockLanguageServer
      coEvery { mockLanguageServer.aiContextGetProviderCategories() } returns CompletableFuture.completedFuture(
        listOf(
          AiContextCategory.FILE,
          AiContextCategory.ISSUE
        )
      )

      chatApiClient = mockk(relaxed = true)
      every { project.service<ChatApiClient>() } returns chatApiClient

      duoChatToolWindow = mockk(relaxed = true)
      mockDuoChatToolWindowManager(duoChatToolWindow, project)
      every { duoChatToolWindow.activate(null) } just runs

      mockBrowser = mockk<CefChatBrowser>(relaxed = true)
      every { project.service<CefChatBrowser>() } returns mockBrowser
      every { application.service<RegistryManager>() } returns registryManager
      every { project.service<DuoChatStateService>() } returns mockk(relaxed = true)

      val testScope = TestScope(UnconfinedTestDispatcher())
      chatService = ChatService(project, testScope)
    }

    afterEach { clearAllMocks() }
    afterSpec { unmockkAll() }

    describe("activateChatWithPrompt") {
      val context = ChatRecordContext(
        currentFile = ChatRecordFileContext(
          fileName = "TestFile.kt",
          selectedText = "println(\"Hello, World!\")",
          contentAboveCursor = "fun main() {",
          contentBelowCursor = "}"
        )
      )
      val prompt = NewUserPromptRequest(
        content = "test content",
        type = ChatRecord.Type.GENERAL,
        context = context
      )

      describe("when tool window is visible") {
        beforeEach {
          every { duoChatToolWindow.isVisible } returns true
        }

        it("activates the tool window and processes the prompt") {
          chatService.activateChatWithPrompt(prompt)

          verify(exactly = 1) { duoChatToolWindow.activate(null) }
          coVerify(exactly = 1) {
            chatApiClient.processNewUserPrompt(
              subscriptionId = any(),
              question = eq(prompt.content),
              context = eq(context),
              aiContextItems = null
            )
          }
        }
      }

      describe("when tool window is not visible") {
        beforeEach {
          every { duoChatToolWindow.isVisible } returns false
        }

        it("does not activate the tool window but still processes the prompt") {
          chatService.activateChatWithPrompt(prompt)

          verify(exactly = 0) { duoChatToolWindow.activate(null) }
          coVerify(exactly = 1) {
            chatApiClient.processNewUserPrompt(
              subscriptionId = any(),
              question = eq(prompt.content),
              context = eq(context),
              aiContextItems = null
            )
          }
        }
      }
    }
  }
})
