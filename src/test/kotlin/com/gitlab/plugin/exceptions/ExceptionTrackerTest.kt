package com.gitlab.plugin.exceptions

import com.gitlab.plugin.api.mockApplication
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*

class ExceptionTrackerTest : DescribeSpec({
  val sentryTracker = mockk<SentryTracker>(relaxUnitFun = true)
  val exceptionTracker = ExceptionTracker()

  beforeEach {
    val application = mockApplication()
    every { application.getService(SentryTracker::class.java) } returns sentryTracker
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  it("should track handled exceptions using Sentry") {
    val exception = RuntimeException("It went super wrong...")

    exceptionTracker.handle(exception)

    verify(exactly = 1) { sentryTracker.trackException(exception) }
  }
})
