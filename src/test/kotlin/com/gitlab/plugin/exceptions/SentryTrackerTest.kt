package com.gitlab.plugin.exceptions

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.util.IdeMetadata
import com.intellij.openapi.components.service
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*
import io.sentry.Sentry
import io.sentry.protocol.SentryId
import java.util.*

class SentryTrackerTest : DescribeSpec({
  val ideMetadata = mockk<IdeMetadata>(relaxed = true)
  val duoSettings = mockk<DuoPersistentSettings>(relaxed = true)

  lateinit var sentryTracker: SentryTracker

  beforeSpec {
    mockkStatic(Sentry::class)
    mockkObject(IdeMetadata.Companion)
    mockkStatic("com.gitlab.plugin.exceptions.SentryConfigurationKt")
  }

  beforeEach {
    val application = mockApplication()
    every { application.service<DuoPersistentSettings>() } returns duoSettings
    every { IdeMetadata.Companion.get() } returns ideMetadata

    every { duoSettings.telemetryEnabled } returns true

    // Avoid sending events in Tests
    every { Sentry.captureException(any()) } returns mockk<SentryId>()
    every { isSentryTrackingEnabled() } returns true

    sentryTracker = SentryTracker()
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  it("should not track exception if telemetry is disabled") {
    every { duoSettings.telemetryEnabled } returns false
    val exception = RuntimeException("This is a test")

    sentryTracker.trackException(exception)

    verify(exactly = 0) { Sentry.captureException(exception) }
  }

  it("should not track exception if sentry is disabled") {
    every { isSentryTrackingEnabled() } returns false

    val exception = RuntimeException("This is a test")

    sentryTracker.trackException(exception)

    verify(exactly = 0) { Sentry.captureException(exception) }
  }

  it("should track exception if telemetry is enabled") {
    every { ideMetadata.vendor } returns "JetBrains"
    every { ideMetadata.name } returns "WebStorm"
    every { ideMetadata.version } returns "243.213892138921"
    val exception = RuntimeException("Connection failed: 192.168.1.100:8080 (500)")

    sentryTracker.trackException(exception)

    verify(exactly = 1) {
      Sentry.setExtra("ide", "JetBrains WebStorm 243.213892138921")
      Sentry.captureException(
        match<SanitizedException> { it.message == "Connection failed: ip_address (500)" }
      )
    }
  }
})
