package com.gitlab.plugin.api.duo

import com.gitlab.plugin.api.buildMockEngine
import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.authentication.GitLabTokenProviderManager
import com.gitlab.plugin.exceptions.ExceptionTracker
import com.intellij.openapi.application.Application
import com.intellij.openapi.components.service
import io.ktor.client.call.*
import io.ktor.client.engine.*
import io.ktor.client.engine.mock.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.utils.io.*
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class DuoClientTest {
  private lateinit var application: Application

  private val pat = "a_pat"

  @BeforeEach
  fun init() {
    application = mockApplication()
    every { service<GitLabTokenProviderManager>() } returns mockk()
    every { service<ExceptionTracker>() } returns mockk(relaxed = true)
    every { service<GitLabTokenProviderManager>() } returns mockk<GitLabTokenProviderManager>().also {
      every { it.getToken() } returns pat
    }
  }

  @Nested
  inner class RestClient {
    @Test
    fun `builds request headers correctly`() = runTest {
      val requestData = captureRequest { mockEngine ->
        buildClient(httpClientEngine = mockEngine).get("someEndpoint")
      }.first

      assertEquals("application/json", requestData.headers[HttpHeaders.ContentType])
      assertEquals("gitlabUserAgent", requestData.headers[HttpHeaders.UserAgent])
    }

    @Test
    fun `converts response object keys from underscore`() = runTest {
      val body = buildClient(httpClientEngine = buildMockEngine(content = """{"some_property": "abc"}"""))
        .get("someEndpoint")
        ?.body<Response>()

      assertEquals(Response(someProperty = "abc"), body)
    }

    @Nested
    inner class Get {

      @Test
      fun `creates the request passing additional options`() = runTest {
        val requestData = captureRequest { mockEngine ->
          buildClient(httpClientEngine = mockEngine)
            .get("someEndpoint") {
              header("SOME_HEADER", "SOME_VALUE")
            }
        }.first

        assertEquals(HttpMethod.Get, requestData.method)
        assertEquals("https://example.com/someEndpoint", requestData.url.toString())
        assertEquals("SOME_VALUE", requestData.headers["SOME_HEADER"])
        assertEquals("Bearer $pat", requestData.headers[HttpHeaders.Authorization])
      }

      @Test
      fun `creates a request with the correct url when passed a base url`() = runTest {
        val requestData = captureRequest { mockEngine ->
          buildClient(httpClientEngine = mockEngine, host = "https://gitlab.myinstance.com")
            .get("someEndpoint")
        }.first

        assertEquals("https://gitlab.myinstance.com/someEndpoint", requestData.url.toString())
      }

      @Test
      fun `creates a request with the correct url when passed a base url with a path`() = runTest {
        val requestData = captureRequest { mockEngine ->
          buildClient(httpClientEngine = mockEngine, host = "https://gitlab.myinstance.com/gitlab")
            .get("someEndpoint")
        }.first

        assertEquals("https://gitlab.myinstance.com/gitlab/someEndpoint", requestData.url.toString())
      }
    }

    @Nested
    inner class Post {
      val pat = "a_pat"

      @BeforeEach
      fun setupMocks() {
        val application = mockApplication()
        val tokenProviderManager = mockk<GitLabTokenProviderManager>()
        every { application.service<GitLabTokenProviderManager>() } returns tokenProviderManager
        every { tokenProviderManager.getToken() } returns pat
      }
    }
  }

  private data class Response(val someProperty: String)

  fun buildClient(
    httpClientEngine: HttpClientEngine = buildMockEngine(),
    userAgent: String = "gitlabUserAgent",
    shouldRetry: Boolean = false,
    host: String = "https://example.com"
  ) = DuoClient(
    userAgent,
    shouldRetry,
    httpClientEngine,
    host,
  )

  fun captureRequest(block: suspend (httpEngine: HttpClientEngine) -> HttpResponse?):
    Pair<HttpRequestData, HttpResponse?> = runBlocking {
    var data: HttpRequestData? = null

    val mockEngine = MockEngine { request ->
      data = request
      respond(content = ByteReadChannel(""))
    }

    val httpResponse = block.invoke(mockEngine)

    val requestData = data ?: error("Request data not found")

    return@runBlocking Pair(requestData, httpResponse)
  }
}
