package com.gitlab.plugin.api.listeners

import com.gitlab.plugin.api.duo.GraphQLApi
import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.workspace.WorkspaceSettingsBuilder
import com.intellij.openapi.components.service
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*

class GraphQLApiDuoConfigurationChangedListenerTest : DescribeSpec({
  val graphQLApi = mockk<GraphQLApi>(relaxUnitFun = true)
  val application = mockApplication()
  val listener = GraphQLApiDuoConfigurationChangedListener()

  it("updates GraphQL api base url on GitLab url change") {
    val settings = WorkspaceSettingsBuilder {}
    every { application.service<GraphQLApi>() } returns graphQLApi

    listener.onConfigurationChange(settings)

    verify(atLeast = 1) { graphQLApi.reload() }
  }
})
