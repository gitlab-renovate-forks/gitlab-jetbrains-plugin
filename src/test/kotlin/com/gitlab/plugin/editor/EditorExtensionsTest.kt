package com.gitlab.plugin.editor

import com.gitlab.plugin.language.Language
import com.gitlab.plugin.lsp.settings.CodeCompletion
import com.gitlab.plugin.lsp.settings.LanguageServerSettings
import com.gitlab.plugin.lsp.settings.WorkspaceSettings
import com.intellij.openapi.application.ReadAction
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.fileTypes.FileType
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic

class EditorExtensionsTest : DescribeSpec({
  val mockEditor = mockk<Editor>()
  val mockProject = mockk<Project>()
  val mockFileType = mockk<FileType>()
  val mockLanguageServerSettings = mockk<LanguageServerSettings>()
  val mockWorkspaceSettings = mockk<WorkspaceSettings>()
  val mockCodeCompletion = mockk<CodeCompletion>()

  beforeSpec {
    mockkStatic(ReadAction::class)
    mockkStatic(Language.Companion::class)

    every { mockEditor.project } returns mockProject
    every { ReadAction.compute<FileType?, Throwable>(any()) } returns mockFileType
    every { mockFileType.defaultExtension } returns "kt"
    every { mockProject.service<LanguageServerSettings>() } returns mockLanguageServerSettings
    every { mockLanguageServerSettings.state.workspaceSettings } returns mockWorkspaceSettings
    every { mockWorkspaceSettings.codeCompletion } returns mockCodeCompletion
  }

  describe("getLanguage") {
    it("returns Language when all conditions are met") {
      val expectedLanguage = Language("kotlin", "Kotlin", listOf("kt", "kts"), isJetBrainsSupported = true)

      mockEditor.getLanguage() shouldBe expectedLanguage
    }
  }

  describe("isCodeSuggestionsEnabledForLanguage") {
    it("returns true for supported and not disabled language") {
      every { mockCodeCompletion.disabledSupportedLanguages } returns mutableSetOf("other")

      mockEditor.isCodeSuggestionsEnabledForLanguage() shouldBe true
    }

    it("returns false for supported but disabled language") {
      every { mockCodeCompletion.disabledSupportedLanguages } returns mutableSetOf("kotlin")

      mockEditor.isCodeSuggestionsEnabledForLanguage() shouldBe false
    }

    it("returns true for unsupported language in additional languages") {
      every { mockFileType.defaultExtension } returns "abap"
      every { mockCodeCompletion.additionalLanguages } returns mutableSetOf("abap")

      mockEditor.isCodeSuggestionsEnabledForLanguage() shouldBe true
    }

    it("returns false for unsupported language not in additional languages") {
      every { mockFileType.defaultExtension } returns "abap"
      every { mockCodeCompletion.additionalLanguages } returns mutableSetOf("other")

      mockEditor.isCodeSuggestionsEnabledForLanguage() shouldBe false
    }
  }
})
