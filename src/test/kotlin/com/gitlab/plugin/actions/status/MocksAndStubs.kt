package com.gitlab.plugin.actions.status

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.Presentation
import com.intellij.openapi.util.Computable
import com.intellij.util.application
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.spyk

fun mockAnActionEvent(): AnActionEvent {
  return mockk<AnActionEvent>().also { event ->
    every { event.presentation } returns spyk(Presentation())
  }
}

fun stubRunReadAction() {
  val runnable = slot<Runnable>()
  val computable = slot<Computable<Any>>()
  every { application.runReadAction(capture(runnable)) } answers {
    runnable.captured.run()
  }
  every { application.runReadAction(capture(computable)) } answers {
    computable.captured.compute()
  }
}

fun stubRunWriteAction() {
  val runnable = slot<Runnable>()
  val computable = slot<Computable<Any>>()
  every { application.runWriteAction(capture(runnable)) } answers {
    runnable.captured.run()
  }
  every { application.runWriteAction(capture(computable)) } answers {
    computable.captured.compute()
  }
}

fun stubCodeSuggestionsEnabled(initial: Boolean) {
  var codeSuggestionsEnabled = initial
  every { DuoPersistentSettings.getInstance().codeSuggestionsEnabled } returns codeSuggestionsEnabled
  every { DuoPersistentSettings.getInstance().codeSuggestionsEnabled = any(Boolean::class) } answers {
    codeSuggestionsEnabled = it.invocation.args[0] as Boolean
  }
  every { DuoPersistentSettings.getInstance().toggleCodeSuggestions() } answers {
    codeSuggestionsEnabled = !codeSuggestionsEnabled
    codeSuggestionsEnabled
  }
}
