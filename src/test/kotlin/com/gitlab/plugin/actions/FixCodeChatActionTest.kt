package com.gitlab.plugin.actions

import com.gitlab.plugin.actions.chat.FixCodeChatAction
import com.gitlab.plugin.chat.ChatRecordContextService
import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.core.test.testCoroutineScheduler
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

@OptIn(ExperimentalStdlibApi::class, ExperimentalCoroutinesApi::class)
class FixCodeChatActionTest : DescribeSpec({
  describe("FixCodeChatAction") {
    afterEach { clearAllMocks() }
    afterSpec { unmockkAll() }

    val mockActionEvent = mockk<AnActionEvent>(relaxed = true)
    val mockChatService = mockk<ChatService>(relaxed = true)
    val mockEditor = mockk<Editor>(relaxed = true)

    beforeEach {
      every { mockActionEvent.getData(CommonDataKeys.EDITOR) } returns mockEditor
      every { mockActionEvent.project?.service<ChatService>() } returns mockChatService
      every { mockActionEvent.project?.service<ChatRecordContextService>() } returns mockk(relaxed = true)
      every { mockEditor.selectionModel.hasSelection() } returns true
    }

    describe("actionPerformed") {
      it("calls processNewUserPrompt with the correct content and record type").config(coroutineTestScope = true) {
        val action = FixCodeChatAction()
        action.actionPerformed(mockActionEvent)
        testCoroutineScheduler.advanceUntilIdle()

        coVerify(exactly = 1) { mockChatService.activateChatWithPrompt(any()) }
        coVerify {
          mockChatService.activateChatWithPrompt(
            match {
              it.content == FixCodeChatAction.FIX_CODE_CHAT_CONTENT && it.type == ChatRecord.Type.FIX_CODE
            }
          )
        }
      }
    }
  }
})
