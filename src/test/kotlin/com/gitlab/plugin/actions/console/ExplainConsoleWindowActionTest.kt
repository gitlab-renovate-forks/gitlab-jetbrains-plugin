package com.gitlab.plugin.actions.console

import com.gitlab.plugin.chat.model.ChatRecord
import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.ChatRecordFileContext
import com.gitlab.plugin.chat.model.NewUserPromptRequest
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.execution.impl.ConsoleViewImpl
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.LangDataKeys
import com.intellij.openapi.actionSystem.Presentation
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.editor.SelectionModel
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher

@OptIn(ExperimentalCoroutinesApi::class)
class ExplainConsoleWindowActionTest : DescribeSpec({
  val testDispatcher = UnconfinedTestDispatcher()

  describe("ExplainConsoleWindowAction") {
    val action = ExplainConsoleWindowAction(testDispatcher)

    val event = mockk<AnActionEvent>()
    val presentation = mockk<Presentation>(relaxed = true)
    val selectionModel = mockk<SelectionModel>()

    beforeEach {
      val consoleView = mockk<ConsoleViewImpl>()
      val editor = mockk<Editor>()

      every { event.presentation } returns presentation
      every { event.getData(LangDataKeys.CONSOLE_VIEW) } returns consoleView
      every { consoleView.editor } returns editor
      every { editor.selectionModel } returns selectionModel
    }

    afterEach {
      clearAllMocks()
      unmockkAll()
    }

    describe("update method") {
      it("action should be enabled and visible when text is selected in the console") {
        every { selectionModel.selectedText } returns "Selected text"

        action.update(event)

        verify { presentation.isEnabledAndVisible = true }
      }

      it("action should be disabled and hidden when text is NOT selected in the console") {
        every { selectionModel.selectedText } returns ""

        action.update(event)

        verify { presentation.isEnabledAndVisible = false }
      }
    }

    describe("actionPerformed method") {
      it("should process new user prompt when action is performed") {
        val project = mockk<Project>()
        val chatService = mockk<ChatService>()

        every { event.project } returns project
        every { project.service<ChatService>() } returns chatService
        every { selectionModel.selectedText } returns "Selected console text"
        coEvery { chatService.processNewUserPrompt(any()) } just Runs

        action.actionPerformed(event)

        coVerify {
          chatService.processNewUserPrompt(
            NewUserPromptRequest(
              "/explain",
              ChatRecord.Type.EXPLAIN_CODE,
              context = ChatRecordContext(
                ChatRecordFileContext(
                  fileName = "",
                  selectedText = "Selected console text"
                )
              )
            )
          )
        }
      }
    }
  }
})
