package com.gitlab.plugin.codesuggestions

import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.codesuggestions.InlineCompletionTooltipPresentation.Companion.LOADING_LABEL_TEXT
import com.gitlab.plugin.codesuggestions.listeners.SwitchInlineCompletionEventListener
import com.gitlab.plugin.codesuggestions.services.SUGGESTION_TRACKER_KEY
import com.gitlab.plugin.codesuggestions.services.SuggestionsTracker
import com.gitlab.plugin.codesuggestions.services.SwitchInlineCompletionSuggestionService
import com.gitlab.plugin.ui.GitLabIcons
import com.intellij.icons.AllIcons
import com.intellij.openapi.Disposable
import com.intellij.openapi.application.runInEdt
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.project.Project
import com.intellij.util.messages.MessageBus
import com.intellij.util.messages.MessageBusConnection
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.mockk.*
import java.awt.event.MouseEvent
import javax.swing.*

class InlineCompletionTooltipPresentationTest : DescribeSpec({
  mockkStatic(FileEditorManager::getInstance)
  mockkStatic(::runInEdt)

  val suggestionTracker = mockk<SuggestionsTracker>()
  val switchInlineCompletionSuggestionService = mockk<SwitchInlineCompletionSuggestionService>(relaxed = true)

  val fileEditorManager = mockk<FileEditorManager>()
  val editor = mockk<Editor>()

  val project = mockk<Project>(relaxed = true)
  val messageBus = mockk<MessageBus>(relaxed = true)
  val connection = mockk<MessageBusConnection>(relaxed = true)

  var tooltipPresentation = InlineCompletionTooltipPresentation()

  beforeEach {
    every { suggestionTracker.isStreaming } returns false
    every { suggestionTracker.getIndexInformation() } returns "1/1"

    every { FileEditorManager.getInstance(project) } returns fileEditorManager
    every { fileEditorManager.selectedTextEditor } returns editor

    every { editor.getUserData(SUGGESTION_TRACKER_KEY) } returns suggestionTracker

    every {
      project.getService(SwitchInlineCompletionSuggestionService::class.java)
    } returns switchInlineCompletionSuggestionService

    every { project.messageBus } returns messageBus
    every { messageBus.connect(any<Disposable>()) } returns connection

    every { runInEdt(null, any<() -> Unit>()) } answers { secondArg<() -> Unit>()() }

    tooltipPresentation = InlineCompletionTooltipPresentation()
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  it("should display GitLab Duo label with icon") {
    val panel = tooltipPresentation.getTooltip(project)

    val gitLabDuoLabel = panel.label(GitLabIcons.NotificationIcon)

    gitLabDuoLabel?.text shouldBe "GitLab Duo Code Suggestions"
    gitLabDuoLabel?.icon shouldBe GitLabIcons.NotificationIcon
  }

  it("should not display multiple code suggestions controls if project is null") {
    val panel = tooltipPresentation.getTooltip(project = null)

    val previous = panel.label(AllIcons.Actions.ArrowCollapse)
    val next = panel.label(AllIcons.Actions.ArrowExpand)
    val information = panel.label(LOADING_LABEL_TEXT)

    previous shouldBe null
    next shouldBe null
    information shouldBe null
  }

  it("should not display multiple code suggestions controls if suggestions tracker is null") {
    every { editor.getUserData(SUGGESTION_TRACKER_KEY) } returns null

    val panel = tooltipPresentation.getTooltip(project)

    val previous = panel.label(AllIcons.Actions.ArrowCollapse)
    val next = panel.label(AllIcons.Actions.ArrowExpand)
    val information = panel.label(LOADING_LABEL_TEXT)

    previous shouldBe null
    next shouldBe null
    information shouldBe null
  }

  it("should not display multiple code suggestions controls if suggestion is streaming") {
    every { suggestionTracker.isStreaming } returns true

    val panel = tooltipPresentation.getTooltip(project)

    val previous = panel.label(AllIcons.Actions.ArrowCollapse)
    val next = panel.label(AllIcons.Actions.ArrowExpand)
    val information = panel.label(LOADING_LABEL_TEXT)

    previous shouldBe null
    next shouldBe null
    information shouldBe null
  }

  it("should display multiple code suggestions controls if suggestion is not streaming") {
    every { suggestionTracker.isStreaming } returns false

    val panel = tooltipPresentation.getTooltip(project)

    val previous = panel.label(AllIcons.Actions.ArrowCollapse)
    val next = panel.label(AllIcons.Actions.ArrowExpand)
    val information = panel.label("1/?")

    previous shouldNotBe null
    next shouldNotBe null
    information shouldNotBe null
  }

  it("should load code completions on initial display if not already loaded") {
    every { suggestionTracker.response } returns null
    every { suggestionTracker.getIndexInformation() } returns "1/3"
    val panel = tooltipPresentation.getTooltip(project)

    panel.makeVisible()

    verify(exactly = 1) {
      switchInlineCompletionSuggestionService.loadCompletions(suggestionTracker)
    }
    panel.label("1/3") shouldNotBe null
  }

  it("should not reload code completions on initial display if already loaded") {
    every { suggestionTracker.response } returns Completion.Response(
      choices = listOf(
        Completion.Response.Choice("choice 1"),
        Completion.Response.Choice("choice 2")
      )
    )
    every { suggestionTracker.getIndexInformation() } returns "1/2"
    val panel = tooltipPresentation.getTooltip(project)

    panel.makeVisible()

    verify(exactly = 0) {
      switchInlineCompletionSuggestionService.loadCompletions(suggestionTracker)
    }
    panel.label("1/2") shouldNotBe null
  }

  it("should register the tooltip as listener for switch inline completion topic") {
    tooltipPresentation.getTooltip(project)

    verify {
      connection.subscribe(SwitchInlineCompletionEventListener.SWITCH_INLINE_COMPLETION_TOPIC, tooltipPresentation)
    }
  }

  it("should cycle to next or previous code suggestion on click") {
    val panel = tooltipPresentation.getTooltip(project)

    val previous = panel.label(AllIcons.Actions.ArrowCollapse)
    previous?.dispatchEvent(mouseClickEvent(previous))
    verify { switchInlineCompletionSuggestionService.switch(editor, SwitchInlineCompletionSuggestionMode.PREVIOUS) }

    val next = panel.label(AllIcons.Actions.ArrowExpand)
    next?.dispatchEvent(mouseClickEvent(next))
    verify { switchInlineCompletionSuggestionService.switch(editor, SwitchInlineCompletionSuggestionMode.NEXT) }
  }

  it("should display loading label when requesting code suggestions") {
    val panel = tooltipPresentation.getTooltip(project)

    tooltipPresentation.onRequestMultipleSuggestions()

    panel.label(LOADING_LABEL_TEXT) shouldNotBe null
  }

  it("should display suggestion tracker index on code suggestions loaded") {
    every { suggestionTracker.getIndexInformation() } returns "1/3"
    val panel = tooltipPresentation.getTooltip(project)

    tooltipPresentation.onReceivedMultipleSuggestions(suggestionTracker)

    panel.label("1/3") shouldNotBe null
  }

  it("should display suggestion tracker index on displayed code suggestions changed") {
    every { suggestionTracker.getIndexInformation() } returns "2/3"
    val panel = tooltipPresentation.getTooltip(project)

    tooltipPresentation.onSwitchInlineCompletion(suggestionTracker)

    panel.label("2/3") shouldNotBe null
  }
})

private fun JComponent.label(text: String): JLabel? {
  return components.find { it is JLabel && it.text == text } as JLabel?
}

private fun JComponent.label(icon: Icon): JLabel? {
  return components.find { it is JLabel && it.icon == icon } as JLabel?
}

// To send a showing event on a panel we use addNotify to fire a hierarchy event.
private fun JComponent.makeVisible() {
  if (this is JPanel) {
    this.addNotify()
  }
}

private fun mouseClickEvent(component: JComponent): MouseEvent {
  return MouseEvent(component, MouseEvent.MOUSE_CLICKED, 0, 0, 0, 0, 1, false)
}

//
