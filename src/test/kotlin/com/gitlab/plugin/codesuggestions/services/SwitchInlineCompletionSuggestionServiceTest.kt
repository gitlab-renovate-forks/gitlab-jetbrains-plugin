package com.gitlab.plugin.codesuggestions.services

import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.codesuggestions.SuggestionContext
import com.gitlab.plugin.codesuggestions.SwitchInlineCompletionSuggestionMode
import com.gitlab.plugin.codesuggestions.listeners.SwitchInlineCompletionEventListener
import com.gitlab.plugin.lsp.codesuggestions.LanguageServerCompletionStrategy
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher

@OptIn(ExperimentalCoroutinesApi::class)
class SwitchInlineCompletionSuggestionServiceTest : DescribeSpec({
  val editor = mockk<Editor>(relaxUnitFun = true)
  val project = mockk<Project>()
  val coroutineTestScope = TestScope(UnconfinedTestDispatcher())

  val languageServerCompletionStrategy = mockk<LanguageServerCompletionStrategy>()
  val switchInlineCompletionEventListener = mockk<SwitchInlineCompletionEventListener>(relaxUnitFun = true)

  val suggestionContext = mockk<SuggestionContext>(relaxed = true)
  val suggestionTracker = mockk<SuggestionsTracker>(relaxUnitFun = true)

  lateinit var service: SwitchInlineCompletionSuggestionService

  beforeEach {
    every {
      project.messageBus.syncPublisher(SwitchInlineCompletionEventListener.SWITCH_INLINE_COMPLETION_TOPIC)
    } returns switchInlineCompletionEventListener

    every { project.getService(LanguageServerCompletionStrategy::class.java) } returns languageServerCompletionStrategy

    every { editor.getUserData(SUGGESTION_TRACKER_KEY) } returns suggestionTracker

    every { suggestionTracker.context } returns suggestionContext
    every { suggestionTracker.isStreaming } returns false
    every { suggestionTracker.editor } returns editor

    service = SwitchInlineCompletionSuggestionService(project, coroutineTestScope)
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  it("should not switch inline completion if no suggestion tracking exist for editor") {
    every { editor.getUserData(SUGGESTION_TRACKER_KEY) } returns null

    service.switch(editor, mode = SwitchInlineCompletionSuggestionMode.NEXT)

    verify(exactly = 0) { suggestionTracker.switch(SwitchInlineCompletionSuggestionMode.NEXT) }
  }

  it("should not switch inline completion if current suggestion is streaming") {
    every { editor.getUserData(SUGGESTION_TRACKER_KEY) } returns null
    every { suggestionTracker.isStreaming } returns true

    service.switch(editor, mode = SwitchInlineCompletionSuggestionMode.NEXT)

    verify(exactly = 0) { suggestionTracker.switch(SwitchInlineCompletionSuggestionMode.NEXT) }
  }

  it("should request new suggestions if no response is saved") {
    val response = Completion.Response(
      choices = listOf(
        Completion.Response.Choice(text = "return 1;"),
        Completion.Response.Choice(text = "return 2;"),
        Completion.Response.Choice(text = "return 3;")
      )
    )
    coEvery { languageServerCompletionStrategy.generateCompletions(suggestionContext) } returns response
    every { suggestionTracker.response } returns null

    service.switch(editor, mode = SwitchInlineCompletionSuggestionMode.NEXT)

    verify {
      switchInlineCompletionEventListener.onRequestMultipleSuggestions()

      suggestionContext.isInvoked = true
      suggestionTracker.response = response

      switchInlineCompletionEventListener.onReceivedMultipleSuggestions(suggestionTracker)

      suggestionTracker.switch(SwitchInlineCompletionSuggestionMode.NEXT)
      editor.putUserData(SUGGESTION_TRACKER_KEY, suggestionTracker)
    }
  }

  it("should not request new suggestions is a response is saved") {
    val response = Completion.Response(
      choices = listOf(
        Completion.Response.Choice(text = "return 1;"),
        Completion.Response.Choice(text = "return 2;"),
      )
    )
    every { suggestionTracker.response } returns response

    service.switch(editor, mode = SwitchInlineCompletionSuggestionMode.PREVIOUS)

    coVerify(exactly = 0) {
      switchInlineCompletionEventListener.onRequestMultipleSuggestions()
      languageServerCompletionStrategy.generateCompletions(suggestionContext)
      switchInlineCompletionEventListener.onReceivedMultipleSuggestions(suggestionTracker)
    }
    verify {
      suggestionTracker.switch(SwitchInlineCompletionSuggestionMode.PREVIOUS)
      editor.putUserData(SUGGESTION_TRACKER_KEY, suggestionTracker)
    }
  }

  it("should load multiple code suggestions") {
    val response = Completion.Response(
      choices = listOf(
        Completion.Response.Choice(text = "return 1;"),
        Completion.Response.Choice(text = "return 2;"),
        Completion.Response.Choice(text = "return 3;")
      )
    )
    coEvery { languageServerCompletionStrategy.generateCompletions(suggestionContext) } returns response

    service.loadCompletions(suggestionTracker)

    val sentSuggestionContext = slot<SuggestionContext>()
    val savedSuggestionTracker = slot<SuggestionsTracker>()
    coVerify {
      switchInlineCompletionEventListener.onRequestMultipleSuggestions()
      languageServerCompletionStrategy.generateCompletions(capture(sentSuggestionContext))
      switchInlineCompletionEventListener.onReceivedMultipleSuggestions(suggestionTracker)
      editor.putUserData(SUGGESTION_TRACKER_KEY, capture(savedSuggestionTracker))
    }
    verify {
      sentSuggestionContext.captured.isInvoked = true
      savedSuggestionTracker.captured.response = response
    }
  }
})
