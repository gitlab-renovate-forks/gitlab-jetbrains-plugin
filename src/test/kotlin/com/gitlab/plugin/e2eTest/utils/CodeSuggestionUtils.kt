package com.gitlab.plugin.e2eTest.utils

import com.gitlab.plugin.e2eTest.pages.idea
import com.gitlab.plugin.e2eTest.steps.EditorSteps
import com.intellij.remoterobot.RemoteRobot
import com.intellij.remoterobot.stepsProcessing.log
import com.intellij.remoterobot.utils.keyboard
import java.awt.event.KeyEvent

fun requestCodeSuggestion(
  remoteRobot: RemoteRobot,
  prompt: String,
  suggestionCanDisplayBeforeTypingEnds: Boolean = false
) = with(remoteRobot) {
  idea {
    EditorSteps(remoteRobot).addContentToEditor(prompt)

    try {
      waitForDuoSuggestionLoading()
    } catch (e: Exception) {
      log.info("Code suggestion loading icon was not shown within expected timeout.")

      if (!suggestionCanDisplayBeforeTypingEnds) {
        throw e
      } else {
        log.info("Since suggestions can display before typing ends, let's keep going.")
      }
    }

    // Let's give some time to the language server to respond with (or stream) a suggestion.
    Thread.sleep(1000L)

    waitForDuoSuggestionLoaded()
  }
}

fun verifyCodeSuggestion(remoteRobot: RemoteRobot, prompt: String, expectedTexts: List<String>) = verifyCodeSuggestion(
  remoteRobot,
  prompt,
  { text -> expectedTexts.all { text.contains(it, ignoreCase = true) } },
  { "Expected \"${expectedTexts.joinToString(",")}\" to be included in suggestion." }
)

fun verifyCodeSuggestion(remoteRobot: RemoteRobot, prompt: String, expectedText: String) = verifyCodeSuggestion(
  remoteRobot,
  prompt,
  { it.contains(expectedText, ignoreCase = true) },
  { "Expected \"$expectedText\" to be included in suggestion." }
)

fun verifyCodeSuggestion(remoteRobot: RemoteRobot, prompt: String, expectedRegex: Regex) = verifyCodeSuggestion(
  remoteRobot,
  prompt,
  { expectedRegex.containsMatchIn(it) },
  { "Expected regex \"$expectedRegex\" to be included in suggestion." }
)

fun verifyCodeSuggestion(
  remoteRobot: RemoteRobot,
  prompt: String,
  expectedTextVerifyFn: (String) -> Boolean,
  errorMessage: () -> Any
) = with(remoteRobot) {
  idea {
    val textEditor = textEditor().editor

    waitForDuoSuggestionReady()

    // after suggestion is displayed, accept suggestion
    keyboard { key(KeyEvent.VK_TAB) }

    println("Code suggestion for prompt: \"$prompt\"")
    println(textEditor.text)

    // check suggestion contains something relevant
    assert(expectedTextVerifyFn(textEditor.text), errorMessage)

    waitForDuoSuggestionAccepted()
  }
}
