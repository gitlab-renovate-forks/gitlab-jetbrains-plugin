package com.gitlab.plugin.chat.intentions.fix

import com.gitlab.plugin.chat.ChatRecordContextService
import com.gitlab.plugin.inspection.CurrentFileProblem
import com.gitlab.plugin.inspection.FileProblemsService
import com.gitlab.plugin.services.chat.ChatService
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.CaretModel
import com.intellij.openapi.editor.Document
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiFile
import com.intellij.refactoring.suggested.endOffset
import com.intellij.refactoring.suggested.startOffset
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*

class DuoFixIntentionActionTest : DescribeSpec({
  lateinit var project: Project
  lateinit var editor: Editor
  lateinit var file: PsiFile
  lateinit var caretModel: CaretModel
  lateinit var document: Document
  lateinit var fileProblemsService: FileProblemsService

  beforeTest {
    project = mockk()
    editor = mockk()
    file = mockk()
    caretModel = mockk()
    document = mockk()
    fileProblemsService = mockk()

    every { editor.caretModel } returns caretModel
    every { editor.document } returns document
    every { project.service<FileProblemsService>() } returns fileProblemsService
    every { file.startOffset } returns 0
    every { file.endOffset } returns 100
  }

  afterEach {
    clearAllMocks()
  }

  describe("isAvailable") {
    context("when feature flag is enabled") {
      val action = DuoFixIntentionAction(isDuoFixIntentionEnabled = true)

      beforeTest {
        every { caretModel.offset } returns 15
      }

      it("should return false when there are no errors in the current file") {
        every { fileProblemsService.getCurrentFileErrors() } returns emptySet()

        action.isAvailable(project, editor, file) shouldBe false
      }

      context("when there are errors in the file") {
        it("should return false when caret is on the same line as an error") {
          // Caret is on line 1
          every { document.getLineNumber(15) } returns 1

          every { fileProblemsService.getCurrentFileErrors() } returns setOf(
            CurrentFileProblem("Test error", TextRange(10, 20), 1)
          )

          action.isAvailable(project, editor, file) shouldBe false
        }

        it("should return true when caret is not on same line as any error") {
          // Caret is on line 3
          every { document.getLineNumber(15) } returns 3

          every { fileProblemsService.getCurrentFileErrors() } returns setOf(
            CurrentFileProblem("Test error", TextRange(10, 20), 1)
          )

          action.isAvailable(project, editor, file) shouldBe true
        }

        it("should return true when caret is on different line than multiple errors") {
          // Caret is on line 5
          every { document.getLineNumber(15) } returns 5

          every { fileProblemsService.getCurrentFileErrors() } returns setOf(
            CurrentFileProblem("Error 1", TextRange(10, 20), 1),
            CurrentFileProblem("Error 2", TextRange(30, 40), 3)
          )

          action.isAvailable(project, editor, file) shouldBe true
        }

        it("should return false when caret is on same line as one of multiple errors") {
          // Caret is on line 3
          every { document.getLineNumber(15) } returns 3

          every { fileProblemsService.getCurrentFileErrors() } returns setOf(
            CurrentFileProblem("Error 1", TextRange(10, 20), 1),
            CurrentFileProblem("Error 2", TextRange(30, 40), 3)
          )

          action.isAvailable(project, editor, file) shouldBe false
        }
      }
    }

    context("when feature flag is disabled") {
      val action = DuoFixIntentionAction(isDuoFixIntentionEnabled = false)

      it("should return false regardless of caret position or errors") {
        every { caretModel.offset } returns 15
        every { document.getLineNumber(any()) } returns 1

        every { fileProblemsService.getCurrentFileErrors() } returns setOf(
          CurrentFileProblem("Test error", TextRange(10, 20), 1)
        )

        action.isAvailable(project, editor, file) shouldBe false
      }
    }
  }

  describe("invoke") {
    it("should use file bounds for text range") {
      val action = DuoFixIntentionAction(true)
      val chatService = mockk<ChatService>()
      val chatRecordContextService = mockk<ChatRecordContextService>()

      every { project.service<ChatService>() } returns chatService
      every { project.service<ChatRecordContextService>() } returns chatRecordContextService
      every { chatService.activateChatWithPrompt(any()) } just Runs
      every { chatRecordContextService.getChatRecordContextOrNull(any(), any()) } returns mockk()
      every { fileProblemsService.getCurrentFileErrors() } returns emptySet()

      action.invoke(project, editor, file)

      verify {
        chatRecordContextService.getChatRecordContextOrNull(
          TextRange(0, 100),
          editor
        )
      }
    }
  }
})
