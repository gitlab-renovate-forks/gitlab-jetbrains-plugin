package com.gitlab.plugin.chat

import com.gitlab.plugin.chat.model.ChatRecordContext
import com.gitlab.plugin.chat.model.ChatRecordFileContext
import com.intellij.openapi.application.ReadAction
import com.intellij.openapi.editor.Caret
import com.intellij.openapi.editor.CaretModel
import com.intellij.openapi.editor.Document
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.fileEditor.FileEditorManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.TextRange
import com.intellij.openapi.util.ThrowableComputable
import com.intellij.openapi.vfs.VirtualFile
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic

class ChatRecordContextServiceTest : DescribeSpec({
  lateinit var project: Project
  lateinit var chatRecordContextService: ChatRecordContextService
  lateinit var fileEditorManager: FileEditorManager
  lateinit var editor: Editor
  lateinit var document: Document
  lateinit var caretModel: CaretModel
  lateinit var caret: Caret
  lateinit var virtualFile: VirtualFile

  beforeTest {
    project = mockk()
    fileEditorManager = mockk()
    editor = mockk()
    document = mockk()
    caretModel = mockk()
    caret = mockk()
    virtualFile = mockk()

    every { project.basePath } returns "/base/path"
    mockkStatic(FileEditorManager::class)
    every { FileEditorManager.getInstance(project) } returns fileEditorManager
    every { editor.document } returns document
    every { editor.caretModel } returns caretModel
    every { caretModel.primaryCaret } returns caret
    every { editor.virtualFile } returns virtualFile

    // This allows the code inside ReadAction.compute to just execute
    mockkStatic(ReadAction::class)
    every {
      ReadAction.compute<ChatRecordContext?, Throwable>(any())
    } answers {
      firstArg<ThrowableComputable<ChatRecordContext?, Throwable>>().compute()
    }

    chatRecordContextService = ChatRecordContextService(project)
  }

  afterTest {
    clearAllMocks()
  }

  describe("ChatRecordContextService") {
    describe("getChatRecordContextOrNull") {
      it("returns null when no editor is selected") {
        every { fileEditorManager.selectedTextEditor } returns null

        val result = chatRecordContextService.getChatRecordContextOrNull()

        result shouldBe null
      }

      it("returns null when caret position is out of bounds") {
        every { fileEditorManager.selectedTextEditor } returns editor
        every { document.charsSequence } returns "Sample text"
        every { caret.selectionStart } returns 20
        every { caret.selectionEnd } returns 21

        val result = chatRecordContextService.getChatRecordContextOrNull()

        result shouldBe null
      }

      it("returns null when virtual file is null") {
        every { fileEditorManager.selectedTextEditor } returns editor
        every { document.charsSequence } returns "Sample text"
        every { caret.selectionStart } returns 0
        every { caret.selectionEnd } returns 5
        every { editor.virtualFile } returns null

        val result = chatRecordContextService.getChatRecordContextOrNull()

        result shouldBe null
      }

      it("returns valid context with selected text") {
        every { fileEditorManager.selectedTextEditor } returns editor
        every { document.charsSequence } returns "Sample text content"
        every { caret.selectionStart } returns 0
        every { caret.selectionEnd } returns 6
        every { virtualFile.path } returns "/base/path/src/test.kt"

        val result = chatRecordContextService.getChatRecordContextOrNull()

        result shouldBe ChatRecordContext(
          ChatRecordFileContext(
            fileName = "/src/test.kt",
            selectedText = "Sample",
            contentAboveCursor = "",
            contentBelowCursor = " text content"
          )
        )
      }

      it("handles empty selection") {
        every { fileEditorManager.selectedTextEditor } returns editor
        every { document.charsSequence } returns "Sample text content"
        every { caret.selectionStart } returns 0
        every { caret.selectionEnd } returns 0
        every { virtualFile.path } returns "/base/path/src/test.kt"

        val result = chatRecordContextService.getChatRecordContextOrNull()

        result shouldBe ChatRecordContext(
          ChatRecordFileContext(
            fileName = "/src/test.kt",
            selectedText = "",
            contentAboveCursor = "",
            contentBelowCursor = "Sample text content"
          )
        )
      }

      it("uses provided editor") {
        every { document.charsSequence } returns "Sample text content"
        every { caret.selectionStart } returns 0
        every { caret.selectionEnd } returns 6
        every { virtualFile.path } returns "/base/path/src/test.kt"

        val result = chatRecordContextService.getChatRecordContextOrNull(selectedEditor = editor)

        result shouldBe ChatRecordContext(
          ChatRecordFileContext(
            fileName = "/src/test.kt",
            selectedText = "Sample",
            contentAboveCursor = "",
            contentBelowCursor = " text content"
          )
        )
      }

      describe("with provided TextRange") {
        it("returns valid context") {
          every { fileEditorManager.selectedTextEditor } returns editor
          every { document.charsSequence } returns "Sample text content"
          every { virtualFile.path } returns "/base/path/src/test.kt"

          val range = TextRange(7, 11) // Should select "text"

          val result = chatRecordContextService.getChatRecordContextOrNull(textRange = range)

          result shouldBe ChatRecordContext(
            ChatRecordFileContext(
              fileName = "/src/test.kt",
              selectedText = "text",
              contentAboveCursor = "Sample ",
              contentBelowCursor = " content"
            )
          )
        }

        it("handles empty TextRange") {
          every { fileEditorManager.selectedTextEditor } returns editor
          every { document.charsSequence } returns "Sample text content"
          every { virtualFile.path } returns "/base/path/src/test.kt"

          val range = TextRange(7, 7) // Empty range at position 7

          val result = chatRecordContextService.getChatRecordContextOrNull(textRange = range)

          result shouldBe ChatRecordContext(
            ChatRecordFileContext(
              fileName = "/src/test.kt",
              selectedText = "",
              contentAboveCursor = "Sample ",
              contentBelowCursor = "text content"
            )
          )
        }

        it("handles TextRange at document start") {
          every { fileEditorManager.selectedTextEditor } returns editor
          every { document.charsSequence } returns "Sample text content"
          every { virtualFile.path } returns "/base/path/src/test.kt"

          val range = TextRange(0, 6) // "Sample"

          val result = chatRecordContextService.getChatRecordContextOrNull(textRange = range)

          result shouldBe ChatRecordContext(
            ChatRecordFileContext(
              fileName = "/src/test.kt",
              selectedText = "Sample",
              contentAboveCursor = "",
              contentBelowCursor = " text content"
            )
          )
        }

        it("handles TextRange at document end") {
          every { fileEditorManager.selectedTextEditor } returns editor
          every { document.charsSequence } returns "Sample text content"
          every { virtualFile.path } returns "/base/path/src/test.kt"

          val range = TextRange(12, 19) // "content"

          val result = chatRecordContextService.getChatRecordContextOrNull(textRange = range)

          result shouldBe ChatRecordContext(
            ChatRecordFileContext(
              fileName = "/src/test.kt",
              selectedText = "content",
              contentAboveCursor = "Sample text ",
              contentBelowCursor = ""
            )
          )
        }
      }
    }
  }
})
