package com.gitlab.plugin.chat.telemetry

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.mockApplicationInfo
import com.gitlab.plugin.api.mockPlugin
import com.gitlab.plugin.chat.view.model.TrackFeedbackMessage
import com.gitlab.plugin.services.GitLabApplicationService
import com.gitlab.plugin.telemetry.StandardContext
import com.snowplowanalytics.snowplow.tracker.Tracker
import com.snowplowanalytics.snowplow.tracker.events.Structured
import com.snowplowanalytics.snowplow.tracker.payload.SelfDescribingJson
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.maps.shouldContainAll
import io.mockk.*

class TrackFeedbackTest : DescribeSpec({
  mockkObject(StandardContext, GitLabApplicationService, GitLabBundle)
  mockApplicationInfo()
  mockPlugin()

  val tracker: Tracker = mockk()

  beforeEach {
    every { StandardContext.build(any()) } returns SelfDescribingJson(
      "standard_context",
      mapOf("extra" to "extra-val")
    )

    every { tracker.track(any()) } returns emptyList()

    every { GitLabApplicationService.getInstance().snowplowTracker } returns tracker
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  describe("trackFeedback") {
    it("creates a trackFeedback event with correct payload") {
      val structuredEventSlot = slot<Structured>()
      every { tracker.track(capture(structuredEventSlot)) } returns emptyList()

      trackFeedback(
        TrackFeedbackMessage(
          "track feedback",
          TrackFeedbackMessage.Data(
            "give_faster_response",
            "asked_about_gitlab",
            listOf("Helpful, Factually incorrect")
          )
        )
      )

      structuredEventSlot.captured.payload.map shouldContainAll mapOf(
        "se_ca" to "ask_gitlab_chat",
        "se_ac" to "click_button",
        "se_la" to "response_feedback",
        "se_pr" to "Helpful, Factually incorrect"
      )
    }

    it("creates a trackFeedback event with correct context") {
      val structuredEventSlot = slot<Structured>()
      every { tracker.track(capture(structuredEventSlot)) } returns emptyList()

      trackFeedback(TrackFeedbackMessage("", TrackFeedbackMessage.Data("", "", emptyList())))

      structuredEventSlot.captured.context.first().map shouldContainAll mapOf(
        "schema" to "standard_context",
        "data" to mapOf("extra" to "extra-val")
      )
    }
  }
})
