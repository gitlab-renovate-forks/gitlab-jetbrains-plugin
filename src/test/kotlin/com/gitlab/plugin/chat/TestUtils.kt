package com.gitlab.plugin.chat

import com.gitlab.plugin.actions.chat.DUO_CHAT_TOOL_WINDOW_ID
import com.gitlab.plugin.chat.model.ChatRecord
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.wm.ToolWindow
import com.intellij.openapi.wm.ToolWindowManager
import io.mockk.*
import kotlinx.datetime.LocalDateTime
import java.util.*

fun buildChatRecord(
  id: String = UUID.randomUUID().toString(),
  requestId: String = UUID.randomUUID().toString(),
  timestamp: LocalDateTime = buildLocalDateTime(),
  chunkId: Int? = null
) = ChatRecord(
  id = id,
  role = ChatRecord.Role.USER,
  requestId = requestId,
  state = ChatRecord.State.READY,
  timestamp = timestamp,
  chunkId = chunkId
)

fun buildLocalDateTime() = LocalDateTime.parse("2022-01-01T00:00:00")

fun mockDuoChatToolWindowManager(duoChatToolWindow: ToolWindow, project: Project?) {
  val toolWindowManager: ToolWindowManager = mockk<ToolWindowManager>()
  every { project?.service<ToolWindowManager>() } returns toolWindowManager
  every { toolWindowManager.getToolWindow(DUO_CHAT_TOOL_WINDOW_ID) } returns duoChatToolWindow
  every { duoChatToolWindow.isVisible } returns true
  every { duoChatToolWindow.activate(null) } just runs
}
