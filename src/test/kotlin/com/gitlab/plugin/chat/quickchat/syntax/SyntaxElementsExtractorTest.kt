package com.gitlab.plugin.chat.quickchat.syntax

import com.gitlab.plugin.chat.quickchat.markdown.SyntaxElement
import com.gitlab.plugin.chat.quickchat.markdown.SyntaxElementsExtractor
import com.gitlab.plugin.chat.quickchat.markdown.markdownParser
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe

class SyntaxElementsExtractorTest : DescribeSpec({
  it("extracts plain text") {
    val input = "This is plain text."
    val tree = markdownParser.parse(input)
    val elements = mutableListOf<SyntaxElement>()

    tree.accept(SyntaxElementsExtractor(input, elements))

    elements shouldBe listOf(
      SyntaxElement.Text("This is plain text.")
    )
  }

  it("extracts inline code") {
    val input = "This is `inline code`."
    val tree = markdownParser.parse(input)
    val elements = mutableListOf<SyntaxElement>()

    tree.accept(SyntaxElementsExtractor(input, elements))

    elements shouldBe listOf(
      SyntaxElement.Text("This is "),
      SyntaxElement.Code("inline code"),
      SyntaxElement.Text(".")
    )
  }

  it("extracts fenced code block") {
    val input = """
            This is a fenced code block:
            ```kotlin
            fun hello() {
                println("Hello, world!")
            }
            ```
    """.trimIndent()
    val tree = markdownParser.parse(input)
    val elements = mutableListOf<SyntaxElement>()

    tree.accept(SyntaxElementsExtractor(input, elements))

    elements shouldBe listOf(
      SyntaxElement.Text("This is a fenced code block:\n"),
      SyntaxElement.FencedCodeBlock(
        """
                fun hello() {
                    println("Hello, world!")
                }
        """.trimIndent(),
        "kotlin"
      )
    )
  }

  it("extracts mixed content") {
    val input = """
            This is a mixed content example.
            Here's some `inline code`.
            And a fenced code block:

            ```python
            def greet(name):
                print(f"Hello, {name}!")
            ```

            Followed by more text.
    """.trimIndent()
    val tree = markdownParser.parse(input)
    val elements = mutableListOf<SyntaxElement>()

    tree.accept(SyntaxElementsExtractor(input, elements))

    elements shouldBe listOf(
      SyntaxElement.Text("This is a mixed content example.\nHere's some "),
      SyntaxElement.Code("inline code"),
      SyntaxElement.Text(".\nAnd a fenced code block:\n"),
      SyntaxElement.FencedCodeBlock(
        """
                def greet(name):
                    print(f"Hello, {name}!")
        """.trimIndent(),
        "python"
      ),
      SyntaxElement.Text("\nFollowed by more text.")
    )
  }
})
