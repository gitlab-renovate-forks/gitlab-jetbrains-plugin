package com.gitlab.plugin.chat.quickchat.services

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.chat.quickchat.QUICK_CHAT_SESSION_KEY
import com.gitlab.plugin.chat.quickchat.QuickChatOpenMethod
import com.gitlab.plugin.chat.quickchat.QuickChatSession
import com.gitlab.plugin.chat.quickchat.ui.QuickChatUI
import com.gitlab.plugin.chat.quickchat.ui.QuickChatUIFactory
import com.gitlab.plugin.services.GitLabUserService
import com.gitlab.plugin.services.chat.ChatApiClient
import com.gitlab.plugin.telemetry.tracked.actions.trackQuickChatOpen
import com.intellij.openapi.editor.*
import com.intellij.openapi.editor.markup.HighlighterLayer
import com.intellij.openapi.project.Project
import io.kotest.core.spec.style.DescribeSpec
import io.mockk.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.test.StandardTestDispatcher

class QuickChatServiceTest : DescribeSpec({
  val openMethod = QuickChatOpenMethod.SHORTCUT

  val project = mockk<Project>()
  val editor = mockk<Editor>(relaxed = true)
  val inlay = mockk<Inlay<ComponentInlayRenderer<QuickChatUI>>>(relaxed = true)
  val ui = mockk<QuickChatUI>(relaxed = true)

  val userService = mockk<GitLabUserService>()

  val service = QuickChatService(project, CoroutineScope(StandardTestDispatcher()))

  beforeSpec {
    mockkObject(QuickChatUIFactory)
    mockkStatic("com.intellij.openapi.editor.ComponentInlayKt")
    mockkStatic("com.gitlab.plugin.telemetry.tracked.actions.TrackQuickChatOpenKt")
  }

  beforeEach {
    val application = mockApplication()
    every { application.getService(GitLabUserService::class.java) } returns userService
    every { project.getService(ChatApiClient::class.java) } returns mockk()

    every { QuickChatUIFactory.create() } returns ui
    every { editor.addComponentInlay(any(), any(), any<QuickChatUI>()) } returns inlay

    every { editor.getUserData(QUICK_CHAT_SESSION_KEY) } returns null
    every { editor.selectionModel.hasSelection() } returns false
    every { editor.caretModel.offset } returns 5

    every { trackQuickChatOpen(openMethod) } returns Unit
  }

  afterEach {
    clearAllMocks()
  }

  afterSpec {
    unmockkAll()
  }

  it("should close quick chat session if already opened on a different line") {
    val session = mockk<QuickChatSession>(relaxUnitFun = true) {
      every { line() } returns 10
    }
    every { editor.caretModel.visualPosition } returns VisualPosition(12, 0)
    every { editor.getUserData(QUICK_CHAT_SESSION_KEY) } returns session

    service.openChat(editor, openMethod)

    verify { session.close() }
  }

  it("should not close quick chat session if already opened on the same line") {
    val session = mockk<QuickChatSession>(relaxUnitFun = true) {
      every { line() } returns 10
    }
    every { editor.caretModel.visualPosition } returns VisualPosition(10, 0)
    every { editor.getUserData(QUICK_CHAT_SESSION_KEY) } returns session

    service.openChat(editor, openMethod)

    verify(exactly = 0) { session.close() }
  }

  it("should open the chat at the caret offset if there is no selection") {
    every { editor.selectionModel.hasSelection() } returns false
    every { editor.caretModel.offset } returns 10

    service.openChat(editor, openMethod)

    verify {
      editor.addComponentInlay(
        10,
        match { it.priority == 1 },
        ui
      )
    }

    ui.requestFocus()
  }

  it("should open the chat at the selection end offset if there is a selection") {
    every { editor.selectionModel.hasSelection() } returns true
    every { editor.selectionModel.selectionEnd } returns 20
    every { editor.caretModel.offset } returns 10

    service.openChat(editor, openMethod)

    verify {
      editor.addComponentInlay(
        20,
        match { it.priority == 1 },
        ui
      )

      ui.requestFocus()
    }
  }

  it("should save the new quick chat session once created") {
    service.openChat(editor, openMethod)

    verify { editor.putUserData(QUICK_CHAT_SESSION_KEY, any<QuickChatSession>()) }
  }

  it("should create a gutter icon at the inlay line") {
    every { inlay.visualPosition } returns VisualPosition(10, 0)
    every { editor.visualToLogicalPosition(VisualPosition(10, 0)) } returns LogicalPosition(20, 0)

    service.openChat(editor, openMethod)

    verify {
      editor.markupModel.addLineHighlighter(20, HighlighterLayer.CARET_ROW, null)
    }
  }

  it("should scroll to the quick chat") {
    service.openChat(editor, openMethod)

    verify { editor.scrollingModel.scrollTo(any<LogicalPosition>(), ScrollType.MAKE_VISIBLE) }
  }

  it("should track quick chat open") {
    service.openChat(editor, openMethod)

    verify { editor.scrollingModel.scrollTo(any<LogicalPosition>(), ScrollType.MAKE_VISIBLE) }
  }
})
