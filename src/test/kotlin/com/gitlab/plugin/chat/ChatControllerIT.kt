package com.gitlab.plugin.chat

import com.gitlab.plugin.chat.context.*
import com.gitlab.plugin.chat.context.managers.LanguageServerContextManager
import com.gitlab.plugin.chat.view.CefChatBrowser
import com.gitlab.plugin.chat.view.WebViewChatView
import com.gitlab.plugin.chat.view.model.*
import com.gitlab.plugin.lsp.MockLanguageServer
import com.gitlab.plugin.services.DuoChatStateService
import com.google.gson.JsonParser
import com.intellij.testFramework.*
import io.kotest.assertions.nondeterministic.eventually
import io.kotest.matchers.shouldBe
import io.mockk.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import kotlinx.serialization.serializer
import org.junit.jupiter.api.*
import kotlin.time.Duration.Companion.seconds

class ChatControllerIT : HeavyPlatformTestCase() {
  private var preSetUpHook: (() -> Unit)? = null
  private var afterTearDownHook: (() -> Unit)? = null
  private lateinit var chatController: ChatController
  private lateinit var testServer: MockLanguageServer

  @BeforeEach
  public override fun setUp() {
    // For tests requiring the mock language server, the mock server must be started before services are created
    preSetUpHook?.invoke()

    super.setUp()
  }

  @AfterEach
  override fun tearDown() {
    afterTearDownHook?.invoke()

    runInEdtAndWait {
      super.tearDown()
    }
  }

  @Nested
  inner class `With language server` {
    private lateinit var view: WebViewChatView
    private lateinit var viewOnMessageCallback: ((message: ChatViewMessage) -> Unit)

    init {
      preSetUpHook = {
        testServer = MockLanguageServer()
        testServer.start()
      }

      afterTearDownHook = {
        testServer.stop()
      }
    }

    @BeforeEach
    fun setUp() {
      view = spyk(
        WebViewChatView(
          chatBrowser = mockk<CefChatBrowser>(relaxed = true),
          toolWindowManager = mockk(relaxed = true),
          duoChatStateService = mockk<DuoChatStateService>(relaxed = true)
        )
      )

      every { view.onMessage(captureLambda()) } answers {
        viewOnMessageCallback = lambda<(message: ChatViewMessage) -> Unit>().captured
      }

      testServer.setMethodResponse(
        "$/gitlab/ai-context/get-provider-categories",
        """
          {
            "result": ["file"]
          }
        """.trimIndent()
      )
      testServer.setMethodResponse(
        "$/gitlab/ai-context/retrieve",
        """
            {
              "result": []
            }
        """.trimIndent()
      )

      @Suppress("InjectDispatcher")
      chatController = ChatController(
        chatView = view,
        project = project,
        contextManager = LanguageServerContextManager(project),
        coroutineScope = CoroutineScope(Dispatchers.Default)
      )

      testServer.clearReceivedRequests()
    }

    @Test
    fun `updates current context items on ContextItemAddedMessage`() {
      val item = AiContextItem(
        id = "abc123",
        category = AiContextCategory.FILE,
        content = "some content",
        metadata = AIContextItemMetadata(
          title = "test.js",
          enabled = true,
          subType = AIContextProviderType.OPEN_TAB
        )
      )

      testServer.setMethodResponse(
        "$/gitlab/ai-context/retrieve",
        """
          {
            "result": ${Json.encodeToJsonElement(serializer(), listOf(item))}
          }
        """.trimIndent()
      )

      lateinit var requests: List<String>

      // Check that the request to add the item was made to the language server
      runBlocking {
        viewOnMessageCallback(ContextItemAddedMessage(item))

        eventually(5.seconds) {
          requests = testServer.requestsForMethod("$/gitlab/ai-context/add")
          requests.size shouldBe 1
        }
      }

      // Check that the request to add items was correctly serialized
      val params = JsonParser.parseString(requests[0]).asJsonObject["params"].asJsonObject
      params["id"].asString shouldBe "abc123"
      params["category"].asString shouldBe "file"
      params["content"].asString shouldBe "some content"

      val metadata = params["metadata"].asJsonObject
      metadata["title"].asString shouldBe "test.js"
      metadata["enabled"].asBoolean shouldBe true
      metadata["subType"].asString shouldBe "open_tab"

      // Check that the request to retrieve the current context items was made to the language server
      runBlocking {
        eventually(5.seconds) {
          testServer.requestsForMethod("$/gitlab/ai-context/retrieve").size shouldBe 1
        }
      }

      // Check that the message was sent to update current context items
      verify { view.postMessage(ContextCurrentItemsResultMessage(listOf(item))) }
    }

    @Test
    fun `updates current context items on ContextItemAddedRemoved `() {
      val item = AiContextItem(
        id = "abc123",
        category = AiContextCategory.FILE,
        content = "some content",
        metadata = AIContextItemMetadata(
          title = "test.js",
          enabled = true,
          subType = AIContextProviderType.OPEN_TAB
        )
      )

      lateinit var requests: List<String>

      // Check that the request to remove the item was made to the language server
      runBlocking {
        viewOnMessageCallback(ContextItemRemovedMessage(item))

        eventually(5.seconds) {
          requests = testServer.requestsForMethod("$/gitlab/ai-context/remove")
          requests.size shouldBe 1
        }
      }

      // Check that the request to remove items was correctly serialized
      val params = JsonParser.parseString(requests[0]).asJsonObject["params"].asJsonObject
      params["id"].asString shouldBe "abc123"
      params["category"].asString shouldBe "file"
      params["content"].asString shouldBe "some content"

      val metadata = params["metadata"].asJsonObject
      metadata["title"].asString shouldBe "test.js"
      metadata["enabled"].asBoolean shouldBe true
      metadata["subType"].asString shouldBe "open_tab"

      // Check that the request to retrieve the current context items was made to the language server
      runBlocking {
        eventually(5.seconds) {
          testServer.requestsForMethod("$/gitlab/ai-context/retrieve").size shouldBe 1
        }
      }

      // Check that the message was sent to update current context items
      verify { view.postMessage(ContextCurrentItemsResultMessage(listOf())) }
    }

    @Test
    fun `sets context item search results on ContextItemSearchQueryMessage`() {
      val projectDir = PlatformTestUtil.getOrCreateProjectBaseDir(project)

      val item = AiContextItem(
        id = "abc123",
        category = AiContextCategory.FILE,
        content = "some content",
        metadata = AIContextItemMetadata(
          title = "index.js",
          enabled = true,
          subType = AIContextProviderType.OPEN_TAB
        )
      )

      testServer.setMethodResponse(
        "$/gitlab/ai-context/query",
        """
        {
          "result": ${Json.encodeToJsonElement(serializer(), listOf(item))}
        }
        """.trimIndent()
      )

      lateinit var requests: List<String>

      // Check that the query was made to the language server
      runBlocking {
        viewOnMessageCallback(ContextItemSearchQueryMessage("index", AiContextCategory.FILE))

        eventually(4.seconds) {
          requests = testServer.requestsForMethod("$/gitlab/ai-context/query")
          requests.size shouldBe 1
        }
      }

      val params = JsonParser.parseString(requests[0]).asJsonObject["params"].asJsonObject
      params["category"].asString shouldBe "file"
      params["query"].asString shouldBe "index"

      val workspaceFoldersParams = params["workspaceFolders"].asJsonArray
      workspaceFoldersParams.size() shouldBe 1

      val workspaceFoldersParam = workspaceFoldersParams[0].asJsonObject
      workspaceFoldersParam["uri"].asString shouldBe "${projectDir.url}/"
      workspaceFoldersParam["name"].asString shouldBe projectDir.name

      // Check that the message was sent to update search result items
      runBlocking {
        eventually(5.seconds) {
          verify { view.postMessage(ContextItemsSearchResultMessage(listOf(item))) }
        }
      }
    }

    @Test
    fun `updates context categories on SlashCommandsOpenedMessage`() {
      testServer.setMethodResponse(
        "$/gitlab/ai-context/get-provider-categories",
        """
          {
            "result": ["file"]
          }
        """.trimIndent()
      )

      lateinit var requests: List<String>

      viewOnMessageCallback(SlashCommandsOpenedMessage)

      // Check that the query was made to the language server
      runBlocking {
        eventually(5.seconds) {
          requests = testServer.requestsForMethod("$/gitlab/ai-context/get-provider-categories")
          requests.size shouldBe 1
        }
      }

      // Check that the message was sent to update categories
      runBlocking {
        eventually(5.seconds) {
          verify { view.postMessage(ContextCategoriesResultMessage(listOf(AiContextCategory.FILE))) }
        }
      }
    }

    @Test
    fun `updates recognized categories on SlashCommandsOpenedMessage when language server response includes unrecognized categories`() {
      testServer.setMethodResponse(
        "$/gitlab/ai-context/get-provider-categories",
        """
          {
            "result": ["some_unrecognized_category", "file"]
          }
        """.trimIndent()
      )

      lateinit var requests: List<String>

      viewOnMessageCallback(SlashCommandsOpenedMessage)

      // Check that the query was made to the language server
      runBlocking {
        eventually(5.seconds) {
          requests = testServer.requestsForMethod("$/gitlab/ai-context/get-provider-categories")
          requests.size shouldBe 1
        }
      }

      // Check that the message was sent to update categories
      runBlocking {
        eventually(5.seconds) {
          verify { view.postMessage(ContextCategoriesResultMessage(listOf(AiContextCategory.FILE))) }
        }
      }
    }
  }

  // Avoids error thrown on HeavyPlatformTestCase setup, which expects to be able to find a file name
  // using a different annotation style than the JUnit5 annotations used here in order to nest tests and setup
  override fun getName(): String {
    return if (super.getName() == null) {
      // Provide a default test name for nested tests
      "ChatControllerIT"
    } else {
      super.getName()
    }
  }
}
