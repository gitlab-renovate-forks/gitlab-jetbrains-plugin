package com.gitlab.plugin.authentication

import com.gitlab.plugin.api.mockApplication
import com.gitlab.plugin.ui.GitLabNotificationManager
import com.gitlab.plugin.ui.NotificationGroupType
import com.intellij.execution.ExecutionException
import com.intellij.execution.configurations.GeneralCommandLine
import com.intellij.execution.process.ProcessOutput
import com.intellij.execution.util.ExecUtil
import com.intellij.notification.NotificationType
import com.intellij.openapi.vfs.encoding.EncodingManager
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.shouldBe
import io.mockk.*

class OnePasswordTokenProviderTest : DescribeSpec({
  val output = mockk<ProcessOutput>(relaxed = true)

  val settings = mockk<DuoPersistentSettings>(relaxed = true)
  var provider = OnePasswordTokenProvider()

  beforeSpec {
    mockkConstructor(GitLabNotificationManager::class)
  }

  beforeEach {
    val application = mockApplication()
    every { application.getService(DuoPersistentSettings::class.java) } returns settings
    every { application.getService(EncodingManager::class.java) } returns mockk(relaxed = true)

    every {
      anyConstructed<GitLabNotificationManager>().sendNotification(
        any(),
        NotificationGroupType.IMPORTANT,
        NotificationType.ERROR
      )
    } returns Unit

    every { output.exitCode } returns 0
    every { output.stderrLines } returns emptyList()
    every { output.stdoutLines } returns emptyList()

    every { settings.integrate1PasswordCLI } returns true
    every { settings.integrate1PasswordAccount } returns null

    provider = OnePasswordTokenProvider()
  }

  afterEach {
    clearAllMocks(answers = false)
  }

  afterSpec {
    unmockkAll()
  }

  fun executeInMockCommand(test: () -> Unit) {
    val mock = mockkStatic(ExecUtil::class) {
      every { ExecUtil.execAndGetOutput(any()) } returns output

      test()
    }

    unmockkStatic(mock::class)
  }

  describe("token") {
    it("should not return a token if 1password is not configured") {
      every { settings.integrate1PasswordCLI } returns false

      provider.token() shouldBe ""
    }

    it("should update the cached token when the configured secret reference changes") {
      every { output.exitCode } returns 0

      executeInMockCommand {
        // Request a token for a secret for the first time
        every { settings.integrate1PasswordCLISecretReference } returns "op://Private/GitLab Personal Access Token/token"
        every { output.stdoutLines } returns listOf("glpat-xxxxxxxx")
        provider.token() shouldBe "glpat-xxxxxxxx"

        // Request a token for a new secret for the first time
        every { settings.integrate1PasswordCLISecretReference } returns "op://Private/GitLab Personal Access Token/new_token"
        every { output.stdoutLines } returns listOf("glpat-yyyyyyyy")
        provider.token() shouldBe "glpat-yyyyyyyy"

        // Check that new token has been cached
        provider.token() shouldBe "glpat-yyyyyyyy"

        val commandLines = mutableListOf<GeneralCommandLine>()
        verify(exactly = 2) { ExecUtil.execAndGetOutput(capture(commandLines)) }

        commandLines[0].commandLineString shouldBe "op read \"op://Private/GitLab Personal Access Token/token\""
        commandLines[1].commandLineString shouldBe "op read \"op://Private/GitLab Personal Access Token/new_token\""
      }
    }

    it("should update the cached token when the configured secret reference changes but the token is the same") {
      every { output.exitCode } returns 0
      every { output.stdoutLines } returns listOf("glpat-xxxxxxxx")

      executeInMockCommand {
        // Request a token for a secret for the first time
        every { settings.integrate1PasswordCLISecretReference } returns "op://Private/GitLab Personal Access Token/token"
        provider.token() shouldBe "glpat-xxxxxxxx"

        // Request a token for a new secret for the first time
        every { settings.integrate1PasswordCLISecretReference } returns "op://Private/GitLab Personal Access Token/new_token"
        every { output.stdoutLines } returns listOf("glpat-xxxxxxxx")
        provider.token() shouldBe "glpat-xxxxxxxx"

        // Check that new token has been cached
        provider.token() shouldBe "glpat-xxxxxxxx"

        val commandLines = mutableListOf<GeneralCommandLine>()
        verify(exactly = 2) { ExecUtil.execAndGetOutput(capture(commandLines)) }

        commandLines[0].commandLineString shouldBe "op read \"op://Private/GitLab Personal Access Token/token\""
        commandLines[1].commandLineString shouldBe "op read \"op://Private/GitLab Personal Access Token/new_token\""
      }
    }

    it("should run the correct command when setting for 1Password account is stored") {
      every { output.exitCode } returns 0
      every { output.stdoutLines } returns listOf("glpat-xxxxxxxx")

      executeInMockCommand {
        every { settings.integrate1PasswordCLISecretReference } returns "op://Private/GitLab Personal Access Token/token"
        every { settings.integrate1PasswordAccount } returns "myaccount.1password.com"
        provider.token() shouldBe "glpat-xxxxxxxx"

        val commandLines = mutableListOf<GeneralCommandLine>()
        verify(exactly = 1) { ExecUtil.execAndGetOutput(capture(commandLines)) }

        commandLines[0].commandLineString shouldBe "op read \"op://Private/GitLab Personal Access Token/token\" --account myaccount.1password.com"
      }
    }

    it("should update the cached token when the stored 1Password account changes but the token is the same") {
      every { output.exitCode } returns 0
      every { output.stdoutLines } returns listOf("glpat-xxxxxxxx")

      executeInMockCommand {
        // Request a token for a secret for the first time
        every { settings.integrate1PasswordCLISecretReference } returns "op://Private/GitLab Personal Access Token/token"
        every { settings.integrate1PasswordAccount } returns "myaccount.1password.com"
        provider.token() shouldBe "glpat-xxxxxxxx"

        // Request a token for a new account for the first time
        every { settings.integrate1PasswordAccount } returns "myotheraccount.1password.com"
        every { output.stdoutLines } returns listOf("glpat-yyyyyyyy")
        provider.token() shouldBe "glpat-yyyyyyyy"

        // Check that new token has been cached
        provider.token() shouldBe "glpat-yyyyyyyy"

        val commandLines = mutableListOf<GeneralCommandLine>()
        verify(exactly = 2) { ExecUtil.execAndGetOutput(capture(commandLines)) }

        commandLines[0].commandLineString shouldBe
          "op read \"op://Private/GitLab Personal Access Token/token\" --account myaccount.1password.com"
        commandLines[1].commandLineString shouldBe
          "op read \"op://Private/GitLab Personal Access Token/token\" --account myotheraccount.1password.com"
      }
    }

    listOf(
      "op://Private/GitLab Personal Access Token/token",
      "\"op://Private/GitLab Personal Access Token/token\""
    ).forEach { secret ->
      it("should cache the token for the configured secret reference if 1password exit code is 0") {
        every { settings.integrate1PasswordCLISecretReference } returns secret
        every { output.exitCode } returns 0
        every { output.stdoutLines } returns listOf("glpat-xxxxxxxx")

        executeInMockCommand {
          provider.token() shouldBe "glpat-xxxxxxxx"
          provider.token() shouldBe "glpat-xxxxxxxx"

          val commandLine = slot<GeneralCommandLine>()
          verify(exactly = 1) { ExecUtil.execAndGetOutput(capture(commandLine)) }

          commandLine.captured.commandLineString shouldBe "op read \"op://Private/GitLab Personal Access Token/token\""
        }
      }

      it("should return an empty string if 1password exit code is not 0 for configured secret") {
        every { settings.integrate1PasswordCLISecretReference } returns "op://Private/GitLab Personal Access Token/token"
        every { output.exitCode } returns 1

        executeInMockCommand {
          provider.token() shouldBe ""

          verify(exactly = 1) {
            anyConstructed<GitLabNotificationManager>().sendNotification(
              any(),
              NotificationGroupType.IMPORTANT,
              NotificationType.ERROR
            )
          }
        }
      }

      it("should return an empty string if it encounters an execution error for configured secret") {
        every { settings.integrate1PasswordCLISecretReference } returns "op://Private/GitLab Personal Access Token/token"

        mockkStatic(ExecUtil::class) {
          every { ExecUtil.execAndGetOutput(any()) } throws ExecutionException("Failed to execute command")

          provider.token() shouldBe ""

          verify(exactly = 1) {
            anyConstructed<GitLabNotificationManager>().sendNotification(
              any(),
              NotificationGroupType.IMPORTANT,
              NotificationType.ERROR
            )
          }
        }
      }
    }
  }

  describe("getTokenBySecretReference") {
    listOf(
      "op://Private/GitLab Personal Access Token/token",
      "\"op://Private/GitLab Personal Access Token/token\""
    ).forEach { secret ->
      it("should not cache the token for the secret reference input if 1password exit code is 0") {
        every { output.exitCode } returns 0
        every { output.stdoutLines } returns listOf("glpat-xxxxxxxx")

        executeInMockCommand {
          provider.getTokenBySecretReference(secret) shouldBe "glpat-xxxxxxxx"
          provider.getTokenBySecretReference(secret) shouldBe "glpat-xxxxxxxx"

          val commandLines = mutableListOf<GeneralCommandLine>()
          verify(exactly = 2) { ExecUtil.execAndGetOutput(capture(commandLines)) }

          commandLines[0].commandLineString shouldBe "op read \"op://Private/GitLab Personal Access Token/token\""
          commandLines[1].commandLineString shouldBe "op read \"op://Private/GitLab Personal Access Token/token\""
        }
      }

      it("should return an empty string if 1password exit code is not 0 for input secret") {
        every { output.exitCode } returns 1

        executeInMockCommand {
          provider.getTokenBySecretReference("op://Private/GitLab Personal Access Token/token") shouldBe ""

          verify(exactly = 1) {
            anyConstructed<GitLabNotificationManager>().sendNotification(
              any(),
              NotificationGroupType.IMPORTANT,
              NotificationType.ERROR
            )
          }
        }
      }

      it("should return an empty string if it encounters an execution error for onput secret") {
        mockkStatic(ExecUtil::class) {
          every { ExecUtil.execAndGetOutput(any()) } throws ExecutionException("Failed to execute command")

          provider.getTokenBySecretReference("op://Private/GitLab Personal Access Token/token") shouldBe ""

          verify(exactly = 1) {
            anyConstructed<GitLabNotificationManager>().sendNotification(
              any(),
              NotificationGroupType.IMPORTANT,
              NotificationType.ERROR
            )
          }
        }
      }

      it("should run the correct command when account provided") {
        every { output.exitCode } returns 0
        every { output.stdoutLines } returns listOf("glpat-xxxxxxxx")

        executeInMockCommand {
          provider.getTokenBySecretReference("op://Private/GitLab Personal Access Token/token", "myaccount.1password.com") shouldBe "glpat-xxxxxxxx"

          val commandLine = slot<GeneralCommandLine>()
          verify(exactly = 1) { ExecUtil.execAndGetOutput(capture(commandLine)) }

          commandLine.captured.commandLineString shouldBe
            "op read \"op://Private/GitLab Personal Access Token/token\" --account myaccount.1password.com"
        }
      }
    }
  }

  describe("getAccounts") {
    it("returns an empty list when exit code is not 0") {
      every { output.exitCode } returns 1
      every { output.stdoutLines } returns listOf("some string")

      executeInMockCommand {
        provider.getAccounts() shouldBe emptyList()
      }
    }

    it("returns an empty list when command raises exception") {
      mockkStatic(ExecUtil::class) {
        every { ExecUtil.execAndGetOutput(any()) } throws ExecutionException("Failed to execute command")

        provider.getAccounts() shouldBe emptyList()
      }
    }

    it("returns the expected list of accounts when exit code is 0") {
      every { output.exitCode } returns 0
      every { output.stdoutLines } returns listOf(
        "URL                        EMAIL                           USER ID",
        "gitlab.1password.com       fake@gitlab.com         AAAABBBBBBCCCCCCC",
        "mypersonalacccount.1password.com    fake@gmail.com    DDDDDDEEEEEEFFFFF"
      )

      val expectedAccounts = listOf(
        OnePasswordTokenProvider.Account(url = "gitlab.1password.com", email = "fake@gitlab.com"),
        OnePasswordTokenProvider.Account(url = "mypersonalacccount.1password.com", email = "fake@gmail.com")
      )

      executeInMockCommand {
        provider.getAccounts() shouldBe expectedAccounts

        val commandLines = mutableListOf<GeneralCommandLine>()
        verify(exactly = 1) { ExecUtil.execAndGetOutput(capture(commandLines)) }

        commandLines[0].commandLineString shouldBe "op account list"
      }
    }
  }
})
