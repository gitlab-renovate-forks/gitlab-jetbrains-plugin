package com.gitlab.plugin.authentication

import com.gitlab.plugin.api.mockApplication
import com.intellij.collaboration.auth.services.OAuthCredentialsAcquirer
import com.intellij.collaboration.auth.services.OAuthCredentialsAcquirerHttp
import com.intellij.openapi.components.service
import com.intellij.util.Url
import com.intellij.util.application
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import io.kotest.matchers.types.beInstanceOf
import io.ktor.http.*
import io.mockk.*
import kotlinx.datetime.Clock
import org.jetbrains.ide.BuiltInServerManager
import java.net.http.HttpResponse

class GitLabOAuthServiceTest : DescribeSpec({
  val oAuthTokenProvider = mockk<OAuthTokenProvider>()
  val oauthService = GitLabOAuthService()
  val responseMock = mockk<HttpResponse<String>>()

  beforeTest {
    mockApplication()
    mockkObject(DuoPersistentSettings)
    every { DuoPersistentSettings.getInstance().oauthEnabled = any() } just Runs
    every { application.service<OAuthTokenProvider>() } returns oAuthTokenProvider
    every { oAuthTokenProvider.updateToken(any()) } just Runs

    mockkObject(OAuthCredentialsAcquirerHttp)
    every { responseMock.statusCode() } returns HttpStatusCode.OK.value
    every { OAuthCredentialsAcquirerHttp.requestToken(any()) } returns responseMock
  }

  describe("revokeToken") {
    it("should revoke token and update settings") {
      val token = "test_token"

      oauthService.revokeToken(token)

      verify(exactly = 1) { DuoPersistentSettings.getInstance().oauthEnabled = false }
      verify(exactly = 1) { oAuthTokenProvider.updateToken(null) }
    }
  }

  describe("refreshToken") {
    val oldToken = GitLabAuthorizationToken(
      "old_token",
      "refresh_token",
      3600,
      Clock.System.now().epochSeconds - 3601
    )

    beforeEach {
      mockkObject(BuiltInServerManager)
      every { BuiltInServerManager.getInstance().port } returns 12345
    }

    it("should return success result with token when request successful") {
      val newTokenCreatedAt = System.currentTimeMillis()
      every {
        responseMock.body()
      } returns """
        {
          "access_token":"new_token",
          "token_type":"Bearer",
          "expires_in":3600,
          "refresh_token":"new_refresh_token",
          "scope":"api",
          "created_at":$newTokenCreatedAt
        }
      """.trimIndent()

      val result = oauthService.refreshToken(oldToken)
      val urlSlot = slot<Url>()
      verify(exactly = 1) { OAuthCredentialsAcquirerHttp.requestToken(capture(urlSlot)) }

      val capturedUrl = urlSlot.captured.toString()
      capturedUrl should { url ->
        url.contains("client_id=732efe7d78ba62b01d2a95cd3b02f3ca2a00c7cec32c01e060c1b7664a9aa367") &&
          url.contains("refresh_token=${oldToken.refreshToken}") &&
          url.contains("grant_type=refresh_token") &&
          url.contains("redirect_uri=http://127.0.0.1:12345/api/oauth/gitlab/authorization") &&
          url.contains("Content-Type=application/x-www-form-urlencoded")
      }

      result should beInstanceOf<OAuthCredentialsAcquirer.AcquireCredentialsResult.Success<GitLabAuthorizationToken>>()

      val token = (result as OAuthCredentialsAcquirer.AcquireCredentialsResult.Success<GitLabAuthorizationToken>).credentials
      token.accessToken shouldBe "new_token"
      token.refreshToken shouldBe "new_refresh_token"
      token.expiresIn shouldBe 3600
      token.createdAt shouldBe newTokenCreatedAt
    }

    it("should return error result when request unsuccessful") {
      val errorMessage = "The provided authorization grant is invalid, expired, or revoked"

      every { responseMock.statusCode() } returns 400
      every {
        responseMock.body()
      } returns """
        {
          "error": "invalid_grant",
          "error_description": "$errorMessage"
        }
      """.trimIndent()

      val result = oauthService.refreshToken(oldToken)
      result should beInstanceOf<OAuthCredentialsAcquirer.AcquireCredentialsResult.Error<GitLabAuthorizationToken>>()
      (result as OAuthCredentialsAcquirer.AcquireCredentialsResult.Error<GitLabAuthorizationToken>).description shouldContain errorMessage
    }
  }
})
