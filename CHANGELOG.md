# Changelog

## [Unreleased]

### Added

### Changed

### Removed

### Fixed

## [3.6.5] - 2025-03-05

### Fixed

- Update GitLab Language Server to a version that resolves code suggestions Open Tabs to be disabled ([!1558](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1558))

## [3.6.4] - 2025-03-04

### Changed

- Cancel ongoing code suggestions requests when a new one is made ([!1552](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1552))

### Fixed

- Resolved an issue that included a relative path in the Origin header when a GitLab instance hosted at a relative path ([!1555](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1555))

## [3.6.3] - 2025-02-27

### Fixed

- Fix proxy authentication with null login or password causing a NullPointerException ([!1517](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1517))

## [3.6.1] - 2025-02-06

### Fixed

- Include relative path in Duo Chat subscription URL ([!1462](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1462))

## [3.6.0] - 2025-01-21

### Added

- Add Duo explain action to the console context menu ([!1363](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1363))
- Support version 2025.1 of JetBrains IDEs ([!1451](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1451))

### Changed

- Duo Chat and Quick Chat now match the fonts and sizes selected in settings. ([!1383](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1383))

### Fixed

- Attempt reopens with backed off Duo Chat WebSocket closures ([![871](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1413))
- Extend timeout to acquire IntelliJ API ([!872](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1414))
- Fix blank Duo Chat window when external link clicked ([!790](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1418))
- Send file events to the Language Server in coroutine to avoid blocking the UI thread ([!1438](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1438))

## [3.5.5] - 2024-12-16

### Fixed

- Correctly ignore system chunks sent back in Duo Chat message stream ([!1396](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1396))
- Encode proxy login and password in HTTP proxy URL ([!1393](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1393))

## [3.5.4] - 2024-12-16

### Fixed

- Fixed a race condition causing WebSocket messages received before connection is established to be lost ([!1373](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1373))
- Language Server start is now handled asynchronously ([!1384](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1384))

## [3.5.3] - 2024-12-11

### Added

- Display information notification on Duo Chat EOFException ([!1338](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1338))
- Additional context items can now be added to Duo Chat using `/include` ([#118](https://gitlab.com/groups/gitlab-org/editor-extensions/-/epics/118))

### Changed

- Reduce code suggestions provider debounce from 250ms to 50ms ([!1359](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1359))

### Fixed

- Relative workspace folder uri sent to the language server ([!1317](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1317))
- Fix Duo Chat UI to make error messages visible ([!1355](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1355))
- Send RFC-3986 compliant URIs to the language server ([!1353](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1353))

## [3.5.0] - 2024-11-26

### Changed

- Made small changes to Duo Chat UI as part of UI library version upgrade ([!1267](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1267))

### Fixed

- Resolve IllegalStateException when multiple VCS_REPOSITORY_MAPPING_UPDATED events are received at the same time ([!1301](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1301))
- Pressing enter in Quick Chat without selected slash command causing an ArrayIndexOutOfBoundsException  ([!1304](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1304))

## [3.4.1] - 2024-11-13

### Changed

- Replace deprecated success icon ([!1258](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1258))

## [3.4.0] - 2024-11-12

### Added

- Added support for authenticated proxies when the language server is enabled ([!1242](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1242))

### Changed

- Improved current authentication method legibility in setting panel with icons and helper text ([!1217](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1217))
- Changed quick chat actions menu height to adjust dynamically ([!1216](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1216))
- Updated language server process to use JetBrains HTTP Proxy settings ([!1232](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1232))

### Fixed

- Fixed OAuth token refresh failures when using multiple JetBrains IDEs ([!1221](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1221))
- Fixed Duo Chat code block horizontal scrollbars being very bright in dark mode ([!1226](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1226))
- Fixed OAuth showing as enabled in settings when there is no longer a valid token ([!1252](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1252))

## [3.3.1] - 2024-10-29

### Changed

- Updated Quick Chat action hint styling for improved usability ([!1173](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1173))

### Removed

- Removed the GitLab icon in the dropdown for `Open Quick Chat`([!1172](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1172))

### Fixed

- Fixed "Code suggestions has been disabled" pop up appearing during project startup ([!1156](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1156))
- Fixed OAuth token refresh failures ([!1162](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1162))

## [3.3.0] - 2024-10-15

### Added

- Added the ability to choose a 1Password account to use with the 1Password integration ([!1085](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1085))

### Fixed

- Fixed multiple code suggestions icon showing up in the Gutter ([!1143](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1143), [!1144](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1144))
- Fixed Container Released exception in GitLabEditorFactoryListener ([!1151](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1151))

## [3.2.1] - 2024-10-09

### Added

- Added the ability to toggle Code Suggestions per language ([!1081](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1081))

### Changed

- Updated the GitLab Duo Chat icon ([!1083](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1083))
- Quick Chat no longer throws Bad Location exceptions on Windows ([!1123](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1123))
- Fix Quick Chat selected lines being incorrect when imports are collapsed ([!1124](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1124))
- Fixed Code Suggestions enabled language names displaying "GitLab Duo" in all items ([!1129](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1129))

### Fixed

- Respect relative path for instance URL for GraphQL operations ([!1088](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1088))
- Use unsaved changes for Verify Setup ([!1112](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1112))

## [3.1.0] - 2024-10-01

### Added

- Added the generate code suggestions screen in the user onboarding workflow ([!945](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/945))
- Added syntax highlighting to Markdown blocks while Duo Chat is streaming ([!1033](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1033))
- Updated the authentication tabs to disable the fields of the inactive authentication methods ([!1018](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1018))

### Fixed

- Fixed the Quick Chat gutter icon not appearing on the correct line ([!1053](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1053))
- Fixed PAT verification not working when GitLab instance URL includes a path ([!1048](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/1048))

## [3.0.2] - 2024-09-25

### Added

- Added the ability to trigger Duo Quick Chat within the editor([&80](https://gitlab.com/groups/gitlab-org/editor-extensions/-/epics/80))
- Added the user onboarding workflow([!853](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/853), [!863](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/863), [!876](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/876), [!906](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/906), [!917](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/917), [!945](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/945))

### Fixed

- Fixed LS error notification at startup ([!907](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/907))

## [2.12.0] - 2024-09-03

### Added

- Added OAuth as an authentication option for GitLab.com users (Epic: [&70](https://gitlab.com/groups/gitlab-org/editor-extensions/-/epics/70), Merge Request: [!771](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/771))
- Added the ability to insert code in the editor directly from the chat window ([!832](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/832))
- Added /fix command to the Duo Chat actions ([!891](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/891))

### Fixed

- Fixed GitLab instance URL being changed immediately instead of on "Apply" ([!883](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/883))
- Restored open tabs context which required GitLab Language Server 6.3.0 and later ([!887](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/887))
- Fixed styling of various components in Duo Chat ([!753](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/753))

## [2.10.2] - 2024-08-28

### Fixed

- Restored open tabs context which required GitLab Language Server 6.3.0 and later ([!887](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/887))

## [2.10.1] - 2024-08-14

### Fixed

- Fix slow running operation in 1Password authentication ([!818](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/818))

## [2.10.0] - 2024-08-13

### Added

- Added syntax highlighting for GitLab Duo Chat ([!765](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/765))
- Added ability to fetch CA certificates from the system certificate store ([!773](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/773))
- Added an option to pass CA certificate from the plugin to the Language Server, which may help users who use
proxy software in their environment ([!784](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/784))
- Added JCEF help window ([!768](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/786))
- Added list of supported languages for code suggestions that users can
  enable/disable ([!774](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/774))
- Added "Duo" label to status bar icons ([!812](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/812))

### Changed

- Moved authentication options (PAT & 1Password) in settings panel to a tabbed interface ([!695](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/695))
- Changed status bar widget to include additional actions ([!719](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/719))

### Fixed

- Fixed slash command not working in Duo Chat ([!785](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/785))
- Fixed styling of in-progress code blocks in Duo Chat messages ([!766](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/766))

## [2.9.0] - 2024-07-30

### Added

- Added ability to cancel user prompt ([!545](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/545))
- Added ability to cycle through code suggestions using `alt [` and `alt ]` ([&50](https://gitlab.com/groups/gitlab-org/editor-extensions/-/epics/50))

### Fixed

- Fixed over typing the suggestion making it disapear ([!758](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/758))

## [2.8.1] - 2024-07-24

### Fixed

- Fix Gutter Icon and Telemetry listeners not registered on clean install ([!728](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/728))
- Fix creating inline completion element with undefined width ([!729](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/729))

## [2.8.0] - 2024-07-23

### Added

- Improve Enable/Disable Code Suggestions text ([!712](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/712))

### Fixed

- Fix sending the wrong language identifier for some languages to the Language Server ([!720](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/720))
- Fix enabling Duo suggestions when user enters a correct access token ([!722](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/722))
- Fix token occasionally being malformed when settings are saved ([!721](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/721))
- Fix Language Server startup issues for new users ([!718](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/718))

## [2.7.0] - 2024-07-16

### Added

- Add a Setting for sending Open Tabs as Context ([!668](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/668))
- Add a hint about Language IDs to the additional languages setting ([!690](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/690))

### Changed

- Now there is a single configuration for telemetry, this will enable or disable telemetry both for Language Server enabled or disabled
  ([!676](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/676))
- Now there is a single configuration to ignore certificate errors ([!680](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/680))

### Removed

- Removed "Language Server" configuration page, now you can find these settings in the "Advanced" section ([!682](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/682))
- Removed ability to change telemetry destination, but it is still viewable in plugin settings
  ([!676](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/676))
- Removed secret redaction setting as it should be always enabled ([!679](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/679))

### Fixed

- Fixed additional languages setting being parsed incorrectly ([!690](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/690))
- Prevent "null" from appearing in the plugin version shown in Duo notification ([!684](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/684))
- Fixed certain settings not enabling "Apply" button in settings panel ([!703](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/703))

## [2.6.0] - 2024-07-10

### Changed

- Package Language Server binaries with the plugin ([!660](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/660))

## [2.5.0] - 2024-07-04

### Added

- Add code suggestions streaming support ([!634](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/634))

## [2.4.2] - 2024-07-03

### Fixed

- Fixed telemetry not being sent ([!640](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/640))

## [2.4.1] - 2024-07-03

### Fixed

- Fixed plugin settings incorrectly updating when Language Server is enabled ([!628](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/628))

## [2.4.0] - 2024-07-02

### Added

- Add language server support for code suggestions

### Fixed

- Fix icon status update on startup and settings change ([!532](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/532))
- Fix error on startup when user attempts to get code suggestions too early ([!553](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/553))

## [2.2.0] - 2024-06-04

### Added

- Add health check section in the plugin settings ([!429](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/429))

### Fixed

- Remove trailing slashes after configured host URL to avoid 404 Not Found while executing ChatSubscription. ([!440](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/440))
- Load access token on background thread when settings are opened. Disable Verify button and PAT text field until loaded. ([!511](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/511))

## [2.1.1] - 2024-05-22

### Added

- Shift the focus on the DuoChat window when invoking DuoChat actions ([!420](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/420))
- Display user notifications where errors are occurring ([!427](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/427))
- Indent and format code suggestions based on the code style settings ([!410](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/410))

## [2.1.0] - 2024-05-15

### Added

- Add GitLab Duo: Accept Code Suggestion shortcut under the GitLab Duo group ([!415](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/415))
- Integrate with 1Password CLI to provide personal access tokens ([!357](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/357), [Learn more](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/149736))
- Display indicator in the Gutter for code suggestions ([!423](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/423))

## [2.0.3] - 2024-05-08

### Added

- Show/hide the Duo Chat tool window with the new <kbd>alt</kbd>+<kbd>D</kbd> keyboard shortcut ([!398](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/398))
- Add Beta label to GitLab Duo settings when running pre-release builds ([!390](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/390))
- Display balloon notifications to alert users when a personal access token validation fails ([!391](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/391))

### Changed

- Group Duo Chat and Code Suggestions settings under **Settings > GitLab Duo > Features** ([!403](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/403))

### Fixed

- Token updates no longer require the user to reopen the project ([!394](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/394))
- Under **Settings > GitLab Duo > Advanced**, selecting **Enable Duo Chat** now displays or hides the chat tool window ([!380](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/380))

## [2.0.2] - 2024-04-18

### Fixed

- Toggling Duo Chat features in the settings panel now displays or hides context menu actions ([!380](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/380))

## [2.0.1] - 2024-04-18

### Fixed

- Update GitLab Duo actions prefix from `DuoChat` to `GitLab Duo Chat` ([!377](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/377))

## [2.0.0] - 2024-04-17

### Added

- Add the new GitLab Duo Chat tool window ([!237](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/237))
- Add right-click actions for the /refactor, /explain, and /tests Chat commands  ([!260](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/260))
- Add configurable key maps for the /refactor, /explain, and /tests Chat commands ([!260](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/260))
- Disable the status in the GitLab Duo icon tooltip when the opened project has GitLab Duo disabled ([!334](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/334))
- Display a notification when the opened project has GitLab Duo disabled ([!323](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/323))
- Set up a JetBrains Marketplace release channel for preview builds ([#224](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/224))
- Allow for switching between light and dark themes in the Chat view ([!328](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/328))
- Add support for ignoring certificate errors which are _expected_ by user with Deep Packet Inspection/TLS intercepting
  proxies. ([!341](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/341))

  Users should verify their specific GitLab SSL configuration, and attempt to use the JetBrains and system certificate
  settings before using this option.
- Check PAT scope when opening a project or verifying setup, and show a notification if scopes are missing. ([#203](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/203))

### Changed

- Update plugin setup instructions to use the `api` PAT scope ([!313](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/313)). This scope is required for the upcoming Duo Chat feature.

### Fixed

- Specify the action update thread to be EDT for ChatActionGroup and OpenChatAction. ([!353](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/353))
- Allow token verification to be done without opening a project. ([!362](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/362))
- Support Personal Access Tokens with only `api` scope ([!349](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/349))

  Previously including the `ai_features` scope was required for Duo Code Suggestions by the plugin.
- Refresh API clients base URL when GitLab Duo settings are updated. ([!369](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/369))

## [1.0.1] - 2024-02-23

### Fixed

- Handle 404 response during setup verification for GitLab.com ([!300](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/300))

## [1.0.0] - 2024-02-14

### Added

- Code Suggestions now requires a [Duo Pro](https://about.gitlab.com/gitlab-duo/) license. Display Code Suggestions [license](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/#required-subscription) provisioning status in Duo icon tooltip ([#191](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/191))
- Code Suggestions now requires GitLab version 16.8 or later. Display warning in GitLab Duo icon tooltip for unsupported instances. ([#189](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/189))

### Changed

- Update setup instructions to include `read_user` PAT scope ([!275](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/275))

## [0.5.6] - 2024-01-23

### Added

- Support for Code Suggestions in `sh`, `html`, and `css` files ([#136](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/136))
- Support for 2024.1 EAP IDEs ([#182](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/182))

## [0.5.5] - 2024-01-10

### Fixed

- Improve error message shown when user verifies settings ([#134](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/134))
- Verify Setup in settings fails when switching GitLab instance URLs ([#133](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/133))

## [0.5.4] - 2023-12-14

### Fixed

- Fix bug where reporting an error sometimes led to a 400 Bad Request error page ([#138](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/138))

## [0.5.3] - 2023-12-07

### Changed

- Require a slightly newer IDE build to prevent installation in incompatible IDEs such as
  RustRover ([#149](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/149))

## [0.5.2] - 2023-12-04

### Changed

- Open GitLab issue form when error reports are submitted in the
  IDE ([!184](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/184))
- Reduce debounce delay for code
  suggestions ([!207](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/207))

## [0.5.1] - 2023-11-13

### Changed

- Increase request and socket timeouts to accommodate longer
  requests ([!181](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/181))

## [0.5.0] - 2023-11-03

### Changed

- Update plugin to be compatible with 2023.3 EAP (2023.2 and earlier IDEs are not supported by this
  version) ([!152](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/152))

## [0.4.3] - 2023-10-30

### Changed

- Update GitLab Duo logo ([#107](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/107))

## [0.4.2] - 2023-09-28

### Changed

- Improve exception notification
  messages ([!121](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/121))
- Expand supported file extensions for code
  suggestions ([!136](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/136))

## [0.4.1] - 2023-09-20

### Changed

- Added debounce delay of 1 second in suggestions request to avoid suggestions during
  typing ([#77](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/77))
- Only request suggestion if at end of line or after the cursor are only special
  characters ([#96](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/96))

## [0.4.0] - 2023-09-15

### Added

- Add settings to enabled/disable auto-completion
  ([#48](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/48))

### Changed

- Improves error message when server instance is not configured correctly
  ([#88](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/88))
- Improve the Settings UI ([#92](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/92))
- Update plugin to be compatible with 2023.2.2 IDEs (previous versions are not
  supported) ([!111](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/111))

## [0.3.0] - 2023-09-07

### Added

- Uses the IDE proxy settings if they are configured
  ([#74](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/74))

### Fixed

- Plugin should not require a restart when installing or toggling its
  visibility ([#85](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/85))

## [0.2.0] - 2023-09-04

### Added

- Adds button to verify setup of GitLab Code Suggestions
  ([#63](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/63))

### Changed

- Avoids making a request for suggestion if there's not enough content
  ([#83](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/83))

### Fixed

- Suggestions can be requested from GitLab servers at version 16.3.0-ee
  ([#82](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/82))

## [0.1.3] - 2023-08-28

### Added

- Add setting for users to control code suggestions
  telemetry ([#67](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/67))

### Changed

- Improves UX on setting up Duo Plugin
  ([#63](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/63))
- Uses GitLab API endpoint if GitLab server version is 16.3 or above
  ([#51](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/51))
- Give priority to code suggestion over built-in intellisense while pressing tab key
  ([#71](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/71))
- Update supported file
  extensions ([#58](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/58))

### Fixed

- Ensure plugin restart when installing or
  updating ([#70](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/70))
- Partially fixes suggestions crashing when triggered from within parenthesis and brackets
  ([#75](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/75))

## [0.1.2] - 2023-08-15

### Changed

- Rename plugin to GitLab
  Duo ([#55](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/55))

### Fixed

- Status indicator is not getting refreshed while adding/removing
  token ([#36](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/36))
- GitLab Instance URL with dash(es) not accepted by the
  plugin ([#59](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/59))
  by [@Prezu](https://gitlab.com/Prezu)
- Handle offline exceptions and show them in the
  UI ([!41](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/merge_requests/41))
- Plugin breaks in some IDEs due to wrong
  imports ([#61](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/61))

## [0.1.1] - 2023-07-21

### Added

- Settings screen to configure a GitLab instance and credentials for the Plugin
- Status bar icon to track configuration and busy/ready state
- GitLab Duo Code Suggestions initial integration

[Unreleased]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v3.6.5...HEAD
[3.6.5]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v3.6.4...v3.6.5
[3.6.4]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v3.6.3...v3.6.4
[3.6.3]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v3.6.1...v3.6.3
[3.6.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v3.6.0...v3.6.1
[3.6.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v3.5.5...v3.6.0
[3.5.5]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v3.5.4...v3.5.5
[3.5.4]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v3.5.3...v3.5.4
[3.5.3]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v3.5.0...v3.5.3
[3.5.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v3.4.1...v3.5.0
[3.4.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v3.4.0...v3.4.1
[3.4.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v3.3.1...v3.4.0
[3.3.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v3.3.0...v3.3.1
[3.3.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v3.2.1...v3.3.0
[3.2.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v3.1.0...v3.2.1
[3.1.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v3.0.2...v3.1.0
[3.0.2]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.12.0...v3.0.2
[2.12.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.10.2...v2.12.0
[2.10.2]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.10.1...v2.10.2
[2.10.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.10.0...v2.10.1
[2.10.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.9.0...v2.10.0
[2.9.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.8.1...v2.9.0
[2.8.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.8.0...v2.8.1
[2.8.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.7.0...v2.8.0
[2.7.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.6.0...v2.7.0
[2.6.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.5.0...v2.6.0
[2.5.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.4.2...v2.5.0
[2.4.2]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.4.1...v2.4.2
[2.4.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.4.0...v2.4.1
[2.4.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.2.0...v2.4.0
[2.2.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.1.1...v2.2.0
[2.1.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.1.0...v2.1.1
[2.1.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.0.3...v2.1.0
[2.0.3]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.0.2...v2.0.3
[2.0.2]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.0.1...v2.0.2
[2.0.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v2.0.0...v2.0.1
[2.0.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v1.0.1...v2.0.0
[1.0.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.5.6...v1.0.0
[0.5.6]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.5.5...v0.5.6
[0.5.5]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.5.4...v0.5.5
[0.5.4]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.5.3...v0.5.4
[0.5.3]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.5.2...v0.5.3
[0.5.2]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.5.1...v0.5.2
[0.5.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.4.3...v0.5.0
[0.4.3]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.4.2...v0.4.3
[0.4.2]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.1.3...v0.2.0
[0.1.3]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/commits/v0.1.1
