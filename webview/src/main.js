import Vue from 'vue';
import App from './App.vue';

Vue.config.productionTip = false;

new Vue({
  render: createElement => createElement(App),
}).$mount('#app');

window.addEventListener('GitLabDuoThemeChanged', (event) => {

  for (const themeProperty in event.detail.themeColors) {
    let [r, g, b] = event.detail.themeColors[themeProperty]
    let rgbString = `rgb(${r}, ${g}, ${b})`;

    document.documentElement.style.setProperty("--" + themeProperty, rgbString);
  }
})

window.addEventListener('GitLabDuoFontChanged', (event) => {
  for (const fontId in event.detail.fontPayload) {
    let fontObject = event.detail.fontPayload[fontId]

    document.documentElement.style.setProperty(`--${fontId}Name`, fontObject.fontName);
    document.documentElement.style.setProperty(`--${fontId}Size`, `${fontObject.fontSize}px`);
  }
})
